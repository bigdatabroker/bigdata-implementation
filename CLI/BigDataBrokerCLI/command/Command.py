class Command(object):
    """Abstract class Command.
    
    Command presents the different methods which later would be implemented 
    by various commands.
    
    Author: Florian Mueller
    
    Attributes:
        cmd: Command string from CommandExecutor.
        results: Result object from JMSAPIConnector.
    """
    
    def __init__(self, cmd):
        self._cmd = cmd
        self._results = object
    
    
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Raises:
            NotImplementedError: Abstract class.
        """
        raise NotImplementedError("abstract class")
        
        
    def execute(self):
        """Command is executed by the concrete operation object.
        
        Raises:
            NotImplementedError: Abstract class.
        """
        raise NotImplementedError("abstract class")
    
    
    def needs_input(self):
        """Checks whether the command requires more input.
        
        Checks whether the Command still requires more data to create a command for the
        interactive shell.
        
        Raises:
            NotImplementedError: Abstract class.
        """
        raise NotImplementedError("abstract class")
        
        
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Raises:
            NotImplementedError: Abstract class.        
        """
        raise NotImplementedError("abstract class")
    
    
    @staticmethod     
    def can_handle():
        """Checks if the given command must be run on the selected object.
        
        Raises:
            NotImplementedError: Abstract class.        
        """
        raise NotImplementedError("abstract class")
    
    
    @staticmethod 
    def get_help():
        """Return information about the concrete class.
        
        Raises:
            NotImplementedError: Abstract class.        
        """
        raise NotImplementedError("abstract class")        
        