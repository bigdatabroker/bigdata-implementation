from operation.Auth import Auth
from operation.CreateJob import CreateJob
from operation.CreateNewOperation import CreateNewOperation
from operation.DeleteJob import DeleteJob
from operation.DeleteOperationFromJob import DeleteOperationFromJob
from operation.DeleteServer import DeleteServer
from operation.FinalizeJob import FinalizeJob
from operation.GetAllOperationsOfJob import GetAllOperationsOfJob
from operation.GetInfoFromJobAndOperation import GetInfoFromJobAndOperation
from operation.GetJobInfo import GetJobInfo
from operation.GetJobLog import GetJobLog
from operation.GetJobsInfo import GetJobsInfo
from operation.GetMoreNodes import GetMoreNodes
from operation.GetNode import GetNode
from operation.GetServer import GetServer
from operation.GetServerInfo import GetServerInfo
import operation.Help  # no from .. import ... because of cyclic references!
from operation.UpdateJob import UpdateJob
from operation.UpdateServer import UpdateServer
from operation.UpdateOperation import UpdateOperation
from operation.GetLogentry import GetLogentry
from operation.GetClassification import GetClassification
from operation.AddConnection import AddConnection
from operation.UpdateServerConnection import UpdateServerConnection
from operation.DeleteServerConnection import DeleteServerConnection
from operation.GetServerConnection import GetServerConnection
from operation.GetJobStatus import GetJobStatus
from operation.Debug import Debug
from operation.Logout import Logout


command_classes = [AddConnection,
Auth,
CreateJob,
CreateNewOperation,
Debug,
DeleteJob,
DeleteOperationFromJob,
DeleteServer,
DeleteServerConnection,
FinalizeJob,
GetAllOperationsOfJob,
GetClassification,
GetInfoFromJobAndOperation,
GetJobInfo,
GetJobLog,
GetJobsInfo,
GetJobStatus,
GetLogentry,
GetMoreNodes,
GetNode,
GetServer,
GetServerConnection,
GetServerInfo,
operation.Help.Help,  # no from .. import ... because of cyclic references!
Logout,
UpdateJob,
UpdateOperation,
UpdateServer,
UpdateServerConnection]
