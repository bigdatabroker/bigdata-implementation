import subprocess
import re

def get_stdout(cmd):
    print("successful!")
    print(cmd)
    cmd = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)  
    data = cmd.stdout.read()
    cmd.terminate()
    return str(data)

def to_string(unknown):
    result = ""
    for sign in unknown:
        if sign != "[" and sign != "]" and sign != "'" and sign != " ":
            result += sign
    return result

def get_job_id(data):
    job_id = re.findall("n[0-9]+", data)
    job_id = str(job_id).replace("n", "")
    job = to_string(job_id)
    return job

def get_string_value(data, key):
    result = re.findall("\"" + key + "\": \"[A-Za-z0-9]+", data)
    result = str(result).replace("\"" + key + "\": \"", "")
    result = to_string(result)
    if result.find(",") != -1:
        result = re.sub(",[A-Za-z0-9]*", "", result)
    return result
    
def get_int_value(data, key):
    result = re.findall("\"" + key + "\": [0-9]+", data)
    result = str(result).replace("\"" + key + "\": ", "")
    return to_string(result)
    
def main():
    cli = "python3 Cli.py "
    debug_on = cli + "debug on"
    debug_off = cli + "debug off"
    login = cli + "login 2"
    logout = cli + "logout"
    add_job = cli + "add-job script-job"
    update_job = cli + "update-job "
    name = "updated-job"
    priority = "1"
    get_jobs = cli + "get-jobs"
    get_job = cli + "get-job "
    get_job_log = cli + "get-job-log "
    get_logentry = cli + "getlogentry "
    delete_job = cli + "delete-job "
    
    add_operation = cli + "add-op "
    operation = " COPY "
    op_target = " server2:/home "
    op_source = " server2:/boot "
    get_operations = cli + "get-operations "
    update_op = cli + "update-operation "
    get_operation = cli + "get-operation "
    delete_operation = cli + "delete-operation "
    finalize_job = cli + "finalize-job "
    get_status = cli + "get-status "
    
    get_servers = cli + "get-servers "
    update_server = cli + "update-server "
    server_id = "1 "
    server_hostname = " testserver:/ "
    capacity = " 3333 "
    get_server = cli + "get-server "
    ls = cli + "ls "
    depth = " 2 "
    
    get_classification = cli + "get-classification "
    add_connection = cli + "add-connection "
    target_server_id = " 2 "
    connection_protocol = " testftp "
    connection_speed = " 500 "
    delete_server_connection = cli + "delete-server-connection "
    update_server_connection = cli + "update-server-connection "
    connection_speed_update = " 200 "
    
    """login
    
    """
    #login
    print(login)
    
    """check debug-mod
    
    """
    data = get_stdout(debug_off)
    if data.find("disabled") == -1:
        print("Error at: " + debug_off)
        return 1
    
    data = get_stdout(debug_on)
    if data.find("enabled") == -1:
        print("Error at: " + debug_on)
        return 1
    
    """create, update, show and delete jobs
    
    """
    #add-job
    data = get_stdout(add_job)
    try:
        job = get_int_value(data, "id")
    except Exception:
        print("Error at: " + add_job)
        return 1
    
    #update-job
    update_job = update_job + " " + job + " " + name + " " + priority
    data = get_stdout(update_job)
    try:
        prio = get_int_value(data, "priority")
        if prio != str(priority) or data.find(name) == -1:
            raise Exception()
    except Exception:
        print("Error at: " + update_job)
        return 1
    
    #get-jobs
    data = get_stdout(get_jobs)
    try:
        all_jobs = get_job_id(data)
        if job not in all_jobs:
            raise Exception()
    except Exception:
        print("Error at: " + get_jobs)
        return 1
    
    #get-job
    data = get_stdout(get_job + job)
    try:
        if data.find(name) == -1:
            raise Exception()
    except Exception:
        print("Error at: " + get_job + job)
        return 1
    
    #get-job-log
    get_job_log += job
    data = get_stdout(get_job_log)
    try:
        if data.find("Error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + get_job_log)
        return 1
    
    #get-logentry
    data = get_stdout(get_logentry)
    try:
        if data.find("Error: 404") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + get_logentry)
        return 1
    
    #delete-job
    delete_job += " " + job
    data = get_stdout(delete_job)
    try:
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + delete_job)
        return 1
    
    #get-job
    data = get_stdout(get_job + job)
    try:
        if data.find(name) is not -1:
            raise Exception()
    except Exception:
        print("Error at: " + get_job + job)
        return 1
    
    """create, update, show and delete operations
    
    """
    #add-job
    data = get_stdout(add_job)
    try:
        job = get_int_value(data, "id")
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + add_job)
        return 1
    
    #add-operation
    add_operation += job + operation + op_source + op_target
    data = get_stdout(add_operation)
    try:
        op_id = get_int_value(data, "id")
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + add_operation)
        return 1
    
    #get-operations
    get_operations += job
    data = get_stdout(get_operations)
    try:
        found_op_id = get_int_value(data, "id") 
        if data.find("error") != -1 or data.find("Invalid input") != -1 or found_op_id != op_id:
            raise Exception()
    except Exception:
        print("Error at: " + get_operations)
        return 1
    
    
    #update-operation
    update_op += job + " " + op_id + operation + op_target + op_source 
    data = get_stdout(update_op)
    try:
        op_status = get_string_value(data, "status")
        op_id = get_int_value(data, "id")
        if op_status != "WAITING":
            raise Exception()
    except Exception:
        print("Error at: " + update_op)
        return 1
    
    #get-operation
    get_operation += job + " " + op_id
    data = get_stdout(get_operation)
    try:
        op = get_string_value(data, "type")
        if op  != to_string(operation):
            raise Exception()
    except Exception:
        print("Error at: " + get_operation)
        return 1
    
    #delete-operation
    delete_operation += job + " " + op_id
    data = get_stdout(delete_operation)
    try:
        if data.find("ok") == -1:
            raise Exception()
    except Exception:
        print("Error at: " + delete_operation)
        return 1
    
    #finalize-job
    finalize_job += job
    data = get_stdout(finalize_job)
    try:
        if data.find("finalized") == -1:
            raise Exception()
    except Exception:
        print("Error at: " + finalize_job)
        return 1
    
    #get-status
    get_status += job
    data = get_stdout(get_status)
    try:
        if data.find("READY") == -1:
            raise Exception()
    except Exception:
        print("Error at: " + get_status)
        return 1
    
    
    
    """update and show server and show server-directories
    
    """
    #get-servers
    data = get_stdout(get_servers)
    try:
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + get_servers)
        return 1
    
    #update-server
    update_server += server_id + capacity
    data = get_stdout(update_server)
    try:
        capa = get_int_value(data, "capacity")
        if capa != to_string(capacity):
            raise Exception()
    except Exception:
        print("Error at: " + update_server)
        return 1
    
    #get-server
    get_server += server_id
    data = get_stdout(get_server)
    try:
        capa = get_int_value(data, "capacity")
        if capa != to_string(capacity):
            raise Exception()
    except Exception:
        print("Error at: " + get_server)
        return 1
    
    #ls
    ls += server_hostname + depth
    data = get_stdout(ls)
    try:
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + ls)
        return 1
    
    """create, update, show and delete server-connections
    
    """
    #get-classification
    get_classification +=  server_id
    data = get_stdout(get_classification)
    try:
        if data.find("Error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + get_classification)
        return 1
    
    #add-connection
    add_connection += server_id + target_server_id + connection_protocol + connection_speed
    data = get_stdout(add_connection)
    try:
        src_id = get_int_value(data, "source")
        trg_id = get_int_value(data, "target")
        conn_speed = get_int_value(data, "speed")
        protocol = get_string_value(data, "protocol")
        if src_id != to_string(server_id) or trg_id != to_string(target_server_id):
            raise Exception()
        if conn_speed != to_string(connection_speed) or protocol != to_string(connection_protocol):
            raise Exception()
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + add_connection)
        return 1
    
    #update-server-connection
    update_server_connection += server_id + target_server_id + connection_protocol + connection_speed_update
    data = get_stdout(update_server_connection)
    try:
        new_con_speed = get_int_value(data, "speed")
        if new_con_speed != to_string(connection_speed_update):
            raise Exception()
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + update_server_connection)
        return 1
    
    
    #delete-server-connection
    delete_server_connection += server_id + target_server_id + connection_protocol
    data = get_stdout(delete_server_connection)
    try:
        if data.find("error") != -1 or data.find("Invalid input") != -1:
            raise Exception()
    except Exception:
        print("Error at: " + delete_server_connection)
        return 1
    
    """logout
    
    """
    print(logout)
    print("successful!")
    return 0

    
    
if __name__ == '__main__':
    if main() == 1:
        print("An error has occurred during the test!")
    else:
        print("Test has been completed successfully!")