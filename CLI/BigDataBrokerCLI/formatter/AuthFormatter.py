class AuthFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Authentication formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """
    
    def __init__(self):
        self.__name = "auth"
    
    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
        
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        return "Authentication was successful!"
