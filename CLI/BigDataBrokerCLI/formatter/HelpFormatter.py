class HelpFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Help formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """

    def __init__(self):
        self.__name = "help"
        
    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
    
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """         
        result = ""
        for line in data["commands"]:
            try:
                result += (line["name"] + "\n" + " "*4 + line["description"] + "\n\n")
            except TypeError:
                pass
            
        return result
            
