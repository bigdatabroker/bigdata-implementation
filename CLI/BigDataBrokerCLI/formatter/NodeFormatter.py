class NodeFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Node formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """
    
    def __init__(self):
        self.__name = "node"
           
    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
    
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        if not data:
            return "No nodes available!"
        
        result = ""
        try:
            if data["results"]:
                del data["formatter"]
                results = data["results"]
                parent = results["parent"]
                result = self.__construct_tree(results, parent, 0)
            else:
                return "No nodes available!"

        except KeyError:
            result += str(data["type"])     
            result += " "*4 + str(data["hostname"]) + " "*4 + str(data["name"])
            result += " "*4 + str(data["owner"]["user"]) + ":" + str(data["owner"]["group"])
            result += " "*4 + str(data["permissions"])
        
        if result is not "":
            return "type | hostname | name | user:group | permissions" + "\n\n" + result
        return None
    
    def __get_childs(self, data, parent):
        full_parent_path = [parent["hostname"]]
        full_parent_path.extend(parent["path"])
        childs = []
        for outter in data["nodes"]:
            full_path = [outter["hostname"]]
            full_path.extend(outter["path"])
            full_path.pop()
            if full_path == full_parent_path:
                childs.append(outter)
                
        return childs
    
    def __construct_tree(self, data, node, level):
        result = "\t"*level + node["type"]
        result += " "*2 + str(node["hostname"]) + " "*2 + str(node["name"])
        result += " "*2 + str(node["owner"]["user"]) + ":" + str(node["owner"]["group"])
        result += " "*2 + str(node["permissions"]) + "\n"
        childs = self.__get_childs(data, node)
        
        for child in childs:
            result += self.__construct_tree(data, child, (level + 1))
        return result   
