class OperationFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Operation formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """
    
    def __init__(self):
        self.__name = "operation"

    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
    
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        if not data:
            return "No operations available!"
        
        result = ""
        try:
            if data["results"]:
                del data["formatter"]
                for outter in data["results"]:
                    result += str(outter["job"]) + " "*4 + str(outter["id"])
                    result += " "*4 + str(outter["type"]) + " "*4 + str(outter["start"])
                    result += " "*4 + str(outter["end"]) + " "*4 + str(outter["status"])
                    result += " "*4 + str(outter["source"]) + " "*4 + str(outter["target"]) + "\n"
            else:
                return "No operations available!" 
        except KeyError:
            result += str(data["job"]) + " "*4 + str(data["id"])
            result += " "*4 + str(data["type"]) + " "*4 + str(data["start"])
            result += " "*4 + str(data["end"]) + " "*4 + str(data["status"])
            result += " "*4 + str(data["source"]) + " "*4 + str(data["target"])

        return "jobID | opID | operation | start | end | status | source | target" + "\n\n" + result