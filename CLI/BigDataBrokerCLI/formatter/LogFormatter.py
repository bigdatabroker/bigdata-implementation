class LogFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Log formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """
    
    def __init__(self):
        self.__name = "log"
            
    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
        
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        if not data:
            return "No logs available!"                            
        result = ""
        
        try:
            if data["results"]:
                del data["formatter"]
                for outter in data["results"]:
                    if outter["foreign_id"]:
                        result += str(outter["foreign_id"]) + " "*4
                    else:
                        result += "-" + " "*4
                    result += str(outter["type"]) + "(" + str(outter["created_on"]) + ")\n"
                    result += "\t" + str(outter["message"]) + "\n"
            else:
                return "No logs available!"
        except KeyError:
            if data["foreign_id"]:
                    result += str(outter["foreign_id"]) + " "*4
            else:
                    result += "-" + " "*4
            result += str(data["type"]) + "(" + str(data["created_on"]) + ")\n"
            result += "\t" + str(data["message"]) + "\n"
        
        return "ID | type(created on)" + "\n\t" + "message" + "\n\n" + result
