class ConnectionFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Connection formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """
    
    def __init__(self):
        self.__name = "connection"
       
    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
            
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        if not data:
            return "No connection available!"
        
        result = ""
        try:
            del data["formatter"]
            for outter in data["results"]:
                result += str(outter["source"]) + " "*4 + str(outter["target"])
                result += " "*4 + str(outter["protocol"]) + " "*4 + str(outter["speed"]) + " Mb/s"
                result += "\n"
        
        except KeyError:
            result += str(data["source"]) + " "*4 + str(data["target"])
            result += " "*4 + str(data["protocol"]) + " "*4 + str(data["speed"]) + " Mb/s"
            
            
        return "source | target | protocol | speed" + "\n\n" + result