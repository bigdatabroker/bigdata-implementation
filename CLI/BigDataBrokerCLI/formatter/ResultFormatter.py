class ResultFormatter(object):
    """Template to format the return from the Job Management Server.
    
    Selects the strategy for formatting and returns the formatted data.
    Author: Florian Mueller    
    """


    def __init__(self, strategy):
        self.__strategy = strategy
        
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        return self.__strategy.get_formatted(data)