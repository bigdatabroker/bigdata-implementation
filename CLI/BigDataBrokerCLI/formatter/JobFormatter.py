class JobFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Job formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """
         
    def __init__(self):
        self.__name = "job"
             
    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
    
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        if not data:
            return "No jobs available!"
        result = ""
        
        try:
            if data["results"]:
                del data["formatter"]
                for outter in reversed(data["results"]):
                    result += str(outter["id"]) + " "*4 + str(outter["user"])
                    result += " "*4 + str(outter["priority"]) + " "*4 + str(outter["status"])
                    result += " "*4 + str(outter["name"]) + " "*4                   
                    result += "\n"
                return "jobID | user | priority | status | name" + "\n\n" + result
            else:
                return "No jobs available!"
        except KeyError:
            result += str(data["id"]) + " "*4 + str(data["user"])
            result += " "*4 + str(data["priority"]) + " "*4 + str(data["status"])
            result += " "*4 + str(data["name"]) + " "*4
            if data["dates"]:
                for date in data["dates"]:
                    result += "\n" + "\t"
                    result += str(date["event"]) + " "*4 
                    result += str(date["time"]) + " "*4
        
        return "jobID | user | priority | status | name" + "\n" + "\t" + "event | time" + "\n\n" + result
