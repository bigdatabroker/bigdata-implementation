class ServerFormatter(object):
    """Formatter for the return from the Job Management Server.
    
    Used Default-Server formatting to format the return form the
    Job Management Server for the output in the shell.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this Formatter.
    """
    
    def __init__(self):
        self.__name = "server"

    def get_name(self):
        """Returns the formatter name.

        Returns:
            The formatter name.
        """
        return self.__name
    
    def get_formatted(self, data):
        """Formatting of the return from the Job Management Server.
        
        Args:
            data: Unformatted data.
        
        Returns:
            Formatted string to print on shell.        
        """
        if not data:
            return "No server available!"
        
        result = ""
        try:
            if data["results"]:
                del data["formatter"]
                for outter in data["results"]:
                    result += str(outter["id"]) + " "*4 + str(outter["hostname"])
                    result += " "*4 + str(outter["capacity"]) + " "*4 + str(outter["last_activity"])
                    result += " "*4 + str(outter["is_active"])
                    result += "\n"
            else:
                return "No server available!"
        except KeyError:
            result += str(data["id"]) + " "*4 + str(data["hostname"])
            result += " "*4 + str(data["capacity"]) + " "*4 + str(data["last_activity"])
            result += " "*4 + str(data["is_active"])

        return "serverID | hostname | capacity | last-activity | active" + "\n\n" + result