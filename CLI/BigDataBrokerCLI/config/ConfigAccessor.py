from config.Config import Config

class ConfigAccessor(object):
    """Provides access to the configuration object.
    
    Provides access to a system-wide unique configuration object.
    Author: Florian Mueller
    
    Attributes:
        ACCESSOR: A system-wide unique reference to the config object.
        CONFIGS: Multiple configuration objects to handle.
    """
    
    ACCESSOR = None
    __CONFIGS = [Config, ]

    def __init__(self):
        raise Exception("abstract class")
    
    
    @staticmethod 
    def get_config(name):
        """Searches for the matching configuration object.
        
        Args:
            name: Name of the configuration object.
        
        Returns:
            Configuration object.
        """
        for conf in ConfigAccessor.__CONFIGS:
            configuration = conf()
            if configuration.get_name() == name:
                    return configuration
        return None
            
        
