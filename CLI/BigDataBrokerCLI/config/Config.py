import os
import sys
import json

class Config(object):
    """Provides read and write access to the configuration file.
    
    The config has a configuration file ready to write or read with data.
    Author: Florian Mueller
    
    Attributes:
        name: Name of this configuration editor.
    """
    
    def __init__(self):
        self.__name = "default"
        self.__filename = "cli.cfg"
        
             
    def get_name(self):
        """Returns the config name.

        Returns:
            The config name.
        """
        return self.__name
        
        
    def get_value(self, name):
        """Reading a value from the configuration file.
        
        Args:
            name: Name of the value to read.
        
        Returns:
            Read out value.
        
        Raises:
            IOError: Configuration file does not exist.
        """
        result = ""
        if os.path.isfile(self.__filename) is False:
                print(self.__filename + " file does not exist!")
                try:
                    with open (self.__filename, "w") as outfile:
                        data = {"hostname": "", "port": "", "user_id": "", "token": "", "debug": False}
                        json.dump(data, outfile, sort_keys=True, indent=4, separators=(",", ": "))
                        print("Generated empty %s file." % self.__filename)
                except Exception:
                    pass
                sys.exit()
        
        try:
            with open (self.__filename, "r+") as outfile:
                data = json.load(outfile)
                result = data[name]
                outfile.close()
        except IOError:
            print("Can not open file.")
            return False
        except ValueError:
            print("Can not store data into file.")
            return False
        except KeyError:
            print("Can not find the key: " + name)

        return result
    
    
    def set_value(self, key, value):
        """Write a value to the configuration file.
        
        Args:
            value: Value to write into file.
            key: Location in the file.
        
        Returns:
            True, if writing was successful, else False.
        
        Raises:
            IndexError: No value is specified.
        """
        if os.path.isfile(self.__filename) is False:
                print("config.txt file does not exist!")
                try:
                    with open (self.__filename, "w") as outfile:
                        data = {"hostname": "", "port": "", "user_id": "", "token": "", "debug": False}
                        json.dump(data, outfile, sort_keys=True, indent=4, separators=(",", ": "))
                        print("Generated empty config.txt file.")
                except Exception:
                    pass
                return False
            
        try:
            with open (self.__filename, "r+") as outfile:
                data = json.load(outfile)
                data[key] = value
                outfile.close()
                with open (self.__filename, "w") as outfile:
                    json.dump(data, outfile, ensure_ascii=False, sort_keys=False, indent=4, separators=(',', ': '))  
            outfile.close()
        except IOError:
            print("Can not open file.")
            return False
        except ValueError:
            print("Can not store data into file.")
            return False
        except KeyError:
            print("Can not find the key: " + key)
            return False
  
        return True
