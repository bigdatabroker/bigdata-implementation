import unittest
from config.Config import Config

class TestConfig(unittest.TestCase):
    """Config unittest
    
    Config unittest
    Author: Florian Mueller
    """
    
    def setUp(self):
        self.config = Config()
        self.key1 = "port"
        self.key2 = "debug"
        self.key3 = "token"
        self.key4 = "user_id"
        self.key5 = "hostname"
        self.value1 = 443
        self.value2 = True
        self.value3 = "hh86z6iybm5lijnq2ckc4kd5t0542tnw"
        self.value4 = 2
        self.value5 = "jms.bigdatabroker.de"
        self.config.set_value(self.key1, self.value1)
        self.config.set_value(self.key2, self.value2)
        self.config.set_value(self.key3, self.value3)
        self.config.set_value(self.key4, self.value4)
        self.config.set_value(self.key5, self.value5)
        
    def tearDown(self):
        del self.config

    def test_set_value(self):
        self.assertEqual(self.config.set_value(self.key1, self.value1), True)
        self.assertEqual(self.config.set_value(self.key2, self.value2), True)
        self.assertEqual(self.config.set_value(self.key3, self.value3), True)
        self.assertEqual(self.config.set_value(self.key4, self.value4), True)
        self.assertEqual(self.config.set_value(self.key5, self.value5), True)
        
    def test_get_value(self):
        self.assertEqual(self.config.get_value(self.key1), self.value1)
        self.assertEqual(self.config.get_value(self.key2), self.value2)
        self.assertEqual(self.config.get_value(self.key3), self.value3)
        self.assertEqual(self.config.get_value(self.key4), self.value4)
        self.assertEqual(self.config.get_value(self.key5), self.value5)


if __name__ == "__main__":
    unittest.main()