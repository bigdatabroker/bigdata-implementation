import unittest
from shell.CommandParser import CommandParser

class Test(unittest.TestCase):
    """CommandParser unittest
    
    CommandParser unittest.
    Author: Florian Mueller
    """


    def setUp(self):
        self.parser = CommandParser()
        self.argv = []
        self.argv.append("c:/data/bla")
        self.argv.append("äüö")
        self.argv.append("--force")


    def tearDown(self):
        del self.parser


    def test_parse_command(self):
        parsed = self.parser.parse_command(self.argv)
        self.assertEqual(parsed[0], self.argv[1])
        self.assertEqual(parsed[1], self.argv[2])


if __name__ == "__main__":
    unittest.main()