import unittest
from config.ConfigAccessor import ConfigAccessor


class Test(unittest.TestCase):
    """ConfigAccessor unittest
    
    ConfigAccessor unittest.
    Author: Florian Mueller
    """


    def setUp(self):
        self.config_name = "default"
        self.key1 = "debug"
        self.value1 = True
        self.config = ConfigAccessor.get_config(self.config_name)


    def tearDown(self):
        del self.config

    def test_get_config(self):
        self.assertEqual(self.config.get_name(), self.config_name)
    
    def test_config(self):
        self.assertEqual(self.config.set_value(self.key1, self.value1), True)
        self.assertEqual(self.config.get_value(self.key1), self.value1)


if __name__ == "__main__":
    unittest.main()