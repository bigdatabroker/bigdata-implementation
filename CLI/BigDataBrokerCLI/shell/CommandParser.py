import shlex

class CommandParser(object):
    """Formatter for the user input.
    
    Parses user input to be usable by the rest of the application.
    Author: Florian Mueller
    """
    
    def __init__ (self):
        pass    
    
    def parse_command(self, argv):
        """Parses the user input.
        
        Takes the plain input the user enters at the command line interface, removes unneeded
        parameters such as the shell path and returns the result.
        
        Args:
            argv: Input String from the command line interface.
                    
        Returns:
            string: Formatted input string.
        """    
        shlexer = shlex.shlex(str(argv))
        shlexer.wordchars += "."
        shlexer.source = "source"
        return argv[1:]
