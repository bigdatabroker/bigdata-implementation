import sys
import getpass
from shell.CommandParser import CommandParser
from shell.CommandExecutor import CommandExecutor

class Shell(object):
    """Main class of the command line interface.
    
    It handles user input and 
    executes commands via the REST-API of the Job Management Server.
    Author: Florian Mueller
    
    Attributes:
        input_str: The string entered by the user. For example, the user input 
        "./shell.py auth user1" results in __input_str = ["auth", "user1"]. 
    """
    
    def __init__(self):
        """Takes the command line arguments as input and stores them in argv. 
        
        Args:
            Input from user.
        """
        self.__input_str = sys.argv

    def main(self):
        """Is used for the user interaction.
            
        Passes the user request input to the formatter and then to the
        command executor. Finally, the result is passed to print.
        In case of errors, a specific error code is returned.
            
        Returns:
            0:= no error occurred
            1:= invalid input
            2:= command could not be created
            3:= command could not be executed
            4:= result could not be printed
        """
        password = None
        parser = CommandParser()
        parsed = parser.parse_command(self.__input_str)
            
        if len(parsed) == 0 or parsed is None:
            self.__print("Invalid input!")
            return 1
                
        executor = CommandExecutor()
        executed = executor.create_command(parsed)
                
        if executed is not None:
            self.__print(parsed[0] + " unknown command. Invalid input!")
            if executed is not False:
                self.__print(executed)
            return 2
        
        if executor.needs_input() is True:
            if password is None:
                password = getpass.getpass("Please enter password: ")
                executor.execute_more_input(password)
                
        else:
            if executor.execute_command() is False:
                self.__print("During the execution of the command, an error has occurred.")
                return 3
            
        if not self.__print(executor.get_result()):
            return 4
        return 0
    
    def __print(self, result):
        """Prints the result
        
        Prints the results after executing the user input.
        """
        if result is not None and not result == "null":
            print("\n")
            print(result)
            return True
        return False
    
