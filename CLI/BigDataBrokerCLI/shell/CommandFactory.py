from command.formatters import formatters
import command.cmdlist  # no from .. import ... because of cyclic references!

class CommandFactory(object):
    """Abstract factory for generating the specific command-methods class.
    
    Searches for matching command and formatter objects and returns.
    Author: Florian Mueller
    
    Attributes:
        command_class: Reference to create command objects and 
            invoke its static methods.
        formatter: Reference to create formatter objects.
    """
    
    def __init__(self):
        self._command_class = command.cmdlist.command_classes
        self._formatter = formatters
        
    def get_command(self):
        """Abstract method. 
        
        Method to get the right command.
        
        Raises:
            NotImplementedError: Abstract class.
        """
        raise NotImplementedError("abstract class")
    
    def get_result_formatter(self):
        """Abstract method.
         
        Method to get the right formatter.
        
        Raises:
            NotImplementedError: Abstract class.
        """
        raise NotImplementedError("abstract class")   
