from shell.RestAPIFactory import RestAPIFactory
from command.Command import Command

class CommandExecutor(object):
    """Executes user commands and returns the results to the shell.
    
    Creates the command objects interrogates whether additional information is
    needed for execution. The command is executed and the results returned.
    Author: Florian Mueller
    
    Attributes:
        command: Command to execute.
    """

    def __init__(self):
        self.__command = Command
        
    def create_command(self, parsed):
        """Appropriate command for execution is created.
        
        Create the command object and validate user input.
        
        Args:
            parsed: Formatted input.
        
        Returns:
            True, if the command is created, else False.
        """
        factory = RestAPIFactory()
        self.__command = factory.get_command(parsed)
        
        if self.__command is None:
            return False
        if self.__command.validate() is False:
            error_help = []
            error_help.append(self.__command.get_help())
            help = {"commands": error_help, "formatter": "help"}
            formatter = factory.get_result_formatter(help)
            formatted_result = formatter.get_formatted(help)
            return formatted_result
        return None
    
    def needs_input(self):
        """Check if the command object needs more input.
        
        Checks whether the CommandExecutor still requires 
        more data to create a command.
        
        Returns:
            True, if the command needs more input, else False.
        """
        return self.__command.needs_input()
    
    def execute_more_input(self, more_input):
        """Execution of the command with more input.
        
        Execution of the command with additional input from the user.
        
        Args:
            more_input: Additional input from the user.
        
        Returns:
            True, if the command was executed successfully, else False.
        """
        more_information = []
        more_information.append("auth")
        more_information.append("password")
        more_information.append(more_input)
        factory = RestAPIFactory()
        self.__command = factory.get_command(more_information)
        
        if self.__command is None:
            return False
        if self.__command.validate() is False:
            return False
        return self.__command.execute()
        
    def execute_command(self):
        """Execution of the command.
        
        Returns:
            True, if the command was executed successfully, else False.
        """
        if self.__command is not None:
            return self.__command.execute()
        return False
    
    
    def get_result(self):
        """Returns the result.
        
        Returns the formatted result after running the command.
        
        Returns:
            Formatted result.
        """
        factory = RestAPIFactory()
                
        if self.__command is not None:
            if self.__command.get_results() is not True:
                formatter = factory.get_result_formatter(self.__command.get_results())
                formatted_result = formatter.get_formatted(self.__command.get_results())
                return formatted_result
        return None