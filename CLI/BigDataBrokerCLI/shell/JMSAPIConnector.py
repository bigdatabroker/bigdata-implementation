import http.client
import json
import collections
from config.ConfigAccessor import ConfigAccessor

class JMSAPIConnector(object):
    """Handles the communication between the client and Job Management Server.
    
    Loads the data from the configuration file into the hostname_ip and then it
    starts a connection to the Job Management Server. A request to the server
    is started and the response is received.
    Author: Florian Mueller
    
    Attributes:
        target_ip: Target IP address from Job Management Server.
        api_namespace: API namespace for Rest-API.
    """

    def __init__(self):
        self.__target_ip = "jms.bigdatabroker.de:80"
        self.__api_namespace = "/rest_api/v1"
        
    def send_request(self, method, data, is_authentificated=True):
        """Sends a request to the Job Management Server.
        
        Starts a connection to the Job Management Server. A request
        to the server is started and the response is received.
        Complex token handling included.
        
        Args:
            method: Which server request is meant.
            data: Request information to transmit.
        
        Returns:
            object: Response from the Job Management Server.
        """
        config = ConfigAccessor.get_config("default")
        self.__target_ip = str(config.get_value("hostname")) + ":" + str(config.get_value("port"))

        request_method = method.split()[0]
        request_url = self.__api_namespace + method.split()[1]
        try:
            user_token = str(config.get_value("user_id")) + ":" + str(config.get_value("token"))
        except TypeError:
            return None
        data_dump = json.dumps(data)
                                 
        try:
            if config.get_value("debug"):
                print("HTTPConnection: " + self.__target_ip)
                print("Method: " + request_method)
                print("URL: " + request_url)
                print("Body: " + str(data_dump))
                print("user-token: " + str(user_token))
        except TypeError:
            return None
        
        try:            
            https_conn = http.client.HTTPSConnection(self.__target_ip)
            
            if is_authentificated:
                https_conn.request(request_method, request_url, body=data_dump, headers={"Accept": "application/json", "Content-Type": "application/json", "X-BDB-Token": user_token})
            else:
                https_conn.request(request_method, request_url, body=data_dump, headers={"Accept": "application/json", "Content-Type": "application/json"})
            
            response = https_conn.getresponse()
            response_body = response.read()
            response_headers = response.getheaders()
            https_conn.close()
        except TimeoutError:
            print("Timeout error!")
            return None
        except ConnectionRefusedError:
            print("Connection error!")
            return None
        except UnicodeEncodeError:
            print("Unicode encode error! Please try it again with quotation marks. e.g. \"a:/öäü\"")
            return None
        except Exception:
            print("Connection error! " + str(response.status) + ": " + str(response.reason))
            return None
        
        try:
            if config.get_value("debug"):
                print("response-status: " + str(response.status))
                print(response_body)
                print("------------------------------end-of-debugging------------------------------\n") 
        except Exception:
            return None
            
        if len(response_body) == 0 and response.status in range(200, 299):
            return {"status": "ok"}
        
        if response.status in range(400, 599):
            print("HTTP request failed! Error: " + str(response.status) + ": " + str(response.reason))
            try:
                dictionary = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(response_body.decode(encoding='utf_8', errors='ignore'))
                json_dict = (json.dumps(dictionary, ensure_ascii=False, sort_keys=False, indent=4, separators=(',', ': ')))
                data_dict = json.loads(json_dict)
                keys = data_dict.keys()
                for k in keys:
                    print(str(data_dict[k]))
            except Exception:
                return None
            return None
        
        try:
            dictionary = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(response_body.decode(encoding='utf_8', errors='ignore'))
            json_dict = (json.dumps(dictionary, ensure_ascii=False, sort_keys=False, indent=4, separators=(',', ': ')))
        except Exception:
            return "JSON conversion error!"
        
        data_dict = json.loads(json_dict)
        return data_dict
    
