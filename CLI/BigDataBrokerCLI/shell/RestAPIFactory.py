from shell.CommandFactory import CommandFactory
from formatter.ResultFormatter import ResultFormatter

class RestAPIFactory(CommandFactory):
    """Factory for generating the specific command-methods class.
    
    Searches for matching command and formatter objects and returns.
    Author: Florian Mueller    
    """

    def __init__(self):
        super(RestAPIFactory, self).__init__()

    def get_command(self, cmd):
        """Searches for the matching command object.
        
        Iterates over all command classes and generates a matching 
        command object and returns.
        
        Args:
            cmd: Command string.
        
        Returns:
            Command: if the command object is found, else None.
        """
        for operation in self._command_class:
            if operation.can_handle(cmd) is True:
                return operation(cmd)        
        return None
            
    def get_result_formatter(self, data):
        """Searches for the matching formatter object.

        Iterates over all formatter classes and generates 
        a matching formatter object.
        
        Args:
            data: Unformatted data object.
        
        Returns:
            Formatter: if the formatter object is found, else None.
        """
        for format in self._formatter:
            result_formatter = format()
            try:
                if result_formatter.get_name() == data["formatter"]:
                    del data["formatter"]
                    return ResultFormatter(result_formatter)
            except KeyError:
                pass
            except TypeError:
                pass
                 
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "default":
                return ResultFormatter(result_formatter)
        return None


        
