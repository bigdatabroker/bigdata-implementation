import unittest
from command.formatters import formatters
from formatter.ResultFormatter import ResultFormatter


class Test(unittest.TestCase):
    """Formatter unittest
    
    Formatter unittest
    Author: Florian Mueller
    """
    
    def setUp(self):
        self._formatter = formatters
        self.data = {"detail": "ok"}        


    def tearDown(self):
        del self._formatter


    def test_auth_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "auth":
                result = ResultFormatter(result_formatter).get_formatted(self.data)
                self.assertEqual(result, "Authentication was successful!")
                break
        self.assertEqual(result_formatter.get_name(), "auth")
    
    def test_connection_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "connection":
                break
        self.assertEqual(result_formatter.get_name(), "connection")
    
    def test_default_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "default":
                break
        self.assertEqual(result_formatter.get_name(), "default")
    
    def test_finalize_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "finalize":
                break
        self.assertEqual(result_formatter.get_name(), "finalize")
    
    def tes_help_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "help":
                break
        self.assertEqual(result_formatter.get_name(), "help")
    
    def test_job_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "job":
                break
        self.assertEqual(result_formatter.get_name(), "job")
    
    def test_log_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "log":
                break
        self.assertEqual(result_formatter.get_name(), "log")
    
    def test_node_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "node":
                break
        self.assertEqual(result_formatter.get_name(), "node")
    
    def test_operation_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "operation":
                break
        self.assertEqual(result_formatter.get_name(), "operation")
    
    def test_server_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "server":
                break
        self.assertEqual(result_formatter.get_name(), "server")
    
    def test_status_formatter(self):
        for format in self._formatter:
            result_formatter = format()
            if result_formatter.get_name() == "status":
                break
        self.assertEqual(result_formatter.get_name(), "status")


if __name__ == "__main__":
    unittest.main()