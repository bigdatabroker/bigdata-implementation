import unittest
from shell.CommandExecutor import CommandExecutor


class Test(unittest.TestCase):
    """CommandExecutor unittest
    
    CommandExecutor unittest
    Author: Florian Mueller
    """


    def setUp(self):
        self.cmd_exe = CommandExecutor()
        self.cmd = []
        self.cmd.append("add-job")


    def tearDown(self):
        del self.cmd_exe

    def test_create_command(self):
        self.assertEqual(self.cmd_exe.create_command(["", ""]), False)
        self.assertEqual(self.cmd_exe.create_command(["add-job", ""]), None)
        self.assertEqual(self.cmd_exe.create_command(self.cmd).find("add-job"), not(-1))

    def test_execute_command(self):
        self.assertEqual(self.cmd_exe.create_command(["help", ""]), None)
        self.assertEqual(self.cmd_exe.execute_command(), True)
    
if __name__ == "__main__":
    unittest.main()