from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class CreateNewOperation(Command):
    """A new operation for a job is created.
    
    Creates a new operation for the specified job.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(CreateNewOperation, self).__init__(cmd)
            
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 5 and int(self._cmd[1]) and not str(self._cmd[2]) == "delete":
                return True
            if len(self._cmd) == 4 and str(self._cmd[2]) == "delete" and int(self._cmd[1]):
                return True
            if len(self._cmd) == 6:
                if str(self._cmd[5]) == "--force:true" or str(self._cmd[5]) == "--force:false":
                    return True
            if len(self._cmd) == 5 and str(self._cmd[2]) == "delete":
                if str(self._cmd[4]) == "--force:true" or str(self._cmd[4]) == "--force:false":
                    return True                
        except ValueError:
            pass
        except IndexError:
            pass
        return False
     
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        job_id = self._cmd[1]        
        method = "PUT /job/" + job_id + "/operation/"
        
        if len(self._cmd) == 5 and not str(self._cmd[2]) == "delete":
            data = {"type": self._cmd[2], "source": self._cmd[3], "target": self._cmd[4]}
        
        if len(self._cmd) == 4:
            data = {"type": self._cmd[2], "source": self._cmd[3]}
        else:
            if len(self._cmd) == 6 and str(self._cmd[5]) == "--force:true":
                data = {"type": self._cmd[2], "source": self._cmd[3], "target": self._cmd[4], "force": True}
            if len(self._cmd) == 6 and str(self._cmd[5]) == "--force:false":
                data = {"type": self._cmd[2], "source": self._cmd[3], "target": self._cmd[4], "force": False}
                    
        if len(self._cmd) == 5 and str(self._cmd[2]) == "delete":
            if str(self._cmd[4]) == "--force:true": 
                data = {"type": self._cmd[2], "source": self._cmd[3], "force": True}
            if str(self._cmd[4]) == "--force:false":
                data = {"type": self._cmd[2], "source": self._cmd[3], "force": False}
                    
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
      
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "operation"
        return results
  
    def needs_input(self):     
        return False
              
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "createnewoperation" or cmd[0].lower() == "add-op" or cmd[0].lower() == "add-operation":        
            return True
        return False
       
    @staticmethod
    def get_help():
        help = {"name": "add-operation <jobID: Number> <type: copy/move/delete/rename> <source: URL> <target: URL> [--force:true/false]", "description": "A new operation for a job is created."}      
        return help
