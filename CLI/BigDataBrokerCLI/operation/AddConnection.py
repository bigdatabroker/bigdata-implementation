from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class AddConnection(Command):
    """Adds a new connection.
    
    Adds a new connection classification between the two
    specified servers.
    Author: Florian Mueller
    """
    
    def __init__(self, cmd):
        super(AddConnection, self).__init__(cmd)
                
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 5 and int(self._cmd[1]) and int(self._cmd[2]):
                return True
        except ValueError:
            pass        
        return False
     
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        server_id = self._cmd[1]
        target_id = self._cmd[2]
        method = "PUT /server/" + server_id + "/connection/"
        data = {"target": target_id, "protocol": self._cmd[3], "speed": self._cmd[4]}
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False

    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "connection"
        return results

    def needs_input(self):     
        return False

    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "addconnection" or cmd[0].lower() == "add-connection":      
            return True
        return False

    @staticmethod 
    def get_help():
        help = {"name": "add-connection <serverID: Number> <targetID: Number> <protocol: String> <speed: Number>", "description": "Adds a new connection classification."}
        return help