from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class DeleteServerConnection(Command):
    """Deletes the server connection.
    
    Deletes the specified server connection.
    Author: Florian Mueller
    """

    def __init__(self, cmd):
        super(DeleteServerConnection, self).__init__(cmd)
       
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 4 and int(self._cmd[1]) and int(self._cmd[2]):
                return True
        except ValueError:
            pass
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        server_id = self._cmd[1]
        target_id = self._cmd[2]
        protocol = self._cmd[3]
        method = "DELETE /server/" + server_id + "/connection/" + target_id + "/" + protocol + "/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
        
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "status"
        return results
    
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "deleteserverconnection" or cmd[0].lower() == "delete-server-connection":      
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "delete-server-connection <serverID: Number> <targetID: Number> <protocol: String>", "description": "Deletes the specified server connection."}
        return help