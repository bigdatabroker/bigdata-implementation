from command.Command import Command
from config.ConfigAccessor import ConfigAccessor

class Logout(Command):
    """User logout.

    logout from JMS conversation.
    Author: Florian Mueller  
    """

    def __init__(self, cmd):
        super(Logout, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        if len(self._cmd) == 1:
            return True
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        try:
            config = ConfigAccessor.get_config("default")
            config.set_value("user_id", "")
            config.set_value("token", "")
        except TypeError:
            return None
        except KeyError:
            return None
        
        return True
            
    def get_results(self):
        """Returns logout status.
        
        Returns:
            Logout status.
        """
        return {"formatter": "status", "status": "Logout was successful!"}
    
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "logout":       
            return True
        return False
       
    @staticmethod   
    def get_help():
        help = {"name" : "logout", "description" : "User logout."} 
        return help