from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class DeleteOperationFromJob(Command):
    """The operation of a job is deleted.
    
    Deletes the specified operation.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(DeleteOperationFromJob, self).__init__(cmd)
      
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 3 and int(self._cmd[1]) and int(self._cmd[2]):
                return True
        except ValueError:
            pass
        return False
     
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        job_id = self._cmd[1]
        operation_id = self._cmd[2]
        method = "DELETE /job/" + job_id + "/operation/" + operation_id + "/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
       
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "status"
        return results
 
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "deleteoperationfromjob" or cmd[0].lower() == "delete-operation" or cmd[0].lower() == "rm-op":        
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "delete-operation <jobID: Number> <operationID: Number>", "description": "The operation of a job is deleted."}       
        return help
