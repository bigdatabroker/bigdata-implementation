from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class CreateJob(Command):
    """Creation of a new job.
    
    Creates a new job with the specified parameters.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(CreateJob, self).__init__(cmd)
    
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 3 and int(self._cmd[2]) or len(self._cmd) == 2:
                return True
        except ValueError:
            pass
        return False
    
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        method = "PUT /job/"
        data = {"name": self._cmd[1]}
        if len(self._cmd) == 3:
            data = {"name": self._cmd[1], "priority": self._cmd[2]}
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
     
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "job"
        return results
 
    def needs_input(self):     
        return False
    
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "createjob" or cmd[0].lower() == "add-job":        
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name" : "add-job <name: String> [priority: Number]", "description" : "Creation of a new job."}  
        return help
