from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class UpdateOperation(Command):
    """Updates the specified operation.
    
    Updates the specified operation.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(UpdateOperation, self).__init__(cmd)
    
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 6 and int(self._cmd[1]) and int(self._cmd[2]) and not str(self._cmd[3]) == "delete":
                return True
            if len(self._cmd) == 5 and str(self._cmd[3]) == "delete" and int(self._cmd[1]) and int(self._cmd[2]):
                return True
            if len(self._cmd) == 7:
                if str(self._cmd[6]) == "--force:true" or str(self._cmd[6]) == "--force:false":
                    return True
            if len(self._cmd) == 6 and str(self._cmd[3]) == "delete":
                if str(self._cmd[5]) == "--force:true" or str(self._cmd[5]) == "--force:false":
                    return True 
        except ValueError:
            pass
        except IndexError:
            pass
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        job_id = self._cmd[1]
        operation_id = self._cmd[2]        
        method = "DELETE /job/" + job_id + "/operation/" + operation_id + "/"
        data = None
        if connector.send_request(method, data) is None:
            return False
        method = "PUT /job/" + job_id + "/operation/"
        
        if len(self._cmd) == 6 and not str(self._cmd[3]) == "delete":
            data = {"type": self._cmd[3], "source": self._cmd[4], "target": self._cmd[5]}
        else:
            if len(self._cmd) == 5:
                data = {"type": self._cmd[3], "source": self._cmd[4]}
            else:
                if len(self._cmd) == 7:
                    if str(self._cmd[6]) == "--force:true":
                        data = {"type": self._cmd[3], "source": self._cmd[4], "target": self._cmd[5], "force": True}
                    if str(self._cmd[6]) == "--force:false":
                        data = {"type": self._cmd[3], "source": self._cmd[4], "target": self._cmd[5], "force": False}
        
        if len(self._cmd) == 6 and str(self._cmd[3]) == "delete":
            if str(self._cmd[5]) == "--force:true": 
                data = {"type": self._cmd[3], "source": self._cmd[4], "force": True}
            if str(self._cmd[5]) == "--force:false":
                data = {"type": self._cmd[3], "source": self._cmd[4], "force": False}
                    
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
        
    # TODO comment
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "operation"
        return results
      
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "updateoperation" or cmd[0].lower() == "update-operation":       
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "update-operation <jobID: Number> <operationID: Number> <type: copy/move/delete/rename> <source: URL> <target: URL> [--force:true/false]", "description": "A operation is updated."}      
        return help