from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class GetNode(Command):
    """Information about a specific element in the file tree can be delivered.
    
    Retrieves information about a specific node.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(GetNode, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        if len(self._cmd) == 2:
            return True
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        path = self._cmd[1]
        method = "GET /node/" + path + "/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
    
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "node"
        return results
    
    def needs_input(self):     
        return False
     
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "getnode" or cmd[0].lower() == "ls" or cmd[0].lower() == "dir":
            if len(cmd) in (1, 2):
                return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "ls <path: URL(example: a:/b/c/)>", "description": "Information about a specific element in the file tree can be delivered."}
        return help
