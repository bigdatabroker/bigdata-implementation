from command.Command import Command
from config.ConfigAccessor import ConfigAccessor

class Debug(Command):
    """Enable and disable debug mode.
    
    Activates or deactivates the debug mode.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(Debug, self).__init__(cmd)
  
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        if len(self._cmd) == 2:
            if str(self._cmd[1]) == "on" or str(self._cmd[1]) == "off":
                return True
        return False
       
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        debug_status = None
        if str(self._cmd[1]) == "on":
            debug_status = True
        if str(self._cmd[1]) == "off":
            debug_status = False
        
        if debug_status is not None:
            try:
                config = ConfigAccessor.get_config("default")
                config.set_value("debug", debug_status)
            except TypeError:
                return False
            except KeyError:
                return False
            
            return True
        
        return False
      
    def get_results(self):
        """Returns debug mode.
        
        Returns:
            Debug on/off.
        """
        try:
            config = ConfigAccessor.get_config("default")
            debug = config.get_value("debug")
        except TypeError:
            return None
        except KeyError:
            return None
        
        if debug:
            return {"formatter": "status", "status": "Debug mode enabled."}
        if not debug:
            return {"formatter": "status", "status": "Debug mode disabled."}
        return None
 
    def needs_input(self):     
        return False
      
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "debug":       
            return True
        return False
  
    @staticmethod
    def get_help():
        help = {"name": "debug <switch: on/off>", "description": "Enable or disable debug mode."}   
        return help