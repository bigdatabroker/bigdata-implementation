from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector
from config.ConfigAccessor import ConfigAccessor

class Auth(Command):
    """Authentication of the user with their user identification.

    The authentication pseudo resource enables the clients to authenticate and
    legitimate themselves against the Job Management Server. After passing all
    required authentication data like username and password or private keys,
    the Job Management Server verifies the data and issues an access token if
    the verification was successful or returns an error message if the
    authentication was unsuccessful.
    Author: Florian Mueller  
    """

    def __init__(self, cmd):
        super(Auth, self).__init__(cmd)

    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 2 and int(self._cmd[1]):
                config = ConfigAccessor.get_config("default")
                config.set_value("user_id", int(self._cmd[1]))
                return True
            if len(self._cmd) == 3 and self._cmd[1] == "password":
                return True
        except ValueError:
            pass
        return False
   
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        try:
            config = ConfigAccessor.get_config("default")
            user_id = config.get_value("user_id")
        except TypeError:
            return None
        except KeyError:
            return None
            
        connector = JMSAPIConnector()
        method = "POST /auth/"
        password = self._cmd[2]
        data = {"user_id": user_id, "password": password}
        results = connector.send_request(method, data, is_authentificated=False)
        if results is not None:
            self._results = results
            return True
        return False
            
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        if self._results is not None:
            try:
                config = ConfigAccessor.get_config("default")
                config.set_value("token", self._results["token"])
                config.set_value("user_id", int(self._results["user"]))
            except TypeError:
                return None
            except KeyError:
                return None
        
        results = self._results
        results["formatter"] = "auth"
        return results
 
    def needs_input(self):     
        return True
      
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "auth" or cmd[0].lower() == "login":       
            return True
        return False
       
    @staticmethod   
    def get_help():
        help = {"name" : "Auth <userId: Number>", "description" : "Authentication of the user with their user identification."} 
        return help
         
