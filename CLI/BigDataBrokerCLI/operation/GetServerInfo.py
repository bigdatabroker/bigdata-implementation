from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class GetServerInfo(Command):
    """Information about specific server is returned.
    
    Retrieves information about the specified server.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(GetServerInfo, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 2 and int(self._cmd[1]):
                return True
        except ValueError:
            pass
        return False
    
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        server_id = self._cmd[1]
        method = "GET /server/" + server_id + "/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
        
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "server"
        return results
    
    def needs_input(self):     
        return False
    
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "getserverinfo" or cmd[0].lower() == "get-server":        
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "get-server <serverID: Number>", "description": "Information about specific server is returned."}     
        return help
