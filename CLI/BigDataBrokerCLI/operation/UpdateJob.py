from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class UpdateJob(Command):
    """A job is updated.
    
    Updates the specified job.
    Author: Florian Mueller   
    """

    def __init__(self, cmd):
        super(UpdateJob, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) in (3, 4) and int(self._cmd[1]):
                return True
        except ValueError:
            pass
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        job_id = self._cmd[1]
        method = "POST /job/" + job_id + "/"
        data = {"name": self._cmd[2]}
        if len(self._cmd) == 4:
            data = {"name": self._cmd[2], "priority": self._cmd[3]}
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
        
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        results["formatter"] = "job"
        return results
    
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "updatejob" or cmd[0].lower() == "update-job":        
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "update-job <jobID: Number> <name: String> [priority: Number]", "description": "A job is updated."}       
        return help
