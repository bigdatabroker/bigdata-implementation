from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class GetJobsInfo(Command):
    """An overview of all jobs is returned.
    
    The job resource enables clients to view, create, modify and delete jobs.
    Author: Florian Mueller   
    """

    def __init__(self, cmd):
        super(GetJobsInfo, self).__init__(cmd)
    
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        if len(self._cmd) == 1:
            return True
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        method = "GET /job/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
        
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results    
        return {"results": results, "formatter": "job"}
    
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "getjobsinfo" or cmd[0].lower() == "get-jobs" or cmd[0].lower() == "jobs":        
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "get-jobs", "description": "An overview of all jobs is returned."} 
        return help
