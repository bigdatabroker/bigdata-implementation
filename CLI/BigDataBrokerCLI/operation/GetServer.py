from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class GetServer(Command):
    """List of all servers, including information is returned.
    
    Retrieves a list of all registered servers.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(GetServer, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        if len(self._cmd) == 1:
            return True
        return False
       
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        method = "GET /server/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
      
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        return {"results": results, "formatter": "server"}
  
    def needs_input(self):     
        return False
      
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "getserver" or cmd[0].lower() == "get-servers":        
            return True
        return False
  
    @staticmethod
    def get_help():
        help = {"name" : "get-servers", "description" : "List of all servers, including information is returned."}      
        return help
