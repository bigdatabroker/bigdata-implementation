from command.Command import Command
import command.cmdlist  # no from .. import ... because of cyclic references!

class Help(Command):
    """A help text is displayed for each command.
    
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(Help, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        if len(self._cmd) in (1, 2):
            return True
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        return True
        
    def get_results(self):
        """Returns help information.
        
        Returns:
            Help.
        """
        help = []
        op = []
        if len(self._cmd) == 1:
            for operation in command.cmdlist.command_classes:
                help.append(operation.get_help())
        else:
            for operation in command.cmdlist.command_classes:
                op.append(self._cmd[1])
                if operation.can_handle(op):
                    help.append(operation.get_help())
                op.pop()   
        return {"commands": help, "formatter": "help"}
    
    def needs_input(self):   
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "help":    
            return True
        return False
        
    @staticmethod   
    def get_help():
        help = {"name": "help <operation: Command>", "description": "A help text is displayed for each command."} 
        return help
