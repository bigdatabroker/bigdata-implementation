from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class GetAllOperationsOfJob(Command):
    """All operations of a job are returned.
    
    Retrieves a list of all operations of the specified job.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(GetAllOperationsOfJob, self).__init__(cmd)
    
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 2 and int(self._cmd[1]):
                return True
        except ValueError:
            pass
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        job_id = self._cmd[1]
        method = "GET /job/" + job_id + "/operation/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
        
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        return {"results": results, "formatter": "operation"}
    
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "getalloperationsofjob" or cmd[0].lower() == "get-operations":       
            return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "get-operations <jobID: Number>", "description": "All operations of a job are returned."}      
        return help
