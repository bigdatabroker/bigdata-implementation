from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class GetMoreNodes(Command):
    """All children up to a defined depth, starting from the specified path will
    be returned.
    
    Retrieves a list containing all child nodes of the specified node.
    Author: Florian Mueller    
    """

    def __init__(self, cmd):
        super(GetMoreNodes, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 3 and int(self._cmd[2]) < 4:
                return True
        except TypeError:
            pass
        except ValueError:
            pass
        return False
 
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        path = self._cmd[1]
        method = "GET /node/" + path + "/"
        data = None
        parent = connector.send_request(method, data)
        depth = self._cmd[2]
        method = "GET /node/" + path + "/childs/?depth=" + depth
        results = connector.send_request(method, data)
        if results is not None and parent is not None:
            self._results = {"nodes": results, "parent": parent}
            return True
        return False
       
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        return {"results": results, "formatter": "node"}
    
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "getmorenodes" or cmd[0].lower() == "ls" or cmd[0].lower() == "dir":
            if len(cmd) > 2:      
                return True
        return False
    
    @staticmethod
    def get_help():
        help = {"name": "ls <path: URL(example: a:/b/c/)> [depth: Number (e.g. 3)]", "description": "All children up to a defined depth, starting from the specified path will be returned."}       
        return help