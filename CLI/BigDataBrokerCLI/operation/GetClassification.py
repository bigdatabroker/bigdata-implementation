from command.Command import Command
from shell.JMSAPIConnector import JMSAPIConnector

class GetClassification(Command):
    """Retrieves all available classifications.
    
    Retrieves all available classifications between the specified
    server and other servers.
    Author: Florian Mueller
    """
    
    def __init__(self, cmd):
        super(GetClassification, self).__init__(cmd)
        
    def validate(self):
        """Checks whether the supplied parameters are used.
        
        Returns:
            True, if the command is valid, else False.
        """
        try:
            if len(self._cmd) == 2 and int(self._cmd[1]):
                return True
        except ValueError:
            pass
        return False
        
    def execute(self):
        """Command is executed.
            
        Returns:
            True, if execution of the command was successful, else False.
        """
        connector = JMSAPIConnector()
        server_id = self._cmd[1]
        method = "GET /server/" + server_id + "/connection/"
        data = None
        results = connector.send_request(method, data)
        if results is not None:
            self._results = results
            return True
        return False
        
    def get_results(self):
        """Request result from JMSAPIConnector.
        
        Returns:
            Result from JMSAPIConnector.
        """
        results = self._results
        return {"results": results, "formatter": "connection"}
    
    def needs_input(self):     
        return False
        
    @staticmethod        
    def can_handle(cmd):
        if cmd[0].lower() == "getclassification" or cmd[0].lower() == "get-classification":      
            return True
        return False
    
    @staticmethod 
    def get_help():
        help = {"name": "get-classification <serverID: Number>", "description": "Retrieves all available classifications."}
        return help