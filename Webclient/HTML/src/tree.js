/**
* The TreeView class delivers the main control element of the Webclient. It draws the file tree and controls the flat view.
* @param root the root node
*/
function TreeView () {
    rootJSON = new Object();
    rootJSON["url"] = "/";
    rootJSON["name"] = "/";
    this.root = new Directory(rootJSON);


    this.handleResponse = function(response) {
        tree.addServer(response);
        tree.openNode("/");
        tree.changeOpened("/");
        tree.drawTree();
    }


    var connector = new JMSConnector("host", "port");
    var request = new GetAllServers(this.handleResponse);
    connector.sendInstruction(request);


   /**
    * Adds a subtree to the main tree.
    * @param json JSON-Objects with infomation about server
    */
    this.addServer = function(json) {
        var i = 0;
        while(json[i] != undefined){

            this.root.add(new Directory(json[i]));
            i++;
        }
        this.drawTree();

        var children = this.root.getChildren();
        for (var i = 0; i < children.length; i++) {
            var request = new GetNodeChildren(children[i], 3, true);
            connector.sendInstruction(request, false);
        }
        return;
    };


    /**
    * Adds a subtree to the main tree.
    * @param json: The subtree as json object.
    */
    this.add = function(json) {
        var min = 1000000; // Minimum depth of the path
        var max = 0; // Maximum depth of the path

        var j = 0;
        while(json[j] != undefined){ //Find max and min path depth
           var url = json[j].url;
           var countDepth = url.split("/").length;
           if(min > countDepth){
             min = countDepth;
           }

           if(max < countDepth){
                max = countDepth;
           }
           j++;
        }
        for (var k = min; k <= max; k++) {
            var i = 0;
            while(json[i] != undefined){
                var url = json[i].url;
                var countDepth = url.split("/").length;
                if(countDepth == k){
                    var newObject;
                    if(json[i].type == "DIRECTORY"){
                        newObject = new Directory(json[i]);
                    }else if(json[i].type == "FILE"){
                        newObject = new File(json[i]);
                    }
                    var children1 = this.resolvePath(newObject.getNodePath()).getChildren();
                    var b = true;
                    for (var j = 0; j < children1.length; j++) {
                        if(children1[j].getName() == newObject.getName()){
                            b = false;
                        }
                    };
                    if(b){
                        this.resolvePath(newObject.getNodePath()).add(newObject);                  
                        
                    }                    
                }

                i++;

            }
        };
        this.drawTree();
        return;
    };


    /**
    * Removes a file or a entire directory from the tree.
    * @param    path: The path of the node.
    */
    this.remove = function(path) {
        this.drawTree();
        return;
    };


    /** 
    * Opens the node in the flat view.
    * @param path: The path of the node.
    */
    this.openNode = function(path) {
        var directory = this.resolvePath(path);
        document.getElementById("gogogo").value = directory.getNodeUri();
        document.getElementById("filetable").innerHTML = directory.drawFlatView();

        setCurrentFolder(path);

        return;
    };


    /**
    * Private 
    * Resolve the path of a directory
    * @param path: The path of the node.
    * @return the node
    */
    this.resolvePath = function(path) {
        var root = this.root;
        var folders = path.split("/");
        var lastRoot = root;
        for (var i = 0; i < folders.length; i++) { 
                for (var j = 0; j < root.getChildren().length; j++) {                    
                    if(root.getChildren()[j].getName() == folders[i] && root.getChildren()[j] instanceof Directory){
                        root = root.getChildren()[j];
                        break;
                    }
                };
                if(lastRoot == root){
                    break;
                }else{
                    lastRoot = root;
                }
        };
        return root;
    }


    /**
    * Draws the TreeView and returns it in HTML code as a string.
    * @return Returns the TreeView in HTML code as a string.
    */
    this.drawTree = function() {
        var result = "<ul>";
        result += this.root.drawTree();
        result += "</ul>";
        document.getElementById("tree").innerHTML = result;
        return;
    };


    /**
     * Private
     * search the node with node path path and
     * change the state of opend
     * @param path the node 
     * @param root the root, needed for the recursion, if root == null the root of the tree is used
     */
    this.changeOpened = function(path) {
        this.root.changeOpened(path);
        return;
    }


    /**
     * Shows the content of a given path
     * @param path the path to show
     */
    this.showPath = function(path) {
        var nodePath = ecsapeHTML(path.replace(":/", "/"));
        rootJSON = new Object();
        rootJSON["url"] = nodePath;
        rootJSON["name"] = nodePath;
        var searchRoot = new Directory(rootJSON);
        var request = new GetNodeChildren(searchRoot, 1, false);
        connector.sendInstruction(request);   
    }


    /**
     * Draws the flat view for a given path
     * Only used when the user types the requested URL in the input at the top
     * @param json a JSON object representing the requested directory
     */
    this.callbackShowPath = function(json) {
        rootJSON = new Object();
        rootJSON["url"] = "/";
        rootJSON["name"] = "/";
        var localRoot = new Directory(rootJSON);

            var j = 0;
            while(json[j] != undefined){ //Find max and min path depth
                var newObject;
                if(json[j].type == "DIRECTORY"){
                    newObject = new Directory(json[j]);
                }else if(json[j].type == "FILE"){
                    newObject = new File(json[j]);
                }
                localRoot.add(newObject);                    
                j++;
            }
        document.getElementById("filetable").innerHTML = localRoot.drawFlatView();
    }


}
/**
 * Represents a directory in the file tree.
 * @param json A JSON Directory Object
 */
function Directory (json) {
    var url;
    var nodeName;
    /*
    $.each(json, function(key, val) { 
        if (key == "url") {
            url = val;
            //debugAlert("Das ist der path_string" + key + val);
        }
        else if (key == "name") {
            nodeName = val;
            //debugAlert("Das ist der Name: " + key + " - " + val);
        }
        else if (key == "hostname") {
            hostname = val;
            //debugAlert("Das ist der Name: " + key + " - " + val);
        }
    });
    if(url == undefined){
        this.nodePath = hostname + "/";
        this.namex = hostname;
    }else{
        this.nodePath = url.replace(":","");
        this.namex = nodeName;        
    }
    */
    this.opened = false;
    this.childObjects = new Array();


    /**
    * Adds a thee below the current tree node.
    * @param t: The tree to add.
    */
    this.add = function(t) {
        this.childObjects[this.childObjects.length] = t;
        return;
    };


    /** 
    * Gets the children of the current directory.
    * @return The children of the current tree in a string array.
    */
    this.getChildren = function() {
        return this.childObjects;
    };


    /**
    * Opens the subtree of the current directory.
    */
    this.openSubtree = function(directory) {
        this.opened = true;
        return;
    };


    /**
    * Close the subtree of the current directory.
    */
    this.closeSubtree = function(directory) {
        this.opened = false;
        return;
    };


    /** 
    * Gets the value of opend.
    * @return opened.
    */
    this.getOpened = function() {
        return this.opened;
    };


    /** 
    * Gets the value of opend.
    * @return opened.
    */
    this.getNodePath = function() {
        return this.nodePath;
    };


    /*
     * Gets node path with :/ between host and path
     */
    this.getNodeUri = function() {;
        var nodeUri = this.nodePath.replace("/", ":/"); 
        return nodeUri;
    }


    /**
    * Gets the current directory and the comlete subtree as String
    * @return String with table of the directory 
    */
    this.drawTree = function() { 
        this.childObjects.sort(compare);
        var result = this.drawTreeItem();
        result += "<ul>";
        if(this.childObjects.length > 0){
            for (var i = this.childObjects.length - 1; i >= 0; i--) {
                result += this.childObjects[i].drawTree();
            } 
        }
        result += "</ul>";
        result += "</li>";
        return result;
    };


    /**
    * Private
    * Gets the current directory as String for the TreeView
    * @return String of the directory 
    */
    this.drawTreeItem = function() {
        var check = this.opened ? "checked='checked'" : "";
        return     "<li>" +
                    "<input " + check + " onclick=\"clickHandlerPlus('" + 
                        ecsapeHTML(this.nodePath) + 
                    "')\" type='checkbox' id=\"" + 
                        ecsapeHTML(this.nodePath) +
                     "\" />" +
                    "<label  onclick=\"clickHandlerDir('" + 
                        ecsapeHTML(this.nodePath) + 
                    "')\" for='item-0-0'>" + 
                        ecsapeHTML(this.namex) +
                    "</label>";
    }


    /**
    * Gets the FlatView of the current directory as String
    * @return String with table of the directory 
    */
    this.drawFlatView = function() {
        this.childObjects.sort(compare);
        var result = "<tr>" +
            "<td>" + "" + "</td>" +
            "<td onclick=\"gotoHigherFolder('" +
            "')\">" +
            "<i class=\"icon-folder-close\">" +
            "</td>" +
            "<td onclick=\"gotoHigherFolder('" +
            "')\">" + ".." +
        "</td>" +
            "<td>" +
            "</td>" +
            "<td>" +
            "</td>" +
        "</tr>";
        if(this.childObjects.length > 0){
            for (var i = this.childObjects.length - 1; i >= 0; i--) {
                result += this.childObjects[i].drawFlatViewItem();
            } 
            result += 
            "<tr style='display:none;'>" +
                "<td>" +
                    "<label class=\"checkbox\">" +
                    "<input value=\"" +
                    "\" name=\"check\" type=\"checkbox\">" +
                    "</label>" +
                "</td>" +
                "<td  onclick=\"clickHandlerDir('" + 
                    "')\">" +
                    "<i class=\"icon-file\">" +
                "</td>" +
                "<td onclick=\"clickHandlerDir('" + 
                    "')\">" +
                "</td>" +
                //"<td>" +
                    //"01/04/2012" + //TODO
                //"</td>" +
                "<td>" +
                "</td>" +
            "</tr>"
        }else{
            result +=
                "<tr>" +
                    "<td>" +
                        "<span id='emptyL'>This folder is empty</span>" +
                    "</td>" +
                    "<td>" +
                    "</td>" +
                    "<td>" +
                    "</td>" +
                    //"<td>" +
                    //"</td>" +
                    "<td>" +
                    "</td>" +
                    "</tr>";
        }
        return result;
    };


    /**
     * Private
     * Gets the FlatView item of the current directory as String
     * @return String with item of the directory 
     */
    this.drawFlatViewItem = function() {
        var htmlString =
            "<tr>" +
                "<td>" +
                    "<label class=\"checkbox\">" +
                        "<input value=\"" +
                        ecsapeHTML(this.nodePath) +
                        "\" name=\"check\" type=\"checkbox\">" +
                    "</label>" +
                "</td>" +
                "<td onclick=\"clickHandlerDir('" + 
                        ecsapeHTML(this.nodePath) +
                    "')\">" +
                    "<i class=\"icon-folder-close\">" +
                "</td>" +
                "<td onclick=\"clickHandlerDir('" + 
                        ecsapeHTML(this.nodePath) +
                    "')\">" +
                    ecsapeHTML(this.namex) + "" +
                "</td>" +
                //"<td>" +
                    //"01/04/2012" +
                //"</td>" +
                "<td>" +
                "</td>" +
            "</tr>";
        return htmlString;
    }


    /**
     * Returns the name of a tree view element
     * @returns {string} the name of a tree view element
     */
    this.getName = function() {
        return this.namex;
    };

    /** 
     * changes the attribute opened if nodePath1 == this.nodePath
     * @param nodePath the path of the node
     */
    this.changeOpened = function(nodePath1){
        if(this.nodePath == nodePath1){
            this.opened = !this.opened;
        }else{
            var r = this.getChildren();
            for (var i = 0; i < r.length; i++) {
                r[i].changeOpened(nodePath1);
            };
        }
        return;
    }


    /**
     * Compares two elements alphabetically.
     * Returns 1 if a is later in alphabet than b. Returns -1 if b is later in alphabet than a.
     * Returns 0 if it is the same word.
     * @param a
     * @param b
     * @returns {number}
     */
    function compare(a,b) {
        if (a.getName() > b.getName()){
            return -1;
        }
        if (a.getName() < b.getName()){
            return 1;        
        }
        return 0;
    }
}


/**
* Represents a file in the file tree
* @param nodePath The path of the file
* @param namex the nameof the file
* @param ownerUser the user owner of the file
* @param ownerGroup the group owner of the file
* @param sizex the size of the file
* @param permission the permission of the file
* @param createdOn the date the file was created
* @param changedOn the date the file was changed
* @param metaChangedOn the date the meta data of the file was changed
*/
function File (json) {
    var url;
    var nodeName;
    var owner;
    $.each(json, function(key, val) { 
        if (key == "url") {
            url = val;
            //debugAlert("Das ist der path_string" + key + val);
        }
        else if (key == "name") {
            nodeName = val;
            //debugAlert("Das ist der Name: " + key + " - " + val);
        }
        else if (key == "owner") {
            owner = val;
            //debugAlert("Das ist der Name: " + key + " - " + val);
        }
    });
    this.nodePath = url.replace(":","");
    this.namex = nodeName;
    this.ownerUser = owner["user"];


    /**
    * Return an empty String, becuase files are not shown in the tree view. Needed because of compositum
    */
    this.drawTree = function() {
        return "";
    };


    /**
    * Gets the 
    * @return the Name
    */
    this.getName = function() {
        return this.namex;
    };


    /**
    * Gets the node path
    * @return the node path
    */
    this.getNodePath = function() {
        return this.nodePath;
    };


    /**
    * Gets the size
    * @return the size
    */
    this.getSize = function() {
        return this.sizex;
    };
    //TODO getter


    /**
     * Renders an element of the flat view an returns it as HTML string
     * @returns {string} the HTML string
     */
    this.drawFlatViewItem = function() {
        var htmlString =
            "<tr>" +
                "<td>" +
                    "<label class=\"checkbox\">" +
                    "<input value=\"" +
                    ecsapeHTML(this.nodePath)+
                    "\" name=\"check\" type=\"checkbox\">" +
                    "</label>" +
                "</td>" +
                "<td  onclick=\"clickHandlerDir('" + 
                        ecsapeHTML(this.nodePath) +
                    "')\">" +
                    "<i class=\"icon-file\">" +
                "</td>" +
                "<td onclick=\"clickHandlerDir('" + 
                        ecsapeHTML(this.nodePath) +
                    "')\">" +
                    ecsapeHTML(this.namex) +
                "</td>" +
                //"<td>" +
                    //"01/04/2012" + //TODO
                //"</td>" +
                "<td>" +
                    ecsapeHTML(this.ownerUser) +
                "</td>" +
            "</tr>";
        return htmlString;
    }

    this.changeOpened = function(nodePath1){
        return;
    }

    this.getChildren = function(){ //DONT REMOVE THIS!!!!
        return new Array();
    }

    function compare(a,b) {
        if (a.getName() > b.getName()){
            return -1;
        }
        if (a.getName() < b.getName()){
            return 1;        
        }
        return 0;
    }

}


/**
* This is funktion handles the clicks in the TreeView
*/
function clickHandlerDir (nodePath1) {
    clickHandlerPlus(nodePath1);
    tree.openNode(reecsapeHTML(nodePath1));    
    changeLanguage(language);
}


/**
* This is funktion handles the clicks in the TreeView
*/
function clickHandlerPlus (nodePath1) {
    tree.changeOpened(reecsapeHTML(nodePath1));
    if(tree.resolvePath(nodePath1).getOpened()){
        loadSubtreeRecursiv(tree.resolvePath(nodePath1),2); //TODO levels = 3
    }
}


/**
 * Loads a subtree from the server and adds it to the local tree
 * @param root the root of the subtree
 * @param levels levels of children that should be loaded
 */
function loadSubtreeRecursiv(root, levels){
    if(levels == 0){
        return;
    }
    var children = root.getChildren();
    if(children.length > 0){
        for (var i = 0; i < children.length; i++) {
            if(children[i] instanceof Directory){
                loadSubtreeRecursiv(children[i],levels - 1);
            }
        };
    }else{
        if(root instanceof Directory){
            var request = new GetNodeChildren(root, 1, true); //TODO levels = 3
            connector.sendInstruction(request);           
        }  
    }
    tree.drawTree();
    return;
}


/**
 * Loads the next higher folder
 */
function gotoHigherFolder() {
    destination = webClientState.currentFolder;
    var position = destination.lastIndexOf("/");
    if(position > 3) {
        destination = destination.substring(0, position);
        clickHandlerDir(destination);
        webClientState.currentFolder = destination;
    }
    else {
        clickHandlerDir("/");
    }
}
