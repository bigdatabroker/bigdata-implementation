
/**
 * JMSConnector connects the web client to the JMS server
 * @param host
 * @param port
 * @constructor
 */
function JMSConnector(host, port) {
    this.host = host;
    this.port = port;
    var instructions = new Array();
}


/**
 * Sends a instruction as request to the server and handles its response.
 * @param instruction the insruction that should be sended
 */
JMSConnector.prototype.sendInstruction = function(instruction) {
    $.ajax( {
        url : 'http://jms.bigdatabroker.de/rest_api/v1/'
            + instruction.getURLvar(),
        type : instruction.type,
        contentType : 'application/json',
        data : instruction.data,
        dataType : 'json',
        beforeSend : function(request) {
        },
        success : function(response) {
            instruction.responseReceived = true;
            instruction.success = true;
            //instruction.callBackFunction(response);
        },
        error : function(response, ajaxOptions, thrownError) {
            instruction.responseReceived = true;
            debugAlert("CONNECTION ERROR!\nStatus: " + response.status
                + "\nError: " + thrownError);
            instruction.callBackErrorFunction(thrownError, response.responseText);
        }
    });
}








/**
 * Sends a instruction as request to the server without adding a token and handles its response.
 * @param instruction the insruction that should be sended
 */
JMSConnector.prototype.sendInstructionWithoutToken = function(instruction) {
    $.ajax( {
        url : 'http://jms.bigdatabroker.de/rest_api/v1/'
            + instruction.getURLvar(),// 'demojson.js',
        type : instruction.type,
        contentType : 'application/json',
        data : instruction.data,
        dataType : 'json',
        beforeSend : function(request) {
            window.setTimeout(function() {
                if (instruction.responseReceived == false) {
                    alert("Server has not responded for more than 10 seconds. Check your connection!")
                }
            }, 10000);
        },
        success : function(response) {

            instruction.callBackFunction(response);

        },
        error : function(response, ajaxOptions, thrownError) {
            alert("Wrong user name or password");
        }
    });
}





/**
 * This is the mother of all instructions
 * Abstract class! Never use it!
 * @constructor
 */
function Instruction() {
    this.data = "";
    this.responseReceived = false;
    this.success = true;
}


/**
 * This method is called when the connector received the response to this instruction
 * Calls an error, because this is just an abstract method
 * @param argument
 */
Instruction.prototype.callBackFunction = function(argument) {
    //debugAlert("Instruction.prototype.callBackFunction: " + argument);
}


/**
 * This method is called when the connector received an error on this instruction
 * Calls an error, because this is just an abstract method
 * @param argument
 */
Instruction.prototype.callBackErrorFunction = function(error, response) {
    //fancyError("Instruction.prototype.callBackErrorFunction: " + error);
}


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
Instruction.prototype.getURLvar = function() {
    return "Not yet implemented";
}



/**********************************************************************
 *                  AUTH            AUTH                              *
 **********************************************************************

/**
 * This method is called when the connector received the response to this instruction
 * @param error the HTTP error
 * @param response the detail error message created by the server
 */
Auth.prototype.callBackErrorFunction = function(error, response) {
    fancyAlert(response);
}


/**
 * Sending user ID and passwort to the server
 * @param userID the user ID
 * @param pass the password
 * @constructor
 */
function Auth(userID, pass) {
    this.type = "POST";
    this.data = "{\"user_id\": " + userID + ", \"password\": \"" + pass + "\"}";
    this.user = userID;
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
Auth.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
Auth.prototype.getURLvar = function() {
    return "auth/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param response the server response
 */
Auth.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    auth(this.user, response, true);
}


