/**
 * Saves a cookie with the given values
 * @param cookieName {String} the name
 * @param cookieValue {String}
 * @param lifeTime {int} lifeTime in days
 */
function saveCookie (cookieName, cookieValue, lifeTime) {
    var now = new Date();
    var expireDate = new Date(now.getTime() + 1000 * 60 * 60 * 24 * lifeTime);
    document.cookie = cookieName + "=" + cookieValue + "; expires=" + expireDate.toGMTString() + ";";
};


/**
 * Loads an cookie and returns its value
 * @param {String} cookieName the name of the cookie that should be loaded
 * @returns {String} the value of cookie
 */
function loadCookie (cookieName) {
    if (document.cookie) {
        var rawCookies = document.cookie;
        bakedCookies = rawCookies.split("; ");
        for (var i = 0; i < bakedCookies.length; i++) {
            tmp = bakedCookies[i].split("=");
            if (tmp[0] == cookieName) {
                return tmp[1];
            }
        }
        return "";
    }
    else {
        return "";
    }
};


function getCookieQuantity() {
    if (document.cookie) {
        var rawCookies = document.cookie;
        bakedCookies = rawCookies.split("; ");
        for (var i = 0; i < bakedCookies.length; i++) {
            tmp = bakedCookies[i].split("=");
            return tmp.length;
        }
        return 0;
    }
    else {
        return 0;
    }
};


/**
 * Deletes all cookies saved for this domain
 */
function deleteAllCookies() {

    if (document.cookie) {
        var rawCookies = document.cookie;
        var bakedCookies = new Array();
        bakedCookies = rawCookies.split("; ");

        for (var i = 0; i < bakedCookies.length; i++) {
            var tmp = bakedCookies[i].split("=");
            saveCookie(tmp[0], " ", -1);
        }

    }

};