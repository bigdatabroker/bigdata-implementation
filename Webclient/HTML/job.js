/**
 * The job class represents jobs which contain several operations
 * @param jobID {int} the job ID as integer
 * @constructor
 */
function Job(jobID) {
    this.jobID = jobID;
    this.nextOperationID = 0;
    this.operations = new Array();
    this.priority = 3; //TODO initialwert
}


/**
 * Adds an operation to the job
 * @param operation
 */
Job.prototype.addOperation = function(operation) {
    this.operations[this.operations.length] = operation;
    //debugAlert("1 operation added... " + this.operations);
}


/**
 * Removes an operation from the job
 * @param operationID {int} the operation ID is the position od the operation inside the operations array
 */
Job.prototype.removeOperation = function(operationID) {
    for (var i = 0; i < this.operations.length; i++) {
        if(this.operations[i].getID() == operationID){
            this.operations.splice(i,1);
            break;
        }
    };
    return;
}


/**
 * Returns the jobID
 * @returns {string} the jobID
 */
Job.prototype.getJobID = function() {
    return this.jobID;

}


/**
 * Returns the ID of the next operation
 * @returns {int} ID
 */
Job.prototype.getNextOperationID = function() {
    return this.nextOperationID++;
}


/**
 * Sets the priority
 * @param {int} the priority
 */
Job.prototype.setPriority = function(prio) {
	this.priority = prio;
    return;

}


/**
 * Gets the priority
 * @return {int} the priority
 */
Job.prototype.getPriority = function() {
    return this.priority;

}


/**
 * Returns the job as string
 * @returns {string} the job as string
 */
Job.prototype.toString = function() {
    return "job: jobID=" + this.jobID;
}


/**
 * Returns the job as a JSON formatted string
 * @returns {string} the job as a JSON formatted string
 */
Job.prototype.toJSON = function() {
    //TODO!
    return "Not yet implemented";
}


/**
 * Returns the job as a HTML table formatted string
 * @returns {string} the job as a html table string
 */
Job.prototype.drawJob = function() {
    var request = new GetOperations(this.jobID);
    connector.sendInstruction(request); 
}


/**
 * Gets the operations array
 * @returns {Operation[]} the operations array
 */
Job.prototype.getOperations = function() {
    return this.operations;
}
