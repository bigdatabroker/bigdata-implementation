describe("JMS Connector Async", function(){

    var testConnector = new JMSConnector("tmp", "99");
    var authRequest = new Auth("2", "hallo");

    async = new AsyncSpec(this);

    async.beforeEach(function(done){



        setTimeout(function(){
            testConnector.sendInstruction(authRequest);
            done();
        }, 15);

    });

    it("it should be possible to login", function(){
        expect(authRequest.success).toBe(true);
    });


    describe("When username and password are NOT valid", function(){

        var testConnector = new JMSConnector("tmp", "99");
        var authRequest = new Auth("2", "notValidPassword");

        async = new AsyncSpec(this);

        async.beforeEach(function(done){



            setTimeout(function(){
                testConnector.sendInstruction(authRequest);
                done();
            }, 15);

        });

        it("it should NOT be possible to login", function(){
            expect(authRequest.success).toBe(true);
        });
    });

});


