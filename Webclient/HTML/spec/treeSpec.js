describe("Tree", function() {
    var testDirectory = new Directory();//'{"path_string": "/bin", "owner": {"group": 1, "user": 1}, "dates": {"meta_changed": null, "changed": null, "created": null}, "flags": [], "name": "bin", "url": "server2:/bin", "path": ["bin"], "hostname": "server2", "type": "DIRECTORY", "permissions": "0777"}');


    beforeEach(function() {


    })

    it("should be able to open a directory", function() {
        testDirectory.openSubtree();
        expect(testDirectory.opened).toBeTruthy();
    });

    it("should be able to close a directory", function() {
        testDirectory.closeSubtree();
        expect(testDirectory.opened).toBeFalsy();
    });

    it("should be able to add a subtree");


});