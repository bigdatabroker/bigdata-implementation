describe("State", function() {
    var cookieName;
    var cookieValue;
    var cookieQuantity = 0;

    beforeEach(function() {
        cookieName = "TestName";
        cookieValue = "TestValue";
        cookieQuantity = getCookieQuantity();
    });

    it("should be possible to save a cookie", function() {
        saveCookie(cookieName, cookieValue, 5);

        //demonstrates use of custom matcher
        expect(cookieQuantity + 1 == getCookieQuantity());
    });

    it("should be possible to load a cookie", function() {
        var loadedCookie = loadCookie(cookieName);

        //demonstrates use of custom matcher
        expect(loadedCookie).toBeDefined();
    });

    it("should be possible to delete all cookies", function() {
        deleteAllCookies();

        //demonstrates use of custom matcher
        expect(getCookieQuantity() == 0);
    });

});