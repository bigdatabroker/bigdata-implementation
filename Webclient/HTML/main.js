/**
 * The state of the web client
 */
var webClientState;


/*
 * Language:
 * 0 = English
 * 1 = German
 * 2 = French
 * 3 = Swabian
 */
var language = 0;


/*
 * The TreeView
 */
var tree;


/*
 * The JMSConnector sends and receives messages to/from the JMS
 */
var connector = new JMSConnector("host", "port"); //TODO host and port


/**
 * Clipboard
 * Everybody should know what it's like
 */
var clipboard = new Clipboard();


/**
* Enum Command
*/
var COMMAND = {
  CUT : {value: 0, name: "MOVE"}, 
  COPY: {value: 1, name: "COPY"}, 
  RENAME : {value: 2, name: "RENAME"},
  DELETE : {value: 3, name: "DELETE"}
};


/*
 * Sends the login information to the server
 * This methode is called by the Login click
 */
function sendAuth(){
	var username = document.getElementById("inputUser").value;
    var password = document.getElementById("inputPw").value;
    document.getElementById("inputPw").value = "";

	var authRequest = new Auth(username, password);
	connector.sendInstructionWithoutToken(authRequest);
}


/**
 * When server sided authentification was the successfull, this method prepares the client state
 * This methode is called from the Auth Instruktion
 * @param {string} user username
 * @param {string} pass password
 * @returns {bool} true if username password combination is valid,
 * false if username doesn’t exist or password is not correct
 */
function auth (user, json, createJob) {
    webClientState = new State(user, json.token);
    webClientState.authenticated = true;
    var request = new GetJobs(false);
	connector.sendInstruction(request);
    tree = new TreeView();
	tree.drawTree();
	document.getElementById("navibar").style.display="inline";
	document.getElementById("username").innerHTML= user;
    drawFileBrowser();
    $("#loggedInButton").popover({placement:'bottom',
        html: 'true'});
}


/**
 * Private
 * This method checks if a job with editing status already exists for this user on tge server.
 * If such a job exists, it is loaded. If such a job do not exist, it is created.
 * @param json
 */
function findOpenJob(json){
	var i = 0;
	var max = 0;
	var iMax = -1;
    while(json[i] != undefined){
    	if(json[i].id > max){
    		max = json[i].id;
    		iMax = i;
    	}
    	i++;
    }
    if(iMax != -1 && json[iMax].status == "EDITING"){
    	var response = new Object();
    	response["id"] = json[iMax].id;
    	webClientState.createJob(response);
    }else{
    	var request = new PutJob(); //create Job
    	connector.sendInstruction(request); 
    }

}


/**
 * Saves the given memento object to a browser cookie
 * @param m {Memento} the memento destined for saving
 */
function saveMementoToCookie(m) {
    webClientState.saveToCookie();
}


/**
 * Loads the newest memento from a browser cookie if it is valid
 * @returns {Memento} a memento object or null if memento not exists
 */
function loadStateFromCookie() {
	var user = "";
	var accessToken = ""
	user = loadCookie("user");
    accessToken = loadCookie("accessToken");
    if(user != "" && accessToken != ""){
    	json = new Object();
    	json["token"] = accessToken;
    	auth(user,json, false);
    }
}


/**
 * Creates a new incomplete operation. Operation is incomplete unless
 * executeClipboard() is called
 * @param source {String[]} the source path of the files and directories for an operation
 * @param command {Command} the command for the operation 
 */
function addToClipboard(source, command) {
	clipboard.setSource(source);
	clipboard.setCommand(command);
    var statusText = lang[language]["statusCopy"]  + command.name + " \n" + source.length + " objects";
    fancyLog(statusText);
	return;
}


/**
 * Completes the incomplete job created by addToClipboard
 * @param direction {String} the directon path of the files and directories an operation
 */
function executeClipboard(direction) {
	var path = clipboard.getSource();
	for (var i = 0; i < path.length; i++) {
		var request = new PutOperation(webClientState.getCurrentJob().getJobID(), clipboard.getCommand().name, path[i], direction);
        connector.sendInstruction(request); 
	}
}


/**
 * Make the HTML element with the file browser visible
 */
function drawFileBrowser() {
	document.getElementById("browser").style.display="block";
	document.getElementById("status").style.display="none";
	document.getElementById("proceed").style.display="none";
	document.getElementById("login").style.display="none";
    return;
}


/**
 * Links all operations to the job and sends the job management server a start request
 * @returns {String} The server request as string
 */
function proceedJob() {
	//show proceed view
	document.getElementById("browser").style.display="none";
	document.getElementById("status").style.display="none";
	document.getElementById("proceed").style.display="block";
	document.getElementById("login").style.display="none";
	//show title and table
	document.getElementById("jobID").innerHTML = "Job " + webClientState.getCurrentJob().getJobID();
	webClientState.getCurrentJob().drawJob();
	changePriority();
	return;
}


/**
 * Private
 * Renders the proceed view and draws it as HTML in the inner HTML.
 * @param json
 */
function drawProceedTable(json){
	var html = "";
	var i = 0;
	while(json[i] != undefined){
		var icon;
	    switch(json[i].type){
	    	case "COPY":
	    		icon = "<i class='icon-file'><br /><i class='icon-file'>";
	    		break;
	    	case "MOVE":
	    		icon = "<i class='icon-file'><br /><i class='icon-arrow-right'>";
	    		break;
	    	case "DELETE":
	    		icon = "<i class='icon-remove'>";
	    		break;
	    	case "RENAME":
	    		icon = "<i class='icon-pencil'>";
	    		break;
	    	}
		html += "<tr>" + 
                  "<td>" + icon + "</td>" +
                  "<td>" + json[i].id + "</td>" + 
                  "<td>" + json[i].type + "</td>" + 
                  "<td>" + ecsapeHTML(json[i].source) + "</td>" + 
                  "<td>" + ecsapeHTML(json[i].target) + "</td>" + 
                  "<td><a onclick='removeOperation(" + json[i].id + ")' href='#'><i class='icon-remove-sign'></a></td>" +         
                "</tr>";
        i++;
	}
	document.getElementById("proceedTable").innerHTML = html;
    changeLanguage(language);

}


/*
 * Private
 * Private function which changes the priority of the current editing job
 */
function changePriority(){
	var prio = webClientState.getCurrentJob().getPriority();
	switch(prio){
		case 1:
			document.getElementById("vlow").setAttribute("class", "btn btn-info");
			document.getElementById("low").setAttribute("class", "btn");
			document.getElementById("medium").setAttribute("class", "btn");
			document.getElementById("high").setAttribute("class", "btn");
			document.getElementById("vhigh").setAttribute("class", "btn");
			break;

		case 2:
			document.getElementById("vlow").setAttribute("class", "btn");
			document.getElementById("low").setAttribute("class", "btn btn-success");
			document.getElementById("medium").setAttribute("class", "btn");
			document.getElementById("high").setAttribute("class", "btn");
			document.getElementById("vhigh").setAttribute("class", "btn");
			break;

		case 3:
			document.getElementById("vlow").setAttribute("class", "btn");
			document.getElementById("low").setAttribute("class", "btn");
			document.getElementById("medium").setAttribute("class", "btn btn-warning");
			document.getElementById("high").setAttribute("class", "btn");
			document.getElementById("vhigh").setAttribute("class", "btn");
			break;
		
		case 4:
			document.getElementById("vlow").setAttribute("class", "btn");
			document.getElementById("low").setAttribute("class", "btn");
			document.getElementById("medium").setAttribute("class", "btn");
			document.getElementById("high").setAttribute("class", "btn btn-danger");
			document.getElementById("vhigh").setAttribute("class", "btn");
			break;

		case 5:
			document.getElementById("vlow").setAttribute("class", "btn");
			document.getElementById("low").setAttribute("class", "btn");
			document.getElementById("medium").setAttribute("class", "btn");
			document.getElementById("high").setAttribute("class", "btn");
			document.getElementById("vhigh").setAttribute("class", "btn btn-inverse");
			break;
	}
}


/**
 * This method is called by the observer and checks the status of a subject. It calls
 * also the methode handleButtonClick()
 */
function update() {
	handleButtonClick(sSubject.getState());
}


/**
 * This method is called when the user clicks on any button
 */
function handleButtonClick(id) {
    switch(id)
    {
        case "browser":
            drawFileBrowser();
            break;

        case "status":

            var request = new GetJobs(true);
            connector.sendInstruction(request);

            document.getElementById("browser").style.display="none";
            document.getElementById("status").style.display="block";
            document.getElementById("proceed").style.display="none";
            document.getElementById("login").style.display="none";
            break;

        case "editServer":
            var request = new GetAllServers(drawServerEdit);
            connector.sendInstruction(request);
            document.getElementById("browser").style.display="none";
            document.getElementById("status").style.display="block";
            document.getElementById("proceed").style.display="none";
            document.getElementById("login").style.display="none";
            break;

        case "finalize":
            var request1 = new PostJob(webClientState.getCurrentJob().getJobID(), webClientState.getCurrentJob().getPriority());
            connector.sendInstruction(request1);
            $('#statusBox').text(lang[language]["statusSending"]);
            break;

        case "go":
            tree.showPath(document.getElementById("gogogo").value);
            break;

        case "cut":
            findCheckboxesAndExecute(COMMAND.CUT);
            break;

        case "copy":
            findCheckboxesAndExecute(COMMAND.COPY);
            break;

        case "paste":
            executeClipboard(webClientState.getCurrentFolder());
            fancyLog(lang[language]["statusPaste"]);
            break;

        case "delete":
            findCheckboxesAndExecute(COMMAND.DELETE);
            executeClipboard("---");
            fancyLog(lang[language]["statusDelete"]);
            break;

        case "proceed":
            proceedJob();
            break;

        case "vlow":
            webClientState.getCurrentJob().setPriority(1);
            changePriority();
            break;

        case "low":
            webClientState.getCurrentJob().setPriority(2);
            changePriority();
            break;

        case "medium":
            webClientState.getCurrentJob().setPriority(3);
            changePriority();
            break;

        case "high":
            webClientState.getCurrentJob().setPriority(4);
            changePriority();
            break;

        case "vhigh":
            webClientState.getCurrentJob().setPriority(5);
            changePriority();
            break;
        case "logOut":
            webClientState.deleteCookie();
            window.location.reload();
    }
    changeLanguage(language);
}


/**
 * Private
 * The current editing job is set to finalize on the server.
 */
function finalizeJob(b){
	if(b){
		var request2 = new FinalizeJob(webClientState.getCurrentJob().getJobID());
	    connector.sendInstruction(request2); 	
	}
}


/**
 * Handles the server response on a finalize instruction
 */
function callbackFinalize(){	
		var request3 = new PutJob();
	    connector.sendInstruction(request3);
}


/**
 * Private
 * This methode findes the checked chaeckboxes and adds them to the clipboard
 * @param command The command.
 */
function findCheckboxesAndExecute(command) {
	var box = document.formTable.check;
	var sources = new Array();
    //alert(box + " - " + box.length);
	for(var i = 0; i < box.length; i++){
		if(document.formTable.check[i].checked){
			sources[sources.length] = document.formTable.check[i].value;
		}
	}
	addToClipboard(sources, command); 
}


/**
 * Private
 * sets the current folder
 * @param path the path of the folder
 */
function setCurrentFolder(path){
	webClientState.setCurrentFolder(path);
}


/**
 * Removes the operation with id == id
 * @param id id of the operation
 */
function removeOperation(id){
	var request = new DeleteOperation(webClientState.getCurrentJob().getJobID(), id);
	connector.sendInstruction(request);
}


/**
 * Removes the server with id == id
 * @param id id of the operation
 */
function removeServer(id){
    var request = new DeleteServer(id);
    connector.sendInstruction(request);
}


/**
 * Renders the status view and draws it as HTML in the inner HTML.
 * @param json
 */
function drawStatus(json){

	var result = "";
    var i = 0;
    while(json[i] != undefined){
    	var color = "error";
    	var abort = "";
    	switch(json[i].status){
    		case "EDITING":
    		i++;
    		continue;
    		break;

    		case "WAITING":
    		color = "info";
    		abort = "<a onclick='statusDeleteCall(" + json[i].id + ")' href='#'><i class='icon-remove-sign' />";  //TODO onclick
    		break;

            case "READY":
            color = "info";
            abort = "<a onclick='statusDeleteCall(" + json[i].id + ")' href='#'><i class='icon-remove-sign' />";  //TODO onclick
            break;

    		case "RUNNING":
    		color = "warning";
    		break;

    		case "ERROR":
    		color = "error";
    		break;

    		case "SUCCESS":
    		color = "success";
    		break;
    	}
        if(json[i].dates.length > 0){
            var date = json[i].dates[0].time + "";
        }else{
            var date = "0";   
        }
    	
    	date = date.replace("T"," - ").split(".")[0];
		result +="<tr class='" + color + "'>" +
                    "<td>Job " + json[i].id + "</td>" +
                    "<td>" + date + "</td>" +
                    "<td>" + json[i].status + "</td>" +

                    "<td>" + json[i].priority + "</td>" +
                    "<td>" + abort + "</td>" +
                    "<td><a onclick='statusInfoCall(" + json[i].id  + ")' href='#'><i class='icon-info-sign' /></a></td>" +
                "</tr>";
        i++;
    }
    document.getElementById("status").innerHTML = drawStatusHTML(result);
    changeLanguage(language);
}


/**
 * Renders the status view and draws it as HTML in the inner HTML.
 * @param json
 */
function drawServerEdit(json){
    var result = "";
    var i = 0;
    var j = 0;
    var hostname = "";
    var capacity = "";
    var id = "";
    while(json[i] != undefined){
        $.each(json[i], function(key, val) {
            if (key == "id") {
                id = val;
            }
            else if (key == "hostname") {
                hostname = val;
            }
            else if (key == "capacity") {
                capacity = val;
            }
        });
        var color = "error";
        var update = "";
        result +="<tr class='" + color + "'>" +
            "<td>Server " + id + "</td>" +
            "<td>" + hostname + "</td>" +
            "<td>" + capacity + "</td>" +
            "<td><a onclick='removeServer(" + id + ")' href='#'><i class='icon-remove-sign'></a></td>" +
            //"<td>" + update + "</td>" +
            "</tr>";

        i++;
    }
    document.getElementById("status").innerHTML = drawServerEditHTML(result);
}


/**
 * Private
 * Sourrounding the status view with identifiers and returns it as HTML string.
 * @param html
 * @returns {string}
 */
function drawStatusHTML(html) {
return "<div class='container-fluid'>" +
      "<div class='row-fluid'>" +
        "<div class='span12'>" +
            "<table class='table table-bordered table-hover'>" +
              "<thead>" +
                "<tr>" +
                  "<th><span id='jobnumberL'>Job number</span></th>" +
                  "<th><span id='dateStatusL'>Date</span></th>" +
                  "<th><span id='statusStatusL'>Status</span></th>" +
                  "<th><span id='priorityStatusL'>Priority</span></th>" +
                  "<th><span id='abourtL'>Abort</span></th>" +
                  "<th><span id='infoL'>Info</span></th>" +
                "</tr>" +
              "</thead>" +
              "<tbody id='statusTable'>" +
            	html +
              "</tbody>" +
            "</table>" +
          "</div>" +
          "</div>" +
     "</div>";
}


/**
 * Private
 * Sourrounding the server edit view with identifiers and returns it as HTML string.
 * @param html
 * @returns {string}
 */
function drawServerEditHTML(html) {
    var returnString =  "<div class='container-fluid'>" +
        "<div class='row-fluid'>" +
        "<div class='span12'>" +
        "<table class='table table-bordered table-hover'>" +
        "<thead>" +
        "<tr>" +
        "<th>Server ID</th>" +
        "<th>Hostname</th>" +
        "<th>Capacity</th>" +
        "<th>Delete</th>" +
        //"<th>Update</th>" +
        "</tr>" +
        "</thead>" +
        "<tbody id='serverEditTable'>" +
        html +
        "</tbody>" +
        "</table>" +
        "</div>" +
        "</div>" +
        "</div>";
    return returnString;
}


/**
 * Requests the status info of a given job from the server
 * @param id the job
 */
function statusInfoCall(id){
	var request = new GetOperations(id);
	connector.sendInstruction(request);
}


/**
 * Requests to delete a given job by the server
 * @param id the job
 */
function statusDeleteCall(id){
    var request = new DeleteJob(id);
    connector.sendInstruction(request);
}


/**
 * Renders the status info view of a job and draws it as HTML in the inner HTML.
 * @param json the json object about the job received from the server
 */
function drawStatusInfo(json){	
	if(json[0] != undefined){
		var result = "";
	    var i = 0;
	    while(json[i] != undefined){
	    	var icon;
	    switch(json[i].type){
	    	case "COPY":
	    		icon = "<i class='icon-file'><br /><i class='icon-file'>";
	    		break;
	    	case "MOVE":
	    		icon = "<i class='icon-file'><br /><i class='icon-arrow-right'>";
	    		break;
	    	case "DELETE":
	    		icon = "<i class='icon-remove'>";
	    		break;
	    	case "RENAME":
	    		icon = "<i class='icon-pencil'>";
	    		break;

	    	}
			result +="<tr>"+
	                  "<td>" + icon + "</td>"+
	                  "<td>" + json[i].id + "</td>"+
	                  "<td>" + json[i].type + "</td> "+	                  
	                  "<td>" + ecsapeHTML(json[i].source) + "</td>"+
	                  "<td>" + ecsapeHTML(json[i].target) + "</td> "+
	                  "<td>" + json[i].status + "</td>" +
	                "</tr>";

	        i++;
	    }
	    document.getElementById("status").innerHTML = drawStatusInfoHTML(result, json[0].job);		
	}else{
		document.getElementById("status").innerHTML = "<div class='container-fluid'><div class='row-fluid'><div class='span12'>" + //TODO nachladen von speziellem Job
				"<h3><span id='noOperationsL'>No Operations</span></h3>" +
				"<button onclick=\"buttonHandler('status')\" class='btn'><span id='backStatus1L'>Back</span></button>" +				
                "</div></div></div>";
	}
	changeLanguage(language);
}


/**
 * Private
 * Sourrounding the status view with identifiers and returns it as HTML string.
 * @param html the HTML that should be sourrounded
 * @param id the id of the job
 * @returns {string}
 */
function drawStatusInfoHTML(html, id){	
 return "<div class='container-fluid'><div class='row-fluid'><div class='span12'>" + //TODO nachladen von speziellem Job
				"<h2><span id='jobL'>Job </span>" + id+ "</h2>" +
				"<table  class='table table-bordered table-hover'><tr>"+
                  "<th></th>"+
                  "<th><span id='infoNameL'>Operation ID</th>"+ 
                  "<th><span id='infoOperationL'>Operation</th>"+
                  "<th><span id='infoSourceL'>Source</th>"+
                  "<th><span id='infoDestinationL'>Destination</th>"+
                  "<th><span id='infoStatusL'>Status</th>"+
                "</tr>"+
                html +
                "</table>"+
                "<button onclick=\"buttonHandler('status')\" class='btn'><span id='backStatusL'>Back</span></button>" +
                "</div></div></div>";
}


/**
 * Private
 * Replaced the content of all members by a content in the requested language
 */
function changeLanguage(languageID){
	language = languageID;

	for (var member in lang[languageID]) {
		if(document.getElementById(member) != undefined){
			document.getElementById(member).innerHTML = lang[languageID][member];
		}
    }
}


/**
 * Private
 * Escapes HTML code in strings. Ensures that there is no XSS scripting.
 * @param str
 * @returns {string}
 */
function ecsapeHTML(str){
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}


/**
 * Private
 * Unescapes HTML code in strings. So it is readable but without any functionality.
 * Ensures that there is no XSS scripting.
 * @param str
 * @returns {XML}
 */
function reecsapeHTML(str){
	return str.replace('&amp;' , "&").replace('&lt;', "<").replace('&gt;', ">").replace('&quot;', "\\\"");
}