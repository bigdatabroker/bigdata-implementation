/**
 * Description An operation is the elementary object in Big Data Broker. It represents a single
 * action that is executed by an agent
 * @param command {String} The command
 * @param source {String} The source path of the files and directories for the operation
 * @param direction {String} The direction path of the files and directories for the operation
 * @constructor
 */
function Operation(command, source, direction, id) {
    //debugAlert("Hallo");
    this.id = id;
    this.command = command;
    this.source = source;
    this.direction = direction;
    debugAlert(this.command.name + " - " + this.source + " - " + this.direction);
}


/**
 * Returns the id
 * @returns {int} id
 */
Operation.prototype.getID = function() {
  return this.id;
}


/**
 * Returns the operation as string. This method is only used for logging and debugging
 * @returns {string} the operation as string
 */
Operation.prototype.toString = function() {
    var returnString = "operation: ";
    returnString = returnString + "command=" + this.command + "; ";
    returnString = returnString + "source=" + this.source + "; ";
    returnString = returnString + "direction=" + this.direction + "; ";
    return returnString;
}


/**
 * Returns the operation as a JSON formatted string
 * @returns {string} the operation as a JSON formatted string
 */
Operation.prototype.toJSON = function() {
    return "Not yet implemented";
}


// /**
//  * Returns the operation as a HTML table formatted string
//  * @returns {string} the operation as a HTML table formatted string
//  */
// Operation.prototype.drawOperation = function() {
//     return      "<tr>" + 
//                   "<td><i class='icon-file'></td>" + //TODO icon
//                   "<td>" + this.command.name + "</td>" + 
//                   "<td>" + "TODO name" + "</td>" + 
//                   "<td>" + this.source + "</td>" + 
//                   "<td>" + this.direction + "</td>" + 
//                   "<td><a onclick='removeOperation(" + this.id + ")' href='#'><i class='icon-remove-sign'></a></td>" +         
//                 "</tr>";
// }


