debugMode = false;
lastLogTime = 0;
logNo = 0;


/**
 *
 * @param htmlElement
 * @param onClickAction
 * @returns {*}
 */
function addOnClick (htmlElement, onClickAction) {
    alert("LÖSCHEN!!!! utilities.addOnClick")
    var cutPosition = "Kein WERT, TUT NICHT";
    var firstPart = htmlElement.substring(1 ,(htmlElement.length - 2));
    var secondPart = htmlElement.substring(1 ,(htmlElement.length - 2));
    print("ko");
    returnElement = returnElement + "onclick=\"" + onClickAction + "\">";
    print("ko");
    return returnElement;
}


/**
 * Sending username and password hash to server
 * @param {string} user username
 * @param {method} user username
 * @returns {string} true if username password combination is valid,
 * false if username doesn’t exist or password is not correct
 */
function auth1 (element, method) {
    alert("LÖSCHEN!!!! utilities.auth1")
    var bracketPosition = element.search(">");
    if (bracketPosition == -1)
    {
        element = "Exception! This is not a HTML-element";
    }
    else {
        print("Ko");
        element = element.substring(0, element.length - 1);
        element = element + "onclick=\"";
        element = element + method;
        element = element + "\">"
    }
    return element;
}


/**
 * Saves a cookie with the given values
 * @param cookieName {String} the name
 * @param cookieValue {String}
 * @param lifeTime {int} lifeTime in days
 */
function saveCookie (cookieName, cookieValue, lifeTime) {
    var now = new Date();
    var expireDate = new Date(now.getTime() + 1000 * 60 * 60 * 24 * lifeTime);
    document.cookie = cookieName + "=" + cookieValue + "; expires=" + expireDate.toGMTString() + ";";
}


/**
 * Loads an cookie and returns its value
 * @param {String} cookieName the name of the cookie that should be loaded
 * @returns {String} the value of cookie
 */
function loadCookie (cookieName) {
    if (document.cookie) {
        var rawCookies = document.cookie;
        bakedCookies = rawCookies.split("; ");
        for (var i = 0; i < bakedCookies.length; i++) {
            tmp = bakedCookies[i].split("=");
            if (tmp[0] == cookieName) {
                return tmp[1];
            }
        }
        return "";
    }
    else {
        return "";
    }
}


/**
 * Deletes all cookies saved for this domain
 */
function deleteAllCookies() {
    if (document.cookie) {
        var rawCookies = document.cookie;
        var bakedCookies = new Array();
        bakedCookies = rawCookies.split("; ");
        for (var i = 0; i < bakedCookies.length; i++) {
            var tmp = bakedCookies[i].split("=");
            saveCookie(tmp[0], " ", -1);
            print("Deleted+++" + tmp[0]);
        }
    }
}


//////////////////////////////////////////////////////////////////
// THE METHODS BELOW ARE SUPPOSED TO BE USED ONLY FOR DEBUGGING //
//////////////////////////////////////////////////////////////////


/**
 * Writes "Something!" in the document...
 */
function doSomething() {
    document.writeln("Something!<br>");
}


/**
 * Prints a line in the HTML-document and a HTML break command
 * @param {String} content the string that should be printed
 */
function print(content) {
    document.writeln(content + "<br>");
}


/**
 * Shows a alert window with the given alert message
 * @param {String} alertMessage the message that should be shown in the alert window
 */
function debugAlert(alertMessage) {
    if (debugMode == true) {
        alert(alertMessage);
    }
}


/**
 * Creates a readable string out of a JSON object
 * @param JSONobject the JSON object
 * @returns {string} the string
 * @constructor
 */
function JSONtoSTRING(JSONobject) {
    returnString = "";
    if (JSONobject.length > 1)
    {
        for (var i = 0; i < JSONobject.length; i++) {
            returnString += JSONtoSTRING(JSONobject[i]);
        }
    }
    else {
        returnString += JSONobject[0];
    }
    returnString += "\n";
    return returnString;
}


/**
 * Shows an alert only in debug mode that displays an JSON object
 * @param JSONobject
 */
function debugAlertJSON(JSONobject) {
    //if (debugMode == true) {

    var alertMessage = "JSONobject:\n";

    $.each(JSONobject, function(key, val) {
        alertMessage = alertMessage + "Key: " + key + " - Value: " + val + "\n";
    });

    debugAlert(alertMessage);
    //}
}


/**
 * Pausing the runtime for a given amount of seconds
 * @param prmSec
 */
function wait(prmSec) {
    prmSec *= 1000;
    var eDate = null;
    var eMsec = 0;
    var sDate = new Date();
    var sMsec = sDate.getTime();
    do { eDate = new Date();
        eMsec = eDate.getTime();
    }
    while ((eMsec-sMsec)<prmSec);
}


/**
 * Shows an error in a fancy popover
 * @param message the error message
 */
function fancyError(message) {
    $("#statusBoxButton").popover('show');
    $('#statusBox').text("Error! " + message);
    var now = new Date();
    lastLogTime = now.getTime();
    //window.setTimeout(function() { if ((alert(now.getTime() - lastLogTime)) >= 2900) { $("#cutButton").popover('hide'); }}, 3000);
}


/**
 * Shows a log message in a fancy popover
 * @param message the log message
 */
function fancyLog(message) {
    $("#statusBoxButton").popover({HTML: 'true'});
    $("#statusBoxButton").popover('show');
    if (debugMode) { message = "Log No: " + logNo + " " + message; }
    $('#statusBox').text(message);
    logNo++;
    var now = new Date();
    lastLogTime = now.getTime();
    //window.setTimeout(function() { if ((now.getTime() - lastLogTime) >= 2900) { $("#cutButton").popover('hide'); }}, 3000);
}


/**
 * Shows an error in a massive red hover box at the bottom of the page
 * @param message the error message
 */
function massiveError(message) {
    $( ".alert" ).alert('close');
    massiveErrorHTML = "<div class='alert alert-error'>" +
        "<a class='close' data-dismiss='alert' id='massiveError'>×</a>" +
        "<strong>Error!</strong>" + message +
        "</div>";
    $( "#errorPlaceHolder" ).append(massiveErrorHTML);
}