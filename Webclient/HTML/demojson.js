[
    {"path_string": "/b",
        "owner": {"group": 2, "user": 77},
        "flags": ["test"],
        "name": "b",
        "url": "a:/b",
        "path": ["b"],
        "hostname": "a",
        "type": "DIRECTORY",
        "permissions": "050"}, {"path_string": "/r", "owner": {"group": 77, "user": 1}, "flags": ["test"], "name": "r", "url": "a:/r", "path": ["r"], "hostname": "a", "type": "DIRECTORY", "permissions": "0777"}, null, null, {"path_string": "/u", "owner": {"group": 77, "user": 1}, "flags": ["test"], "name": "u", "url": "a:/u", "path": ["u"], "hostname": "a", "type": "DIRECTORY", "permissions": "0777"}]