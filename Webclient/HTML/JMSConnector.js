/**
 * JMSConnector connects the web client to the JMS server
 * @param host
 * @param port
 * @constructor
 */
function JMSConnector(host, port) {
    this.host = host;
    this.port = port;
    var instructions = new Array();
}


/**
 * Sends a instruction as request to the server and handles its response.
 * @param instruction the insruction that should be sended
 */
JMSConnector.prototype.sendInstruction = function(instruction) {
    $.ajax( {
        //SSL ONLY! - DISABLE SSL BY REMOVING THE 'S' IN 'HTTPS'
        url : 'https://jms.bigdatabroker.de/rest_api/v1/'
                + instruction.getURLvar(),// 'demojson.js',
        type : instruction.type,
        contentType : 'application/json',
        data : instruction.data,
        dataType : 'json',
        beforeSend : function(request) {
            if (webClientState != undefined && webClientState.authenticated) {
            var token = webClientState.getUser() + ":"
                    + webClientState.getAccessToken();
            request.setRequestHeader("X-BDB-Token", token);
            }
            window.setTimeout(function() {
                if (instruction.responseReceived == false) {

                    massiveError("Server has not responded for more than 10 seconds. Check your connection!" + instruction.getURLvar())
                }
            }, 10000);
        },
        success : function(response) {
            instruction.responseReceived = true;
            instruction.callBackFunction(response);
        },
        error : function(response, ajaxOptions, thrownError) {
            instruction.responseReceived = true;
            debugAlert("CONNECTION ERROR!\nStatus: " + response.status
                    + "\nError: " + thrownError);
            instruction.callBackErrorFunction(thrownError, response.responseText);
        }
    });
}


/**
 * Sends a instruction as request to the server without adding a token and handles its response.
 * @param instruction the insruction that should be sended
 */
JMSConnector.prototype.sendInstructionWithoutToken = function(instruction) {
    $.ajax( {
        url : 'https://jms.bigdatabroker.de/rest_api/v1/'
                + instruction.getURLvar(),// 'demojson.js',
        type : instruction.type,
        contentType : 'application/json',
        data : instruction.data,
        dataType : 'json',
        beforeSend : function(request) {
            window.setTimeout(function() {
                if (instruction.responseReceived == false) {
                    massiveError("Server has not responded for more than 10 seconds. Check your connection!")
                }
            }, 10000);
        },
        success : function(response) {

            instruction.callBackFunction(response);

        },
        error : function(response, ajaxOptions, thrownError) {
            fancyError("CONNECTION ERROR!\nStatus: " + response.status
                    + "\nError: " + thrownError);
            alert("Wrong user name or password");
        }
    });
}


/**
 * This is the mother of all instructions
 * Abstract class! Never use it!
 * @constructor
 */
function Instruction() {
    this.data = "";
    this.responseReceived = false;
}


/**
 * This method is called when the connector received the response to this instruction
 * Calls an error, because this is just an abstract method
 * @param argument
 */
Instruction.prototype.callBackFunction = function(argument) {
    //debugAlert("Instruction.prototype.callBackFunction: " + argument);
}


/**
 * This method is called when the connector received an error on this instruction
 * Calls an error, because this is just an abstract method
 * @param argument
 */
Instruction.prototype.callBackErrorFunction = function(error, response) {
    //fancyError("Instruction.prototype.callBackErrorFunction: " + error);
}


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
Instruction.prototype.getURLvar = function() {
    return "Not yet implemented";
}


/**
 * This instruction requests a JSON object from the server representing a node with its children and childrens children
 * @param node the path of the requested node
 * @param depth the depth to wich children are requested
 * @param bool placeholdr
 * @constructor
 */
function GetNodeChildren(node, depth, bool) {
    this.node = node;
    this.bool = bool;
    this.depth = 1;//depth;
    this.type = "GET";
    data = new Object();
    data["level"] = "0";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
GetNodeChildren.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
GetNodeChildren.prototype.getURLvar = function() {
    var URLvar = "node/" + encodeURIComponent(this.node.getNodeUri())
            + "/childs/?depth=" + this.depth;
    return (URLvar);
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
GetNodeChildren.prototype.callBackFunction = function(response) {
    if(this.bool){
        tree.add(response);
    }else{
        tree.callbackShowPath(response);
    }
    if(debugMode) { fancyLog("Response"); }
}


/**
 * Requests a list containing all storage servers
 * @constructor
 */
function GetAllServers(callBack) {
    this.type = "GET";
    this.callBack = callBack;
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
GetAllServers.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
GetAllServers.prototype.getURLvar = function() {
    return "server/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param response the server response
 */
GetAllServers.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    this.callBack(response);
}


/**
 * This method is called when the connector received the response to this instruction
 * @param error the HTTP error
 * @param response the detail error message created by the server
 */
GetAllServers.prototype.callBackErrorFunction = function(response) {
    $('#statusBox').text("Error " + response);
}


/**
 * Updates the properties of a storage server
 * @constructor
 */
function UpdateServer(serverID, capacity, hostname) {
    this.type = "POST";
    this.serverID = serverID;
    data = new Object();
    data["capacity"] = capacity;
    data["hostname"] = hostname;
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
UpdateServer.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
UpdateServer.prototype.getURLvar = function() {
    return "server/" + this.serverID + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param response the server response
 */
UpdateServer.prototype.callBackFunction = function(response) {
    //TODO
}


/**
 * This method is called when the connector received the response to this instruction
 * @param error the HTTP error
 * @param response the detail error message created by the server
 */
Auth.prototype.callBackErrorFunction = function(error, response) {
    fancyAlert(response);
}


/**
 * Sending user ID and passwort to the server
 * @param userID the user ID
 * @param pass the password
 * @constructor
 */
function Auth(userID, pass) {
    this.type = "POST";
    this.data = "{\"user_id\": " + userID + ", \"password\": \"" + pass + "\"}";
    this.user = userID;
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
Auth.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
Auth.prototype.getURLvar = function() {
    return "auth/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param response the server response
 */
Auth.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    auth(this.user, response, true);
}


/**
 * This method is called when the connector received the response to this instruction
 * @param error the HTTP error
 * @param response the detail error message created by the server
 */
Auth.prototype.callBackErrorFunction = function(error, response) {
    alert("AuthError: " + error);
}


/**
 * Requests a list containing all jobs from the server
 * @param bool placeholder
 * @constructor
 */
function GetJobs(bool) {
    this.type = "GET";
    this.bool = bool;
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
GetJobs.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
GetJobs.prototype.getURLvar = function() {
    return "job/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
GetJobs.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    //TODO funktion übergeben
    if(this.bool){
        drawStatus(response);        
    }else{
        findOpenJob(response);
    }
}


/**
 * Creates a new job on the server
 * @constructor
 */
function PutJob() {
    this.type = "PUT";
    this.data = "{\"priority\": 5,\"name\":\"Created with fancy Webclient\"}";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
PutJob.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
PutJob.prototype.getURLvar = function() {
    return "job/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
PutJob.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    webClientState.createJob(response);
}


/**
 * Requests all information from the server about a specific job
 * @param id the id of the job
 * @constructor
 */
function GetJob(id) {
    this.id = id;
    this.type = "GET";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
GetJob.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
GetJob.prototype.getURLvar = function() {
    return "job/" + this.id + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
GetJob.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
}


/**
 * Changes the priority of a job on the server
 * @param id the id of the server
 * @param priority the priority
 * @constructor
 */
function PostJob(id, priority) {
    this.id = id;
    this.type = "POST";
    this.data = "{\"priority\": " + priority + "}";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
PostJob.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
PostJob.prototype.getURLvar = function() {
    return "job/" + this.id + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
PostJob.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    finalizeJob(true);
}


/**
 * * Delets a job on the server
 * @param id the id of the server
 * @constructor
 */
function DeleteJob(id) {
    this.id = id;
    this.type = "DELETE";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
DeleteJob.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
DeleteJob.prototype.getURLvar = function() {
    return "job/" + this.id + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
DeleteJob.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    handleButtonClick("status");
}


/**
 * Changes the status of a job on the server from editing to waiting
 * @param id the ID of the job
 * @constructor
 */
function FinalizeJob(id) {
    this.id = id;
    this.type = "POST";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
FinalizeJob.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
FinalizeJob.prototype.getURLvar = function() {
    return "job/" + this.id + "/finalize/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
FinalizeJob.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    handleButtonClick("status");
    callbackFinalize();
    fancyLog(lang[language]["statusSended"]);
}


/**
 * This method is called when the connector received an error on this instruction
 * @param argument
 */
FinalizeJob.prototype.callBackErrorFunction = function(error, response) {
    fancyError(lang[language]["instatusfoDestinationL"] + " - " + response);
}


/**
 * Requests a list with containing all operations that belong to the given job
 * @param jobID the ID of the job
 * @constructor
 */
function GetOperations(jobID) {
    this.jobID = jobID;
    this.type = "GET";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
GetOperations.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
GetOperations.prototype.getURLvar = function() {
    return "job/" + this.jobID + "/operation/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
GetOperations.prototype.callBackFunction = function(response) {
    //debugAlertJSON(response);
    drawProceedTable(response);
    drawStatusInfo(response);
}


/**
 * Sends a new operation to the server
 * @param jobID the ID of the job the operation belongs to
 * @param type the type of the operation
 * @param source the source path
 * @param target the target path
 * @constructor
 */
function PutOperation(jobID, type, source, target) {
    this.jobID = jobID;
    this.type = "PUT";
    if (type != "DELETE") {
        this.data = "{\"source\": \"" + source.replace("/", ":/")
                + "\", \"target\": \"" + target.replace("/", ":/")
                + "\", \"type\": \"" + type + "\"}";
    } else {
        this.data = "{\"source\": \"" + source.replace("/", ":/")
                + "\", \"type\": \"" + type + "\"}";
    }
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
PutOperation.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
PutOperation.prototype.getURLvar = function() {
    return "job/" + this.jobID + "/operation/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
PutOperation.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
}


/**
 * This method is called when the connector received an error on this instruction
 * @param argument
 */
PutOperation.prototype.callBackErrorFunction = function(error, response) {
    var errorMessage = response.substr(12);
    errorMessage = errorMessage.substr(0, errorMessage.length - 2);
    fancyError("Error " + "Could not create operation: (" + errorMessage + ")");
}


/**
 * Requests all information of an operation.
 * @param jobID the ID of the job the operation belongs to
 * @param opID the ID of the operation itself
 * @constructor
 */
function GetOperation(jobID, opID) {
    this.jobID = jobID;
    this.opID = opID;
    this.type = "GET";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
GetOperation.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
GetOperation.prototype.getURLvar = function() {
    return "job/" + this.jobID + "/operation/" + this.opID + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
GetOperation.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    drawProceedTable(response);
}


/**
 * Changes one ore more details of an operation on the server
 * @param jobID the ID of the job the operation belongs to
 * @param opID the ID of the operation itself
 * @param type the operation type
 * @param source the source
 * @param target the target
 * @constructor
 */
function UpdateOperation(jobID, opID, type, source, target) {
    this.jobID = jobID;
    this.opID = opID;
    this.type = "POST";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
UpdateOperation.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
UpdateOperation.prototype.getURLvar = function() {
    return "job/" + this.jobID + "/operations/" + this.opID + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
UpdateOperation.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
}


/**
 * Deletes an operation from a job
 * @param jobID the ID of the job
 * @param opID the ID of the operation
 * @constructor
 */
function DeleteOperation(jobID, opID) {
    this.jobID = jobID;
    this.opID = opID;
    this.type = "DELETE";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
DeleteOperation.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
DeleteOperation.prototype.getURLvar = function() {
    return "job/" + this.jobID + "/operation/" + this.opID + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
DeleteOperation.prototype.callBackFunction = function(response) {
    debugAlertJSON(response);
    proceedJob();
}



/**
 * Deletes an operation from a job
 * @param jobID the ID of the job
 * @param opID the ID of the operation
 * @constructor
 */
function DeleteServer(serverID) {
    this.serverID = serverID;
    this.type = "DELETE";
}


/**
 * Inherits from Instruction
 * @type {Instruction}
 */
DeleteServer.prototype = new Instruction();


/**
 * Returnes the URL variable the connector needs to send the instruction
 * @returns {string} the URL variable
 */
DeleteServer.prototype.getURLvar = function() {
    return "server/" + this.serverID + "/";
}


/**
 * This method is called when the connector received the response to this instruction
 * @param argument
 */
DeleteServer.prototype.callBackFunction = function(response) {
    fancyLog("Deleted server");
    var request = new GetAllServers(drawServerEdit);
    connector.sendInstruction(request);
}