/**
 * This is the state of the Webclient.
 * It saves all information of an active session
 * @param user {String} The user which is logged in
 * @param accessToken {String} The accessToken is used by the webclient
 * to sign every request to the job management server.
 * @constructor
 */
function State(user, accessToken) {
        this.user = user;
        this.accessToken = accessToken;
        this.authenticated = false;
        /*
         * This is the folder that is currently loaded in the flat view
         */
        this.currentFolder = "/";
        this.currentJob;
        this.saveToCookie();
}


/**
 * If there is no job with editing status, this method will create a new one.
 * @param json the JSON response of the server, containing the job ID
 */
State.prototype.createJob = function(json) {
    this.currentJob = new Job(json.id);
    return;
}


/**
 * Returns the user which is logged in
 */
State.prototype.getUser = function() {
    return this.user;
}


/**
 * Returns the access token of the user which is logged in
 * @returns {String} the access token of the user which is logged in
 */
State.prototype.getAccessToken = function() {
    return this.accessToken;
}


/**
 * Returns the folder that is currently loaded in the flat view
 * @returns {String} the folder that is currently loaded in the flat view
 */
State.prototype.getCurrentFolder = function() {
    return this.currentFolder;
}


/**
 * Sets the folder that is currently loaded in the flat view
 * @param {String} cFolder The new current folder for the flat view as string
 */
State.prototype.setCurrentFolder = function(cFolder) {
    this.currentFolder = cFolder;
    return;
}


/**
 * Returns the job which is currently edited
 * returns {Job} the job which is currently edited
 */
State.prototype.getCurrentJob = function() {
    return this.currentJob;
}

/**
 * Saves the state to a cookie
 */
State.prototype.saveToCookie = function() {
    saveCookie("user", this.user, 10);
    saveCookie("accessToken", this.accessToken, 30);
}


/**
 * Saves the state to a cookie
 */
State.prototype.loadFromCookie = function() {
    this.user = loadCookie("user");
    this.accessToken = loadCookie("accessToken");
}


/**
 * Saves the state to a cookie
 */
State.prototype.deleteCookie = function() {
    saveCookie("user", this.user, -10);
    saveCookie("accessToken", this.accessToken, -30);
}