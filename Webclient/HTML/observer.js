function Subject () { 
    this.observers = new Array();
}

/**
* This method adds an observer to the subject.
* @param observer An observer to add.
*/
Subject.prototype.add = function(observer) {
    this.observers[this.observers.length] = observer;
    return;
};

/**
* This method removes an observer to the subject.
* @param observer An observer to remove.
*/
Subject.prototype.remove = function(observer) {
    for (var i = 0; i < observers.length; i++) {
        if(observers[i] == observers){
            observers[i] = null;
        }
    };
    return;
};

/**
* This method calls for every observer which is added the update method from the observer.
*/
Subject.prototype.notify = function() { //TODO 
    // for (var i = 0; i < this.observers.length; i++) {
    //     this.observers[i].update();
    // };
    update();
    return;
};



/**
* This class inherit from Subject and is part of the observer design pattern. It contains the state.
*/
function SpecificSubject () { 
    this.state = "";
}

SpecificSubject.prototype = new Subject();

/**
* Gets the state of the subject.
* @return The state of the Subject.
*/
SpecificSubject.prototype.getState = function() {
    return this.state;
};

/**
* Sets the state of the subject.
* @param state The state to set.
*/
SpecificSubject.prototype.setState = function(state) {
    this.state = state;
    this.notify();
    return;
};


/*
 * A new SpecificSubject
 */
var sSubject = new SpecificSubject();

/**
* This is funktion handles the clicks on the buttons
*/
function buttonHandler (id) { 
    sSubject.setState(id);
}