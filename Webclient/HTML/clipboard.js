/**
 * The job class represents the clipboard
 */
function Clipboard() {
    this.source = new Array();
    this.command;
}


/**
 * Gets the source path of the nodes.
 */
Clipboard.prototype.getSource = function() {
    return this.source;
}


/**
 * Gets the command.
 */
Clipboard.prototype.getCommand = function() {
    return this.command;
}


/**
 * Sets the source of the nodes.
 * @param source The source path in an array!
 */
Clipboard.prototype.setSource = function(source) {
    this.source = new Array();
	if (source.length == 0) {
		return;
	};

	for (var i = 0; i < source.length; i++) {
		this.source[this.source.length] = source[i];

	};
    return;
}


/**
 * Sets the command.
 * @param command the command for the source files
 */
Clipboard.prototype.setCommand = function(command) {
    this.command = command;
    return;
}


// /**
//  * only for testing
//  */
// Clipboard.prototype.alertClipboard = function() {
// 	var string = "";
//     for (var i = 0; i < this.source.length; i++) {
//     	string += this.source[i] + "\n ";
//     };
//     string += this.command.name;
//     alert(string);
//     return;
// }