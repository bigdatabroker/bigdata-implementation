import socket

from connectors import JMSAPIConnector

import random

import time

class Agent:
	def __init__(self):
		self.token = 'crphnsk4fixldzg381xmpvxgs89g4olh'

	def main(self):
		while True:
			task = self.get_task()
			if task is None:
				print("no task, waiting 5 secs.")
				time.sleep(5)
			else:
				print("received task %s" % str(task))

				self.background_execute(task)

				self.send_status('SUCCESS', task['id'])
				print("sent the status to the server")

	def register(self):
		hostname = socket.gethostname()

		connector = JMSAPIConnector()
		result = connector.send_request("PUT /server/", {
			'hostname': hostname,
			'capacity': 0
		}, None)

		self.token = result['token']

	def get_task(self):
		connector = JMSAPIConnector()
		result = connector.send_request("GET /task/", {}, self.token)

		return result

	def send_status(self, state, operation_id = None):

		fields = {
			'status': state
		}

		if operation_id is not None:
			fields['operation'] = operation_id

		connector = JMSAPIConnector()
		result = connector.send_request("POST /status/", fields, self.token)

	def send_log(self, message, is_error = False, log_type = 'server', id = None):

		fields = {
			'message': message,
			'is_error': is_error,
			'type': log_type,
			'foreign_id': id
		}

		connector = JMSAPIConnector()
		result = connector.send_request("PUT /logentry/", fields, self.token)

	def background_execute(self, task):
		secs = random.randint(1,9)
		time.sleep(secs)

		print("some time passed... lets say the task completed successfully :-)")

		return True
