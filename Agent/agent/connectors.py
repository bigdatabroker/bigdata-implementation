import http.client
import json
import collections

class JMSAPIConnector(object):

    def __init__(self):
        self._target_ip = 'jms.bigdatabroker.de'
        self._api_namespace = '/agent_api/v1/'

    def send_request(self, method, data = {}, token = None):

        request_method = method.split()[0]
        request_url = self._api_namespace + method.split()[1]

        data_dump = json.dumps(data)

        headers = {"Accept": "application/json", "Content-Type": "application/json"};

        if token is not None:
            headers['X-BDB-Agent-Token'] = token

        https_conn = http.client.HTTPSConnection(self._target_ip)
        https_conn.request(request_method, request_url, body=data_dump, headers=headers)

        response = https_conn.getresponse()

        response_body = response.read()
        response_headers = response.getheaders()
        https_conn.close()

        if response.status == 204:
            return None

        try:
            dictionary = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(response_body.decode(encoding='utf_8', errors='ignore'))
            json_dict = (json.dumps(dictionary, ensure_ascii=False, sort_keys=False, indent=4, separators=(',', ': ')))

            data_dict = json.loads(json_dict)

            return data_dict
        except Exception:
            return "JSON conversion error!"
