python manage.py runserver

redis-server


Auth

curl -X POST -H "Content-Type: application/json" -d '{"user_id": 2, "password": "hallo"}' http://127.0.0.1:8000/rest_api/v1/auth/


Connection-List

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/server/3/connection/

curl -X PUT -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -H "Content-Type: application/json" -d '{"target": 7,"protocol":"HTTP"}' http://127.0.0.1:8000/rest_api/v1/server/3/connection/

Connection-Detail

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/server/3/connection/7/HTTP/

curl -X POST -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -H "Content-Type: application/json" -d '{"speed":666}' http://127.0.0.1:8000/rest_api/v1/server/3/connection/7/HTTP/

curl -X DELETE -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/server/3/connection/7/HTTP/


JOB-List

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/

curl -X PUT -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -H "Content-Type: application/json" -d '{"priority": 5,"name":"BennosJob2"}' http://127.0.0.1:8000/rest_api/v1/job/

JOB-Detail

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/47/

curl -X POST -H "Content-Type: application/json" -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -d '{"priority": 1}' http://localhost:8000/rest_api/v1/job/7/

curl -X DELETE -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/7/

JOB-Finalize

curl -X POST -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/47/finalize/


Log-List

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/logentry/

Log-Detail

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/1/logentry/


Node-List

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/node/a%2Fb%2F%0A/childs/

Node-Detail

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/node/a%2Fb%2F%0A/


Operation-List

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/47/operation/

curl -X PUT -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -H "Content-Type: application/json" -d '{"source": "server2:/home/root", "target": "nomishe:/", "type": "COPY", "force": "True"}' http://127.0.0.1:8000/rest_api/v1/job/47/operation/

curl -X PUT -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -H "Content-Type: application/json" -d '{"source": "nomishe:/", "target": "server2:/home/root", "type": "MOVE", "force": "True"}' http://127.0.0.1:8000/rest_api/v1/job/46/operation/

curl -X PUT -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -H "Content-Type: application/json" -d '{"source": "nomishe:/", "target": "nomishe:/bigdata-design/Gantt diagram", "type": "MOVE", "force": "True"}' http://127.0.0.1:8000/rest_api/v1/job/46/operation/

Operation-Detail

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/8/operation/1/

curl -X DELETE -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/job/35/operation/4/


Server-List

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/server/

curl -X PUT -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -H "Content-Type: application/json" -d '{"token": 666, "hostname": "Server123"}' http://127.0.0.1:8000/rest_api/v1/server/

Server-Detail

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/server/1/

curl -X DELETE -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/server/1/


User-Detail

curl -X GET -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" http://127.0.0.1:8000/rest_api/v1/user/1/

curl -X POST -H "Content-Type: application/json" -H "X-BDB-Token: 2:q4tlnksgnl184by90evxbo0pk28g1qgp" -d '{"priority": 1}' http://127.0.0.1:8000/rest_api/v1/user/1/





