from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
     url(r'^rest_api/v1/', include('jms.rest_api.urls')),
     url(r'^agent_api/v1/', include('jms.agent_api.urls'))
)