#Path to the directory where the JMS puts the changed subtrees it gets from the agents
TREE_BASE_PATH = "/tmp"

JMS_INDEX_ACCESSOR_CLASS = 'jms.index.postgres.PostgresIndexAccessor'

JMS_LOGGER_CLASS = 'jms.logger.database.DatabaseLogger'

REQUIRE_SSL = False

RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0
    },
    'index': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0
    }
}

DEFAULT_RQ_TIMEOUT = 24*3600 # 1 day

TOKEN_DELETER_INTERVAL = 5 * 60 #all 5 minutes

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    ),
    'FORM_METHOD_OVERRIDE': None,
    'FORM_CONTENT_OVERRIDE': None,
    'FORM_CONTENTTYPE_OVERRIDE': None,
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DEFAULT_THROTTLE_RATES': {
        'anon': '5/minute'
    },
    'DEFAULT_AUTHENTICATION_CLASSES': []
}

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.cache.RedisCache',
        'LOCATION': '127.0.0.1:6379:1',
        'OPTIONS': {
            'PASSWORD': '',
            'PICKLE_VERSION': -1,   # default
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CLIENT_CLASS': 'redis_cache.client.DefaultClient',
        },
    },
}

#if no force parameter is sent by the client the operation check will always be executed
#(False means "do not force creating operations with invalid paths")
DEFAULT_OPERATION_CHECK_BEHAVIOUR = False

DEFAULT_PLAUSIBILITY_CHECKER_CLASSES = ('jms.scheduler.plausibility.SimplePlausibilityChecker', 'jms.scheduler.plausibility.PermissionChecker', 'jms.scheduler.plausibility.InverseChecker')

DEFAULT_OPTIMIZER_CLASSES = ('jms.scheduler.optimizer.GroupOptimizer',)

SCHEDULER_CLASSES = {'micro' : 'jms.scheduler.scheduler.SimpleMicroScheduler',
                     'macro' : 'jms.scheduler.scheduler.SimpleMacroScheduler' }

API_NODE_MAX_REQUEST_DEPTH = 3 # Max depth of the tree the user may request.
API_NODE_MAX_FORCED_DEPTH = 2 # the max depth that will be returned from the API. Example: max request depth 3, max forced depth 2. Requests with depth=3 would be valid, but the server will return 2 levels at max

