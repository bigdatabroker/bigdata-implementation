from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
import json
from django.conf import settings
from jms.models import Server, User, Operation, Job, Event, LogEntry, AccessToken
from datetime import datetime
from jms.utils import get_proper_date

class AgentAPISTaskTestcase(TestCase):
    def setUp(self):        
        self.client = APIClient()
        settings.REQUIRE_SSL = False

        #get token
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)        
        content = json.loads(response.content)
        self.token = content["token"]

        #set active
        server = Server.objects.get(token=self.token)
        server.is_active = True
        server.save()
        self.server = server

        #create required objects
        user1 = User(user_id = 1, name="hans")
        user1.priority = 1
        user1.save()
        self.user1 = user1

        user2 = User(user_id = 2, name="peter")
        user2.priority = 2
        user2.save()
        self.user2 = user2

        job1 = Job(name="testjob1", user_id = user1.user_id)
        job1.status = Job.READY        
        job1.priority = 2
        job1.save()
        self.job1 = job1

        job2 = Job(name="testjob2", user_id = user2.user_id)
        job2.status = Job.READY        
        job2.priority = 3
        job2.save()
        self.job2 = job2

        #prio = 1 * 2 = 2
        op1 = Operation(job_id=job1.id, op_type=Operation.DELETE, source="test:/dev/null")
        op1.status = Operation.WAITING
        op1.start = get_proper_date(datetime.now())
        op1.save()
        self.op1 = op1

        #prio = 2 * 3 = 6
        op2 = Operation(job_id=job2.id, op_type=Operation.DELETE, source="test:/dev/zero")
        op2.status = Operation.WAITING
        op2.start = get_proper_date(datetime.now())
        op2.save()
        self.op2 = op2

    def test_get_task_without_token(self):
        response = self.client.get('/agent_api/v1/task/', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "please authenticate with an access token!")


    def test_get_task_with_invalid_token(self):
        response = self.client.get('/agent_api/v1/task/', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN="invalid")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "Unknown access token!")
        
    def test_get_task(self):
        response = self.client.get('/agent_api/v1/task/', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.content)
        self.assertEqual(res["id"], self.op2.id) #because 2 has higher prio
        job = Job.objects.get(pk=res["job"])
        self.assertEqual(job.status, Job.RUNNING)
        operation = Operation.objects.get(pk=res["id"])
        self.assertEqual(operation.status, Operation.RUNNING)
        self.assertNotEqual(operation.start, None)
        self.assertEqual(operation.end, None)
        self.assertNotEqual(Event.get_event(job, "START"), None)
        self.assertEqual(Event.get_event(job, "END"), None)

    def test_get_both_tasks(self):
        response = self.client.get('/agent_api/v1/task/', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.content)
        self.assertEqual(res["id"], self.op2.id) #because 2 has higher prio

        response = self.client.get('/agent_api/v1/task/', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.content)
        self.assertEqual(res["id"], self.op1.id) 

    def test_get_task_as_inactive_server(self):
        self.server.is_active = False
        self.server.save()

        response = self.client.get('/agent_api/v1/task/', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "Server has to be activated before get tasks.")        

        self.server.is_active = True
        self.server.save()

    def test_get_task_where_no_tasks_are(self):
        self.op1.source = "INVALID"
        self.op1.save()
        self.op2.source = "INVALID"
        self.op2.save()

        response = self.client.get('/agent_api/v1/task/', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 204)