# coding: utf8
from django.test import TestCase
from jms.utils import *
from datetime import datetime

class StatParserTestCase(TestCase):
    def setUp(self):
        self.parser = StatParser()

    def test_StatParsers_parse_line(self):
        self.assertEqual(self.parser.parse_line("abcd"), False)

    def test_StatParsers_get_result(self):
        self.assertEqual(self.parser.parse_line("abcd"), False)
        self.assertEqual(self.parser.get_result(), None)

    def test_StatParsers_get_type(self):
        self.assertEqual(self.parser.get_type("-"), 'FILE')
        self.assertEqual(self.parser.get_type("d"), 'DIRECTORY')
        self.assertEqual(self.parser.get_type("12345"), 'OTHER')

    def test_StatParsers_rwx_to_oct(self):
        self.assertEqual(self.parser.rwx_to_oct("----------"), 0)
        self.assertEqual(self.parser.rwx_to_oct("---x--x--x"), 36)
        self.assertEqual(self.parser.rwx_to_oct("--w--w--w-"), 73)
        self.assertEqual(self.parser.rwx_to_oct("-rwxrwxrwx"), 255)
        self.assertEqual(self.parser.rwx_to_oct("drwxrwxrws"), 255)
        self.assertEqual(self.parser.rwx_to_oct("drwxrwxrwt"), 255)

    def test_StatParsers_get_datetime(self):
        self.assertEqual(self.parser.get_datetime("abcd"), None)
        self.assertEqual(self.parser.get_datetime("2013-08-10 13:00:00"), datetime(2013, 8, 10, 13, 0))
        self.assertEqual(self.parser.get_datetime("0000-00-00 00:00:00"), None)

    def test_wrong_format(self):
        res = self.parser.parse_line('dr-xr-Zr-x 4096 0 0 "?" "2013-07-10 11:02:43.610025354" "2013-06-18 10:01:59.973000001" "2013-06-18 10:01:59.973000001" "„/boot“"')
        self.assertFalse(res)
        self.assertEqual(self.parser.get_result(), None)

    def test_correct_format(self):
        res = self.parser.parse_line('dr-xr-wr-x 4096 0 0 "?" "2013-07-10 11:02:43.610025354" "2013-06-18 10:01:59.973000001" "2013-06-18 10:01:59.973000001" "„/boot“"')
        self.assertTrue(res)
        res = self.parser.get_result()
        self.assertEqual(int(res["owner_group"]), 0)
        self.assertEqual(int(res["owner_user"]), 0)
        self.assertTrue(res["name"].startswith("boot"))
        self.assertEqual(int(res["permissions_oct"]), 365)
        self.assertEqual(len(res["times"]), 4)
        self.assertEqual(res["times"]["birth"], None)
        self.assertNotEqual(res["times"]["last_access"], None)
        self.assertNotEqual(res["times"]["last_modification"], None)
        self.assertNotEqual(res["times"]["last_change"], None)
        self.assertEqual(int(res["size"]), 4096)
        self.assertEqual(res["type"], "DIRECTORY")
        self.assertEqual(res["permissions"], "dr-xr-wr-x")

class TokenGeneratorTestCase(TestCase):
    def test_simpleToken(self):
        generator = TokenGenerator(5, 'a')
        self.assertEqual(generator.get_token(), 'aaaaa')
        self.assertEqual(len(generator.get_token()), 5)


    def test_token(self):
        generator = TokenGenerator(50, '01')
        tok = generator.get_token()
        self.assertEqual(len(tok), 50)
        for c in tok:
            self.assertTrue(c in ['0', '1'])


    def test_token2(self):
        ran = [chr(x) for x in xrange(ord('a'), ord('z'))]
        generator = TokenGenerator(5, ran)
        tok = generator.get_token()
        self.assertEqual(len(tok), 5)
        for c in tok:
            self.assertTrue(c in ran)