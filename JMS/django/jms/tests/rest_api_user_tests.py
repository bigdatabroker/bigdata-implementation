from django.test import TestCase
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import User
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings

from rest_framework.parsers import JSONParser

import json

no_admin_id = 1
admin_id = 2

class RESTAPIUserTestCase(TestCase):
    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": admin_id, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.admin_token = auth_handler.get_access_token()

        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": no_admin_id, "password": "password"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.no_admin_token = auth_handler.get_access_token()

    #test no token

    def test_get_user_detail_no_token(self):
        response = self.client.get('/rest_api/v1/user/42/')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_post_user_detail_no_token(self):
        response = self.client.post('/rest_api/v1/user/42/', '{"priority": 1}', content_type='application/json')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    # test invalid token

    def test_get_user_list_invalid_token(self):
        response = self.client.get('/rest_api/v1/user/42/', HTTP_X_BDB_TOKEN= "2:abcd")

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_post_user_list_invalid_token(self):
        response = self.client.post('/rest_api/v1/user/42/', '{"priority": 1}', content_type='application/json', HTTP_X_BDB_TOKEN= "2:abcd")
        
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    #test the API

    #UserDetail

    def test_get_user_detail_correct(self):
        user = User()
        user.user_id=42
        user.is_admin=True
        user.priority=1
        user.name="TestUser"
        user.save()

        response = self.client.get('/rest_api/v1/user/42/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rUser = json.loads(response.content)
        self.assertEqual(rUser["name"], user.name)
        self.assertEqual(rUser["priority"], user.priority)

    def test_get_user_detail_no_admin(self):
        user = User()
        user.user_id=42
        user.is_admin=True
        user.priority=1
        user.name="TestUser"
        user.save()

        response = self.client.get('/rest_api/v1/user/42/', HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Admin rights required."}')

    def test_post_user_detail_correct(self):
        user = User()
        user.user_id=42
        user.is_admin=True
        user.priority=1
        user.name="TestUser"
        user.save()

        response = self.client.post('/rest_api/v1/user/42/', '{"priority": 1}', content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rUser = json.loads(response.content)
        self.assertEqual(rUser["name"], user.name)
        self.assertEqual(rUser["priority"], user.priority)

        dUser = User.objects.get(user_id=user.user_id)
        self.assertEqual(dUser.name, user.name)
        self.assertEqual(dUser.priority, user.priority)

    def test_post_user_detail_no_admin(self):
        user = User()
        user.user_id=42
        user.is_admin=True
        user.priority=1
        user.name="TestUser"
        user.save()

        response = self.client.post('/rest_api/v1/user/42/', '{"priority": 1}', content_type='application/json', 
            HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Admin rights required."}')
