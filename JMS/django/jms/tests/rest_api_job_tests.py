from django.test import TestCase
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import User, Job, Operation
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings

from rest_framework.parsers import JSONParser

import json

user_id = 2

class RESTAPIJobTestCase(TestCase):
    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.token = auth_handler.get_access_token()

    #test no token

    def test_get_job_list_no_token(self):
        response = self.client.get('/rest_api/v1/job/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_put_job_list_no_token(self):
        response = self.client.put('/rest_api/v1/job/', data='{"priority": 5,"name":"BennosJob2"}', 
            content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_get_job_detail_no_token(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.get('/rest_api/v1/job/'+ str(job1.id) + '/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_post_job_detail_no_token(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/', data='{"priority": 1,"name":"job2"}', content_type='application/json',
            follow=False)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_delete_job_detail_no_token(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()
        self.assertEqual(Job.objects.get(id=job1.id), job1)

        response = self.client.delete('/rest_api/v1/job/'+ str(job1.id) + '/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    #test incorrect token

    def test_get_job_list_invalid_token(self):
        response = self.client.get('/rest_api/v1/job/', HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_put_job_list_invalid_token(self):
        response = self.client.put('/rest_api/v1/job/', data='{"priority": 5,"name":"BennosJob2"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:abcd", follow=False)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_get_job_detail_invalid_token(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.get('/rest_api/v1/job/'+ str(job1.id) + '/', HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_post_job_detail_invalid_token(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()
        
        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/', data='{"priority": 1,"name":"job2"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:abcd", follow=False)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_delete_job_detail_invalid_token(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.delete('/rest_api/v1/job/'+ str(job1.id) + '/', HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    #test the API

    #JobList

    def test_get_job_list(self):
        response = self.client.get('/rest_api/v1/job/', HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 200) # OK
        self.assertEqual(response.content, '[]') #empty list

        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.get('/rest_api/v1/job/', HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 200) # OK
        rJob1 = (json.loads(response.content))[0]
        self.assertEqual(rJob1['id'], job1.id)
        self.assertEqual(rJob1["user"], job1.user_id)
        self.assertEqual(rJob1["status"], job1.get_status_as_string())
        self.assertEqual(rJob1["name"], job1.name)
        self.assertEqual(rJob1["priority"], job1.priority)

    def test_put_job_list_correct(self):
        response = self.client.put('/rest_api/v1/job/', data='{"priority": 5,"name":"job1"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 201) # CREATED
        rJob1 = json.loads(response.content)
        rJob1ID = rJob1["id"]
        self.assertEqual(rJob1["user"], user_id)
        self.assertEqual(rJob1["status"], 'EDITING')
        self.assertEqual(rJob1["name"], 'job1')
        self.assertEqual(rJob1["priority"], 5)

        dJob1 = Job.objects.get(id=rJob1ID)
        self.assertEqual(dJob1.user_id, user_id)
        self.assertEqual(dJob1.get_status_as_string(), 'EDITING')
        self.assertEqual(dJob1.name, 'job1')
        self.assertEqual(dJob1.priority, 5)

    def test_put_job_list_miss_data(self):
        response = self.client.put('/rest_api/v1/job/', HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "No parametes or too many parameters. Only priority and name"}')

    def test_put_job_list_miss_priority(self):
        response = self.client.put('/rest_api/v1/job/', data='{"name":"job1"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 201) # CREATED
        rJob1 = json.loads(response.content)
        rJob1ID = rJob1["id"]
        self.assertEqual(rJob1["user"], user_id)
        self.assertEqual(rJob1["status"], 'EDITING')
        self.assertEqual(rJob1["name"], 'job1')
        self.assertEqual(rJob1["priority"], 1)

        dJob1 = Job.objects.get(id=rJob1ID)
        self.assertEqual(dJob1.user_id, user_id)
        self.assertEqual(dJob1.get_status_as_string(), 'EDITING')
        self.assertEqual(dJob1.name, 'job1')
        self.assertEqual(dJob1.priority, 1)

    def test_put_job_list_miss_name(self):
        response = self.client.put('/rest_api/v1/job/', data='{"priority": 5}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"name": ["This field is required."]}')

    def test_put_job_list_incorrect_priority(self):
        response = self.client.put('/rest_api/v1/job/', data='{"priority": "abc", "name":"job1"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"priority": ["Select a valid choice. abc is not one of the available choices."]}')

    #JobDetail

    def test_get_job_detail(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.get('/rest_api/v1/job/'+ str(job1.id) + '/', HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 200) # OK
        rJob1 = json.loads(response.content)
        self.assertEqual(rJob1['id'], job1.id)
        self.assertEqual(rJob1["user"], job1.user_id)
        self.assertEqual(rJob1["status"], job1.get_status_as_string())
        self.assertEqual(rJob1["name"], job1.name)
        self.assertEqual(rJob1["priority"], job1.priority)

    def test_post_job_detail_correct(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/', data='{"priority": 1,"name":"job2"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 200) # OK
        rJob1 = json.loads(response.content)
        self.assertEqual(rJob1['id'], job1.id)
        self.assertEqual(rJob1["user"], job1.user_id)
        self.assertEqual(rJob1["status"], job1.get_status_as_string())
        self.assertEqual(rJob1["name"], "job2")
        self.assertEqual(rJob1["priority"], 1)

    def test_post_job_detail_miss_data(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/', HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "No parameters or too many parameters. Only priority and name"}')

    def test_post_job_detail_incorrect_priority(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/', data='{"priority": "abc"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"priority": ["Select a valid choice. abc is not one of the available choices."]}')

    def test_delete_job_detail_correct(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()
        job1ID = job1.id
        self.assertEqual(Job.objects.get(id=job1.id), job1)

        response = self.client.delete('/rest_api/v1/job/'+ str(job1.id) + '/', HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 204) # no content
        listJob = Job.objects.filter(id=job1ID)
        self.assertEqual(len(listJob), 0)

    def test_delete_job_detail_incorrect_job_status(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.set_status_by_string("RUNNING")
        job1.save()
        job1ID = job1.id
        self.assertEqual(Job.objects.get(id=job1.id), job1)

        response = self.client.delete('/rest_api/v1/job/'+ str(job1.id) + '/', HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "Job cannot be deleted anymore!"}')

    #JobFinalize

    def test_post_job_finalize_correct(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/finalize/', 
            HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 200) # OK 
        self.assertEqual(response.content, '{"detail": "Job is finalized now."}')    

    def test_post_job_finalize_incorrect_job_status(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.set_status_by_string("RUNNING")
        job1.save()

        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/finalize/', HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "Job is not finalizable anymore!"}')  

    def test_post_job_finalize_incorrect_operation(self):
        job1 = Job(user_id= user_id, name='job1', priority= 5)
        job1.save()

        op1 = Operation(job_id= job1.id, source= "server2:/home/", target="server2:/home/root/", op_type= 0)
        op1.save()

        response = self.client.post('/rest_api/v1/job/'+ str(job1.id) + '/finalize/', HTTP_X_BDB_TOKEN= "2:" + self.token.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"'+ str(op1.id) + '": "criticial error: A folder can not be moved or copied to one of its subfolders."}')    


