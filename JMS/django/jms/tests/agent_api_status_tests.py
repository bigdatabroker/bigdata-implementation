from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
import json
from django.conf import settings
from jms.models import Server, User, Operation, Job, Event, LogEntry, AccessToken
from datetime import datetime
from jms.utils import get_proper_date

class AgentAPIStatusTestcase(TestCase):
    def setUp(self):        
        self.client = APIClient()
        settings.REQUIRE_SSL = False

        #get token
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)        
        content = json.loads(response.content)
        self.token = content["token"]

        #set active
        server = Server.objects.get(token=self.token)
        server.is_active = True
        server.save()
        self.server = server

        #create required objects
        user = User(user_id = 1, name="hans")
        user.save()

        token = AccessToken()
        token.user = user
        token.generate_token()
        token.data = "TESTDATA"
        token.token = "tokenstring"
        token.expires_on = get_proper_date(datetime.now())
        token.save()

        job = Job(name="testjob", user_id = user.user_id)
        job.status = Job.RUNNING
        job.token = token
        job.save()
        self.job = job

        op = Operation(job_id=job.id, op_type=Operation.DELETE, source="test:/dev/null")
        op.status = Operation.RUNNING
        op.start = get_proper_date(datetime.now())
        op.save()
        self.op = op


    def test_post_status_without_token(self):
        response = self.client.post('/agent_api/v1/status/', data='{"status" : "SUCCESS", "operation" : %d}' % self.op.id, content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "please authenticate with an access token!")


    def test_post_status_with_invalid_token(self):
        response = self.client.post('/agent_api/v1/status/', data='{"status" : "SUCCESS", "operation" : %d}' % self.op.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN="invalid")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "Unknown access token!")


    def test_post_status_with_inactive_server(self):        
        self.server.is_active = False
        self.server.save()

        response = self.client.post('/agent_api/v1/status/', data='{"status" : "SUCCESS", "operation": %d}' % self.op.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)        
        self.assertEqual(response.status_code, 403)     
        self.assertEqual(json.loads(response.content)["detail"], "Server has to be activated before it updates its state.")

        self.server.is_active = True
        self.server.save() 


    def test_post_status_without_data(self):
        response = self.client.post('/agent_api/v1/status/', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "no body found")


    def test_post_status_with_wrong_data(self):
        response = self.client.post('/agent_api/v1/status/', data='{"blaa" : "bla"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Operation and status needed!")


    def test_post_status_with_correct_data(self):
        response = self.client.post('/agent_api/v1/status/', data='{"status" : "SUCCESS", "operation": %d}' % self.op.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)        
        self.assertEqual(response.status_code, 200)
        self.op = Operation.objects.get(pk=self.op.id)
        
        event = Event.get_event(self.op.job, "END")
        self.assertNotEqual(event, None)
        self.assertEqual(event.job, self.op.job)
        self.assertNotEqual(self.op.end, None)
        self.assertEqual(event.job.status, Job.SUCCESS)
        self.assertEqual(self.op.status, Operation.SUCCESS)


    def test_post_status_with_correct_data_many_operations(self):
        op = Operation(job_id=self.job.id, op_type=Operation.DELETE, source="test:/dev/null")
        op.status = Operation.WAITING
        op.start = get_proper_date(datetime.now())
        op.save()

        response = self.client.post('/agent_api/v1/status/', data='{"status" : "SUCCESS", "operation": %d}' % self.op.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)        
        self.assertEqual(response.status_code, 200)
        self.op = Operation.objects.get(pk=self.op.id)
        
        event = Event.get_event(self.op.job, "END")
        self.assertEqual(event, None) #not all operations completed!        
        self.assertEqual(op.end, None)
        self.assertNotEqual(self.op.end, None)
        self.assertEqual(op.job.status, Job.RUNNING) #not all operations completed!
        self.assertEqual(self.op.status, Operation.SUCCESS)
        self.assertEqual(op.status, Operation.WAITING)


    def test_post_status_invalid_operation(self):
        op = Operation(job_id=self.job.id, op_type=Operation.DELETE, source="INVALIDSOURCEPATH:/dev/null")
        op.status = Operation.WAITING
        op.start = get_proper_date(datetime.now())
        op.save()

        response = self.client.post('/agent_api/v1/status/', data='{"status" : "SUCCESS", "operation": %d}' % op.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)        
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Operation cannot be executed by this agent.")


    def test_post_status_token_deleted(self):        
        self.assertNotEqual(self.job.token, None)
        self.client.post('/agent_api/v1/status/', data='{"status" : "SUCCESS", "operation": %d}' % self.op.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)           
        self.job = Job.objects.get(pk=self.job.id)        
        self.assertIsNone(self.job.token)
