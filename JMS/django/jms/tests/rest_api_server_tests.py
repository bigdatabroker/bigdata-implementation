from django.test import TestCase
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import Server
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings

from rest_framework.parsers import JSONParser

import json

no_admin_id = 1
admin_id = 2

class RESTAPIServerTestCase(TestCase):
    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.admin_token = auth_handler.get_access_token()

        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": no_admin_id, "password": "password"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.no_admin_token = auth_handler.get_access_token()


    #test no token

    def test_get_server_list_no_token(self):
        response = self.client.get('/rest_api/v1/server/')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_get_server_detail_no_token(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.get('/rest_api/v1/server/' + str(server.id) + '/')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_delete_server_detail_no_token(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.delete('/rest_api/v1/server/' + str(server.id) + '/')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    # test invalid token

    def test_get_server_list_invalid_token(self):
        response = self.client.get('/rest_api/v1/server/', HTTP_X_BDB_TOKEN= "2:abcd")

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_get_server_detail_invalid_token(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.get('/rest_api/v1/server/' + str(server.id) + '/', HTTP_X_BDB_TOKEN= "2:abcd")
        
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_delete_server_detail_invalid_token(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.delete('/rest_api/v1/server/' + str(server.id) + '/', HTTP_X_BDB_TOKEN= "2:abcd")
        
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    #test the API

    #ServerList

    def test_get_server_list(self):
        response = self.client.get('/rest_api/v1/server/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        self.assertEqual(response.content, '[]') #empty list

        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.get('/rest_api/v1/server/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rServer = (json.loads(response.content))[0]
        self.assertEqual(rServer["id"], server.id)
        self.assertEqual(rServer["hostname"], server.hostname)
        self.assertEqual(rServer["capacity"], server.capacity)

    # Server-Detail

    def test_get_server_detail_correct(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.get('/rest_api/v1/server/' + str(server.id) + '/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rServer = (json.loads(response.content))
        self.assertEqual(rServer["id"], server.id)
        self.assertEqual(rServer["hostname"], server.hostname)
        self.assertEqual(rServer["capacity"], server.capacity)

    def test_get_server_detail_no_admin(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.get('/rest_api/v1/server/' + str(server.id) + '/', HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Admin rights are required."}')

    def test_delete_server_detail_correct(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.delete('/rest_api/v1/server/' + str(server.id) + '/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 204) # no content
        listServer = Server.objects.filter(hostname=server.hostname)
        self.assertEqual(len(listServer), 0)

    def test_delete_server_detail_no_admin(self):
        server = Server()
        server.hostname = 'TestServer'
        server.capacity = 100
        server.is_active = True
        server.generate_token()
        server.save()

        response = self.client.delete('/rest_api/v1/server/' + str(server.id) + '/', HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Admin rights are required."}')

