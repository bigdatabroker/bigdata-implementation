from django.test import TestCase

from jms.index.utils import *
from jms.exception.index import *
from jms.index.postgres import *
from jms.models import *
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.parsers import JSONParser
from rest_framework.request import Request
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler

import json

class IndexUtilsTestCase(TestCase):

  def test_url_parser_with_invalid_urls(self):
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url(''))
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url('/x/y/'))
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url('x/y'))
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url('xy'))
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url(':'))
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url('/:'))
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url(':/'))
    self.assertRaises(InvalidURLSyntaxException, lambda: parse_url(':/xy/yz'))

    #self.assertRaises(InvalidURLSyntaxException, lambda: parse_url(':blubb'))

  def test_url_parser_with_valid_urls(self):
    res = parse_url('valid:/')

    self.assertEquals(len(res), 1)
    self.assertEquals(res[0], 'valid')

    res = parse_url('valid:/  ')

    self.assertEquals(len(res), 2)
    self.assertEquals(res[0], 'valid')
    self.assertEquals(res[1], '  ')

    res = parse_url('1:/2/4:5/')
    self.assertEquals(len(res), 3)
    self.assertEquals(res[0], '1')
    self.assertEquals(res[1], '2')
    self.assertEquals(res[2], '4:5')

    res = parse_url('1:/2/4:5')
    self.assertEquals(len(res), 3)
    self.assertEquals(res[0], '1')
    self.assertEquals(res[1], '2')
    self.assertEquals(res[2], '4:5')

  def test_perms_parser_all_perms(self):

    res = parse_perms(int('111111111', 2))

    self.assertEquals(type(res), dict)
    self.assertEquals(len(res), 3)

    for t in ['user', 'group', 'other']:
      for p in ['r', 'w', 'x']:
        self.assertTrue(p in res[t])

  def test_perms_parser_no_perms(self):

    res = parse_perms(int('000000000', 2))

    self.assertEquals(type(res), dict)
    self.assertEquals(len(res), 3)

    for t in ['user', 'group', 'other']:
      for p in ['r', 'w', 'x']:
        self.assertFalse(p in res[t])

  def test_perms_parser_single_perms(self):

    res = parse_perms(int('000001000', 2))

    self.assertEquals(type(res), dict)
    self.assertEquals(len(res), 3)

    for t in ['user', 'group', 'other']:
      for p in ['r', 'w', 'x']:
        if not (t == 'group' and p == 'x'):
          self.assertFalse(p in res[t])

    self.assertTrue('x' in res['group'])



    res = parse_perms(int('010000000', 2))

    self.assertEquals(type(res), dict)
    self.assertEquals(len(res), 3)

    for t in ['user', 'group', 'other']:
      for p in ['r', 'w', 'x']:
        if not (t == 'user' and p == 'w'):
          self.assertFalse(p in res[t])

    self.assertTrue('w' in res['user'])

class PostgresIndexAccessorTestCase(TestCase):

    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.token = auth_handler.get_access_token()

        self.adminAccessor = PostgresIndexAccessor()
        self.adminAccessor.enter_global_mode()

        if self.adminAccessor.has_node(['TestServer']):
            node = self.adminAccessor.get_node(['TestServer'])
            self.adminAccessor.delete_node(node)

        self.assertFalse(self.adminAccessor.has_node(['TestServer'])) # testing the test

        self.server = Server()
        self.server.hostname = 'TestServer'
        self.server.capacity = 1000000
        self.server.is_active = True
        self.server.generate_token()
        self.server.save()

    def test_permission_enforcing(self):
        accessor = PostgresIndexAccessor()
        self.assertRaises(NoPermissionsException, lambda: accessor.add_flag(None, None))
        self.assertRaises(NoPermissionsException, lambda: accessor.remove_flag(None, None))
        self.assertRaises(NoPermissionsException, lambda: accessor.add_node(None, None))
        self.assertRaises(NoPermissionsException, lambda: accessor.update_node(None, None))
        self.assertRaises(NoPermissionsException, lambda: accessor.delete_node(None))

    def _setup_simple_deep_tree(self):
        accessor = self.adminAccessor

        root_node = accessor.get_node([self.server.hostname])

        accessor.add_node(root_node.url, {
            'name': 'sub_node1',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })
        sub_node1 = accessor.get_node([self.server.hostname, 'sub_node1'])

        accessor.add_node(sub_node1.url, {
            'name': 'sub_node2',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })
        sub_node2 = accessor.get_node([self.server.hostname, 'sub_node1', 'sub_node2'])

        accessor.add_node(sub_node1.url, {
            'name': 'sub_node2_2',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })

        accessor.add_node(sub_node2.url, {
            'name': 'sub_node3',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })
        sub_node3 = accessor.get_node([self.server.hostname, 'sub_node1', 'sub_node2', 'sub_node3'])

        accessor.add_node(sub_node3.url, {
            'name': 'sub_node4',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
              'type': 'FILE',
          })

    def test_local_flags(self):
        self._setup_simple_deep_tree()

        accessor = PostgresIndexAccessor(self.token)

        node = accessor.get_node('TestServer:/sub_node1')

        self.assertEquals(type(node.flags), list)
        self.assertEquals(len(node.flags), 0)

        self.adminAccessor.add_flag(node, '~localonly')

        node = accessor.get_node('TestServer:/sub_node1')

        self.assertEquals(type(node.flags), list)
        self.assertEquals(len(node.flags), 1)
        self.assertTrue('localonly' in node.flags)

        subsequent_node = accessor.get_node('TestServer:/sub_node1/sub_node2')

        self.assertEquals(len(subsequent_node.flags), 0)

    def test_recursive_flags(self):
        self._setup_simple_deep_tree()

        accessor = PostgresIndexAccessor(self.token)

        node = accessor.get_node('TestServer:/sub_node1')

        self.assertEquals(type(node.flags), list)
        self.assertEquals(len(node.flags), 0)

        self.adminAccessor.add_flag(node, '+rec')

        node = accessor.get_node('TestServer:/sub_node1')

        self.assertEquals(type(node.flags), list)
        self.assertEquals(len(node.flags), 1)
        self.assertTrue('rec' in node.flags)

        subsequent_node = accessor.get_node('TestServer:/sub_node1/sub_node2')

        self.assertEquals(len(subsequent_node.flags), 1)
        self.assertTrue('rec' in subsequent_node.flags)

        subsequent_node = accessor.get_node('TestServer:/sub_node1/sub_node2_2')

        self.assertEquals(len(subsequent_node.flags), 1)
        self.assertTrue('rec' in subsequent_node.flags)

        subsequent_node = accessor.get_node('TestServer:/sub_node1/sub_node2/sub_node3')

        self.assertEquals(len(subsequent_node.flags), 1)
        self.assertTrue('rec' in subsequent_node.flags)

    def test_recursive_deleting_flags(self):
        self._setup_simple_deep_tree()

        accessor = PostgresIndexAccessor(self.token)

        node = accessor.get_node('TestServer:/sub_node1')

        self.assertEquals(type(node.flags), list)
        self.assertEquals(len(node.flags), 0)

        self.adminAccessor.add_flag(node, '+rec')

        node = accessor.get_node('TestServer:/sub_node1')

        self.assertEquals(type(node.flags), list)
        self.assertEquals(len(node.flags), 1)
        self.assertTrue('rec' in node.flags)


        subsequent_node = accessor.get_node('TestServer:/sub_node1/sub_node2')
        self.adminAccessor.add_flag(subsequent_node, '-rec')
        subsequent_node = accessor.get_node('TestServer:/sub_node1/sub_node2')

        self.assertEquals(len(subsequent_node.flags), 0)
        self.assertFalse('rec' in subsequent_node.flags)

        subsequent_neighbour_node = accessor.get_node('TestServer:/sub_node1/sub_node2_2')

        self.assertEquals(type(subsequent_neighbour_node.flags), list)
        self.assertEquals(len(subsequent_neighbour_node.flags), 1)
        self.assertTrue('rec' in subsequent_neighbour_node.flags)

        subsequent_node = accessor.get_node('TestServer:/sub_node1/sub_node2/sub_node3')
        self.assertEquals(len(subsequent_node.flags), 0)
        self.assertFalse('rec' in subsequent_node.flags)

    def test_enter_global_mode(self):
        self._setup_simple_deep_tree()

        accessor = PostgresIndexAccessor(self.token)

        node = accessor.get_node('TestServer:/sub_node1')
        self.assertRaises(NoPermissionsException, lambda: accessor.add_flag(node, '.xy'))
        accessor.enter_global_mode()
        self.assertRaises(InvalidFlagException, lambda: accessor.add_flag(node, '.xy'))

    def test_get_parent_node(self):
        self._setup_simple_deep_tree()

        accessor = PostgresIndexAccessor(self.token)

        node = accessor.get_node('TestServer:/sub_node1/sub_node2/sub_node3')
        parent_node = accessor.get_node('TestServer:/sub_node1/sub_node2')

        parent_from_accessor = accessor.get_parent(node)

        self.assertEquals(parent_node.url, parent_from_accessor.url)

    def test_invalid_flags(self):
        self._setup_simple_deep_tree()

        accessor = PostgresIndexAccessor()
        accessor.enter_global_mode()

        node = accessor.get_node('TestServer:/sub_node1')

        self.assertRaises(InvalidFlagException, lambda: accessor.add_flag(node, '.xxx'))
        self.assertRaises(InvalidFlagException, lambda: accessor.add_flag(node, 'xxx'))
        self.assertRaises(InvalidFlagException, lambda: accessor.add_flag(node, '#xxx'))
