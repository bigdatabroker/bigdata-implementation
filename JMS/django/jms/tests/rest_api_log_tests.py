from django.test import TestCase
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import LogEntry, Job
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings

from rest_framework.parsers import JSONParser

import json

no_admin_id = 1
admin_id = 2

class RESTAPILogTestCase(TestCase):
    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": admin_id, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.admin_token = auth_handler.get_access_token()

        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": no_admin_id, "password": "password"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.no_admin_token = auth_handler.get_access_token()

    # test no token

    def test_get_log_list_no_token(self):
        response = self.client.get('/rest_api/v1/logentry/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_get_log_detail_no_token(self):
        job1 = Job(user_id= admin_id, name='job1', priority= 5)
        job1.save()

        response = self.client.get('/rest_api/v1/job/' + str(job1.id) + '/logentry/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    #test invalid token

    def test_get_log_list_invalid_token(self):
        response = self.client.get('/rest_api/v1/logentry/', HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_get_log_detail_invalid_token(self):
        job1 = Job(user_id= admin_id, name='job1', priority= 5)
        job1.save()

        response = self.client.get('/rest_api/v1/job/' + str(job1.id) + '/logentry/', HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    # test the API

    # LogList

    def test_get_log_list(self):
        response = self.client.get('/rest_api/v1/logentry/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        self.assertEqual(response.content, '[]') #empty list

        log = LogEntry(message='test')
        log.save()

        response = self.client.get('/rest_api/v1/logentry/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rLog = (json.loads(response.content))[0]
        self.assertEqual(rLog["message"], 'test')

    # Log-Detail

    def test_get_log_detail_correct(self):
        job1 = Job(user_id= admin_id, name='job1', priority= 5)
        job1.save()

        log = LogEntry(foreign_obj= job1, message='test')
        log.save()

        response = self.client.get('/rest_api/v1/job/' + str(job1.id) + '/logentry/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rLog = json.loads(response.content)[0]
        self.assertEqual(rLog["message"], log.message)
        self.assertEqual(rLog["foreign_id"], job1.id)
        self.assertEqual(rLog["type"], 'job')

    def test_get_log_detail_no_admin(self):
        job1 = Job(user_id= admin_id, name='job1', priority= 5)
        job1.save()

        log = LogEntry(foreign_obj= job1, message='test')
        log.save()

        response = self.client.get('/rest_api/v1/job/' + str(job1.id) + '/logentry/', 
            HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "No permissions for this job."}')