from django.test import TestCase
from jms.rest_api.views.bigdatabrokerapiview import BigDataBrokerAPIView
from rest_framework.exceptions import AuthenticationFailed

class BigDataBrokerAPIViewTestCase(TestCase):
    def test_parse_token(self):
        view = BigDataBrokerAPIView()
        self.assertEqual(view._parse_token("2:123"), {'token': '123', 'user_id': 2})
        self.assertEqual(view._parse_token("2:abcd"), {'token': 'abcd', 'user_id': 2})
        self.assertRaises(AuthenticationFailed, lambda: view._parse_token(""))
        self.assertRaises(AuthenticationFailed, lambda: view._parse_token(":"))
        self.assertRaises(AuthenticationFailed, lambda: view._parse_token("2:"))
        self.assertRaises(AuthenticationFailed, lambda: view._parse_token(":ac"))
        self.assertRaises(AuthenticationFailed, lambda: view._parse_token("a:ac"))