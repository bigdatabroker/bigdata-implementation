# coding: utf8

from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory
from jms.index.abstract import IndexAccessor
import json
from django.conf import settings
from jms.models import Server

class AgentAPISTreeTestcase(TestCase):
    def setUp(self):
        self.client = APIClient()
        settings.REQUIRE_SSL = False

        #get token
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)
        content = json.loads(response.content)
        self.token = content["token"]

        #set active
        server = Server.objects.get(token=self.token)
        server.is_active = True
        server.save()
        self.server = server

        accessor = IndexAccessor.get_accessor()
        accessor.enter_global_mode()
        self.acc = accessor

        self.assertTrue(self.acc.has_node([server.hostname]))

    def test_post_tree_without_token(self):
        response = self.client.post('/agent_api/v1/tree/', data='tree will be here', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "please authenticate with an access token!")


    def test_post_tree_with_invalid_token(self):
        response = self.client.post('/agent_api/v1/tree/', data='tree will be here', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN="invalid")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "Unknown access token!")


    def test_post_tree_with_empty_body(self):
        response = self.client.post('/agent_api/v1/tree/', data='', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "No tree given")


    def test_post_tree_with_wrong_content_type(self):
        response = self.client.post('/agent_api/v1/tree/', data='invalid', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 415) #415 = wront media type


    def test_post_tree_with_invalid_root_path(self):
        response = self.client.post('/agent_api/v1/tree/', data='invalid root path\r\ntest', content_type='text/plain', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "invalid root path")


    def test_post_tree_with_invalid_tree(self):
        response = self.client.post('/agent_api/v1/tree/', data='/\r\ntest', content_type='text/plain', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Malformed tree.")


    def test_post_tree_with_empty_tree(self):
        response = self.client.post('/agent_api/v1/tree/', data='/', content_type='text/plain', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 202) #202 = accepted


    def test_post_tree_with_correct_tree(self):
        response = self.client.post('/agent_api/v1/tree/', data='/\r\ndr-xr-xr-x 4096 0 0 "?" "2013-07-10 11:02:43.610025354" "2013-06-18 10:01:59.973000001" "2013-06-18 10:01:59.973000001" "„/boot“"', content_type='text/plain', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 202) #202 = accepted


    def test_post_tree_with_another_invalid_tree(self):
        response = self.client.post('/agent_api/v1/tree/', data='/\r\ndr-xr-Zr-x 4096 0 0 "?" "2013-07-10 11:02:43.610025354" "2013-06-18 10:01:59.973000001" "2013-06-18 10:01:59.973000001" "„/boot“"', content_type='text/plain', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Malformed tree.")
