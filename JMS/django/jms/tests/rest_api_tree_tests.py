from django.test import TestCase
from django.test.client import Client
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import *
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings
from rest_framework.parsers import JSONParser
from jms.index.abstract import IndexAccessor
import json
import urllib
from django.conf import settings

class TreeTestCase(TestCase):
    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.token = auth_handler.get_access_token()

        self.accessor = IndexAccessor.get_accessor()
        self.accessor.enter_global_mode()

        if self.accessor.has_node(['TestServer']):
            node = self.accessor.get_node(['TestServer'])
            self.accessor.delete_node(node)

        self.assertFalse(self.accessor.has_node(['TestServer'])) # testing the test

        self.server = Server()
        self.server.hostname = 'TestServer'
        self.server.capacity = 1000000
        self.server.is_active = True
        self.server.generate_token()
        self.server.save()


    def test_access_without_token(self):
        response = self.client.get('/rest_api/v1/node/' + self.server.hostname + ':/')

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_get_detail_root(self):
        response = self.client.get('/rest_api/v1/node/' + self.server.hostname + ':/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)

        n = json.loads(response.content)

        self.assertEqual(n['url'], 'TestServer:/')
        self.assertEqual(n['path_string'], '/')
        self.assertEqual(n['name'], 'TestServer')
        self.assertEqual(type(n['dates']), dict)
        self.assertEqual(n['name'], 'TestServer')
        self.assertEqual(n['hostname'], 'TestServer')
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(type(n['owner']), dict)
        self.assertEqual(type(n['path']), list)
        self.assertEqual(len(n['path']), 0)
        self.assertEqual(n['type'], 'SERVER')
        self.assertEqual(n['permissions'], '0777')

    def test_get_detail_node(self):

        root_node = self.accessor.get_node([self.server.hostname])

        self.accessor.add_node(root_node.url, {
            'name': 'sub_node1',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'FILE',
        })

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)

        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(n['url'], 'TestServer:/sub_node1')
        self.assertEqual(n['path_string'], '/sub_node1')
        self.assertEqual(n['name'], 'sub_node1')
        self.assertEqual(type(n['dates']), dict)
        self.assertEqual(n['name'], 'sub_node1')
        self.assertEqual(n['hostname'], 'TestServer')
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(type(n['owner']), dict)
        self.assertEqual(type(n['path']), list)
        self.assertEqual(len(n['path']), 1)
        self.assertEqual(n['path'][0], 'sub_node1')
        self.assertEqual(n['type'], 'FILE')
        self.assertEqual(n['permissions'], '0777')

    def _setup_simple_deep_tree(self):
        root_node = self.accessor.get_node([self.server.hostname])

        self.accessor.add_node(root_node.url, {
            'name': 'sub_node1',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })
        sub_node1 = self.accessor.get_node([self.server.hostname, 'sub_node1'])

        self.accessor.add_node(sub_node1.url, {
            'name': 'sub_node2',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })
        sub_node2 = self.accessor.get_node([self.server.hostname, 'sub_node1', 'sub_node2'])

        self.accessor.add_node(sub_node2.url, {
            'name': 'sub_node3',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })
        sub_node3 = self.accessor.get_node([self.server.hostname, 'sub_node1', 'sub_node2', 'sub_node3'])

        self.accessor.add_node(sub_node3.url, {
            'name': 'sub_node4',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'FILE',
        })

    def test_get_list_one_level_implicit(self):

        settings.API_NODE_MAX_REQUEST_DEPTH = 3
        settings.API_NODE_MAX_FORCED_DEPTH = 3

        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)

        n = json.loads(response.content)

        self.assertEqual(type(n), list)
        self.assertEqual(len(n), 1)
        self.assertEqual(n[0]['url'], 'TestServer:/sub_node1/sub_node2')
        self.assertEqual(n[0]['path_string'], '/sub_node1/sub_node2')
        self.assertEqual(n[0]['name'], 'sub_node2')
        self.assertEqual(n[0]['hostname'], 'TestServer')

    def test_get_list_one_level_explicit(self):

        settings.API_NODE_MAX_REQUEST_DEPTH = 3
        settings.API_NODE_MAX_FORCED_DEPTH = 3

        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/?depth=1', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)

        n = json.loads(response.content)

        self.assertEqual(type(n), list)
        self.assertEqual(len(n), 1)
        self.assertEqual(n[0]['url'], 'TestServer:/sub_node1/sub_node2')
        self.assertEqual(n[0]['path_string'], '/sub_node1/sub_node2')
        self.assertEqual(n[0]['name'], 'sub_node2')
        self.assertEqual(n[0]['hostname'], 'TestServer')

    def test_get_multiple_levels(self):

        settings.API_NODE_MAX_REQUEST_DEPTH = 3
        settings.API_NODE_MAX_FORCED_DEPTH = 3

        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/?depth=3', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)

        n = json.loads(response.content)

        self.assertEqual(type(n), list)
        self.assertEqual(len(n), 3)

        self.assertEqual(n[0]['url'], 'TestServer:/sub_node1/sub_node2')
        self.assertEqual(n[0]['path_string'], '/sub_node1/sub_node2')
        self.assertEqual(n[0]['name'], 'sub_node2')
        self.assertEqual(n[0]['hostname'], 'TestServer')

        self.assertEqual(n[1]['url'], 'TestServer:/sub_node1/sub_node2/sub_node3')
        self.assertEqual(n[1]['path_string'], '/sub_node1/sub_node2/sub_node3')
        self.assertEqual(n[1]['name'], 'sub_node3')
        self.assertEqual(n[1]['hostname'], 'TestServer')

        self.assertEqual(n[2]['url'], 'TestServer:/sub_node1/sub_node2/sub_node3/sub_node4')
        self.assertEqual(n[2]['path_string'], '/sub_node1/sub_node2/sub_node3/sub_node4')
        self.assertEqual(n[2]['name'], 'sub_node4')
        self.assertEqual(n[2]['hostname'], 'TestServer')

    def test_get_childs_with_lower_forced_depth(self):

        settings.API_NODE_MAX_REQUEST_DEPTH = 4
        settings.API_NODE_MAX_FORCED_DEPTH = 2

        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/?depth=4', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)

        n = json.loads(response.content)

        self.assertEqual(type(n), list)
        self.assertEqual(len(n), 2)

        self.assertEqual(n[0]['url'], 'TestServer:/sub_node1/sub_node2')
        self.assertEqual(n[0]['path_string'], '/sub_node1/sub_node2')
        self.assertEqual(n[0]['name'], 'sub_node2')
        self.assertEqual(n[0]['hostname'], 'TestServer')

        self.assertEqual(n[1]['url'], 'TestServer:/sub_node1/sub_node2/sub_node3')
        self.assertEqual(n[1]['path_string'], '/sub_node1/sub_node2/sub_node3')
        self.assertEqual(n[1]['name'], 'sub_node3')
        self.assertEqual(n[1]['hostname'], 'TestServer')

    def test_get_childs_with_invalid_depth_too_low(self):

        settings.API_NODE_MAX_REQUEST_DEPTH = 3
        settings.API_NODE_MAX_FORCED_DEPTH = 2

        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/?depth=-1', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 400)

    def test_get_childs_with_invalid_depth_too_high(self):

        settings.API_NODE_MAX_REQUEST_DEPTH = 3
        settings.API_NODE_MAX_FORCED_DEPTH = 2

        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/?depth=7', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 400)

    def test_get_childs_of_node_that_cant_have_childs_with_childs(self):
        self._setup_simple_deep_tree()
        sub_node1 = self.accessor.get_node([self.server.hostname, 'sub_node1'])

        self.accessor.update_node(sub_node1.url, {
            'name': sub_node1.name,
            'owner_user': sub_node1.owner_user,
            'owner_group': sub_node1.owner_group,
            'size': 0,
            'permissions_oct': 511,
            'type': 'FILE'
        })

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/?depth=2', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 400)

    def test_get_childs_of_node_that_cant_have_childs_without_childs(self):
        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_node2/sub_node3/sub_node4') + '/childs/?depth=2', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 400)

    def test_invalid_path_server(self):
        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + 'WhichDoesntExist:/sub_node1/sub_node2') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 404)

    def test_invalid_path_path(self):

        self._setup_simple_deep_tree()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_nodeOneMillionWhichDoesntExist') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 404)

    def test_flags_local(self):
        self._setup_simple_deep_tree()

        node = self.accessor.get_node(self.server.hostname + ':/sub_node1')

        self.accessor.add_flag(node, '~localonly')

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 1)
        self.assertTrue('localonly' in n['flags'])

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_node2') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 0)
        self.assertFalse('localonly' in n['flags'])


    def test_flags_recursive(self):
        self._setup_simple_deep_tree()

        node = self.accessor.get_node(self.server.hostname + ':/sub_node1')

        self.accessor.add_flag(node, '+inherited')

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 1)
        self.assertTrue('inherited' in n['flags'])

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_node2') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 1)
        self.assertTrue('inherited' in n['flags'])

    def test_flags_recursive_removal(self):
        self._setup_simple_deep_tree()

        node = self.accessor.get_node(self.server.hostname + ':/sub_node1')
        node2 = self.accessor.get_node(self.server.hostname + ':/sub_node1/sub_node2/sub_node3')

        self.accessor.add_flag(node, '+inherited')
        self.accessor.add_flag(node2, '-inherited')

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 1)
        self.assertTrue('inherited' in n['flags'])

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_node2') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 1)
        self.assertTrue('inherited' in n['flags'])

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_node2/sub_node3') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 0)
        self.assertFalse('inherited' in n['flags'])

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_node2/sub_node3') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(type(n['flags']), list)
        self.assertEqual(len(n['flags']), 0)
        self.assertFalse('inherited' in n['flags'])

    def _setup_simple_tree_without_perms(self):
        root_node = self.accessor.get_node([self.server.hostname])

        self.accessor.add_node(root_node.url, {
            'name': 'sub_node1',
            'owner_user': 2,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 511,
            'type': 'DIRECTORY',
        })
        sub_node1 = self.accessor.get_node([self.server.hostname, 'sub_node1'])

        self.accessor.add_node(sub_node1.url, {
            'name': 'sub_node2',
            'owner_user': 17,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 448,
            'type': 'DIRECTORY',
        })
        sub_node2 = self.accessor.get_node([self.server.hostname, 'sub_node1', 'sub_node2'])

        self.accessor.add_node(sub_node2.url, {
            'name': 'sub_node3',
            'owner_user': 17,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 448,
            'type': 'DIRECTORY',
        })
        sub_node3 = self.accessor.get_node([self.server.hostname, 'sub_node1', 'sub_node2', 'sub_node3'])

        self.accessor.add_node(sub_node3.url, {
            'name': 'sub_node4',
            'owner_user': 17,
            'owner_group': 77,
            'size': '7799',
            'permissions_oct': 448,
            'type': 'FILE',
        })

    def test_get_no_perms_detail(self):
        self._setup_simple_tree_without_perms()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), dict)
        self.assertEqual(n['url'], 'TestServer:/sub_node1')
        self.assertEqual(n['path_string'], '/sub_node1')
        self.assertEqual(n['name'], 'sub_node1')

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1/sub_node2') + '/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 404)

    def test_get_no_perms_list(self):
        self._setup_simple_tree_without_perms()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), list)
        self.assertEqual(len(n), 0)

    def test_get_no_perms_list_multiple_levels(self):

        settings.API_NODE_MAX_REQUEST_DEPTH = 3
        settings.API_NODE_MAX_FORCED_DEPTH = 3

        self._setup_simple_tree_without_perms()

        response = self.client.get('/rest_api/v1/node/' + urllib.quote_plus(self.server.hostname + ':/sub_node1') + '/childs/?depth=3', HTTP_X_BDB_TOKEN=str(self.token.user.user_id) + ':' + self.token.token)

        self.assertEqual(response.status_code, 200)
        n = json.loads(response.content)

        self.assertEqual(type(n), list)
        self.assertEqual(len(n), 0)
