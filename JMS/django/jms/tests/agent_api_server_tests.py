from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
import json
from django.conf import settings
from jms.models import Server, LogEntry

class AgentAPIServerTestcase(TestCase):
    def setUp(self):
        self.client = APIClient()
        settings.REQUIRE_SSL = False

    def test_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 201)
        content = json.loads(response.content)
        self.assertEqual(content["hostname"], "test")
        self.assertEqual(content["capacity"], 1024)
        self.assertEqual(content["is_active"], False)
		
        obj = Server.objects.get(pk=content["id"])
        self.assertNotEqual(obj, None)
        self.assertEqual(obj.hostname, "test")
        self.assertEqual(obj.capacity, 1024)
        self.assertEqual(obj.token, content["token"])

    def test_without_capacity_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test"}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 201)
        content = json.loads(response.content)
        self.assertEqual(content["hostname"], "test")
        self.assertTrue(content["capacity"] >= 0)
        self.assertEqual(content["is_active"], False)
        
        obj = Server.objects.get(pk=content["id"])
        self.assertNotEqual(obj, None)
        self.assertEqual(obj.hostname, "test")
        self.assertTrue(obj.capacity >= 0)
        self.assertEqual(obj.token, content["token"])

    def test_negative_capacity_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : -1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Capacity must be positive.")
        self.assertRaises(Server.DoesNotExist, lambda: Server.objects.get(hostname="test"))

    def test_illegal_capacity_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : "a"}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertNotEqual(json.loads(response.content)["capacity"], None)
        self.assertRaises(Server.DoesNotExist, lambda: Server.objects.get(hostname="test"))

    def test_illegal_hostname_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "", "capacity" : 1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 400)		
        self.assertEqual(json.loads(response.content)["hostname"], ["This field is required."])
        self.assertRaises(Server.DoesNotExist, lambda: Server.objects.get(hostname=""))

    def test_missing_hostname_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"capacity" : 1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 400)     
        self.assertEqual(json.loads(response.content)["hostname"], ["This field is required."])

    def test_duplicate_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 201)

        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["hostname"], ["Server with this Hostname already exists."])
        self.assertEqual(len(Server.objects.filter(hostname="test")), 1)

    def test_server_creation_with_token(self):
        response = self.client.put('/agent_api/v1/server/', HTTP_X_BDB_AGENT_TOKEN="dummy", data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 400)		
        self.assertEqual(json.loads(response.content)["detail"], "Do not provide a token when requesting a token.")
        self.assertRaises(Server.DoesNotExist, lambda: Server.objects.get(hostname="test"))

    def test_log_after_server_creation(self):
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 201)
        server = Server.objects.get(pk=json.loads(response.content)["id"])
        entries = LogEntry.get_log_entries(Server, server.id)
        self.assertEqual(len(entries), 1)
        self.assertEqual(entries[0].message, "registered successfully")