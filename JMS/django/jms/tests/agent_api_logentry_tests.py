from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
import json
from django.conf import settings
from jms.models import LogEntry, Server, Job, Operation, User

class AgentAPILogentryTestcase(TestCase):
    def setUp(self):
        self.client = APIClient()
        settings.REQUIRE_SSL = False

        #get token
        response = self.client.put('/agent_api/v1/server/', data='{"hostname" : "test", "capacity" : 1024}', content_type='application/json', follow=False)        
        content = json.loads(response.content)
        self.token = content["token"]

        #set active
        server = Server.objects.get(token=self.token)
        server.is_active = True
        server.save()
        self.server = server


    def test_create_log_entry_without_token(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "server", "message" : "test"}', content_type='application/json', follow=False)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "please authenticate with an access token!")


    def test_create_log_entry_with_invalid_token(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "server", "message" : "test"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN="invalid")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "Unknown access token!")
        

    def test_create_log_entry_with_inactive_server(self):        
        self.server.is_active = False
        self.server.save()

        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "server", "message" : "test"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 403)     
        self.assertEqual(json.loads(response.content)["detail"], "Server has to be activated before it can log events.")

        self.server.is_active = True
        self.server.save() 


    def test_create_server_log_entry(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "server", "message" : "test"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 201)   

        entries = LogEntry.get_log_entries(Server, self.server.id)
        self.assertEqual(len(entries), 2)
        self.assertEqual(entries[0].message, "test")
        self.assertEqual(entries[1].message, "registered successfully")


    def test_create_server_log_entry_with_no_message(self):    
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "server"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["message"], ["This field is required."])        


    def test_create_server_log_entry_with_no_type(self):    
        response = self.client.put('/agent_api/v1/logentry/', data='{"message" : "test"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "No entry type specified.")        


    def test_create_server_log_entry_with_invalid_type(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "halloooo", "message" : "test"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Invalid entry type.")  


    def test_create_log_entry_with_no_job_id(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "job", "message" : "test"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "foreign_id required.")        


    def test_create_log_entry_with_nonexistent_job_id(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "job", "message" : "test", "foreign_id" : 1}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Job does not exist.")


    def test_create_log_entry_with_job_id(self):
        user = User(user_id = 1, name="hans")
        user.save()

        job = Job(name="testjob", user_id = user.user_id)
        job.save()

        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "job", "message" : "test", "foreign_id" : %s}' % job.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(json.loads(response.content)["message"], "test")
        self.assertEqual(json.loads(response.content)["foreign_id"], job.id)
        self.assertEqual(json.loads(response.content)["is_error"], False)


    def test_create_log_entry_with_no_operation_id(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "operation", "message" : "test"}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "foreign_id required.")        


    def test_create_log_entry_with_nonexistent_operation_id(self):
        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "operation", "message" : "test", "foreign_id" : 1}', content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["detail"], "Operation does not exist.")


    def test_create_log_entry_with_operation_id(self):
        user = User(user_id = 1, name="hans")
        user.save()

        job = Job(name="testjob", user_id = user.user_id)
        job.save()

        op = Operation(job_id=job.id, op_type=Operation.DELETE, source="/dev/null")
        op.save()

        response = self.client.put('/agent_api/v1/logentry/', data='{"type" : "operation", "message" : "test", "foreign_id" : %s}' % op.id, content_type='application/json', follow=False, HTTP_X_BDB_AGENT_TOKEN=self.token)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(json.loads(response.content)["message"], "test")
        self.assertEqual(json.loads(response.content)["foreign_id"], op.id)
        self.assertEqual(json.loads(response.content)["is_error"], False)