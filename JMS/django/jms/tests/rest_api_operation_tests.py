from django.test import TestCase
from django.test.client import Client
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import User, Job, Operation
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings

from rest_framework.parsers import JSONParser

import json

no_admin_id = 1
admin_id = 2

class RESTAPIOperationTestCase(TestCase):
    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.admin_token = auth_handler.get_access_token()

        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": no_admin_id, "password": "password"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.no_admin_token = auth_handler.get_access_token()

        self.job1 = Job(user_id= admin_id, name='job1', priority= 5)
        self.job1.save()

    # test no token

    def test_get_operation_list_no_token(self):
        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')
    
    def test_put_operation_list_no_token(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', 
            data='{"source": "path_does_not_exist", "target": "path_does_not_exist", "type": "COPY", "force": "True"}', 
            content_type='application/json')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_get_operation_detail_no_token(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()

        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_delete_operation_detail_no_token(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()
        self.assertEqual(Operation.objects.get(id=op1.id), op1)

        response = self.client.delete('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/')
     
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    # test invalid token

    def test_get_operation_list_invalid_token(self):
        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', HTTP_X_BDB_TOKEN= "2:abcd")

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_put_operation_list_invalid_token(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', 
            data='{"source": "path_does_not_exist", "target": "path_does_not_exist", "type": "COPY", "force": "True"}', 
            content_type='application/json', HTTP_X_BDB_TOKEN= "2:abcd")
     
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_get_operation_detail_invalid_token(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()

        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/', HTTP_X_BDB_TOKEN= "2:abcd")
     
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_delete_operation_detail_invalid_token(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()
        self.assertEqual(Operation.objects.get(id=op1.id), op1)

        response = self.client.delete('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/', HTTP_X_BDB_TOKEN= "2:abcd")
     
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')
    
    # test the API

    # OperationList

    def test_get_operations_empty(self):
        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 200) # OK
        self.assertEqual(response.content, '[]') #empty list

    def test_get_operations(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()

        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 200) # OK
        rOp1 = (json.loads(response.content))[0]
        self.assertEqual(rOp1['source'], op1.source)
        self.assertEqual(rOp1["target"], op1.target)
        self.assertEqual(rOp1["type"], op1.get_type_as_string())

    def test_put_operation_force(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', 
            data='{"source": "path_does_not_exist", "target": "path_does_not_exist", "type": "COPY", "force": "True"}', 
            content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 201) # created
        rOp1 = json.loads(response.content)
        rOp1ID = rOp1["id"]
        self.assertEqual(rOp1["source"], "path_does_not_exist")
        self.assertEqual(rOp1["target"], "path_does_not_exist")
        self.assertEqual(rOp1["type"], "COPY")

        dOp1 = Operation.objects.get(id=rOp1ID)
        self.assertEqual(dOp1.id, rOp1ID)
        self.assertEqual(dOp1.source, "path_does_not_exist")
        self.assertEqual(dOp1.target, "path_does_not_exist")
        self.assertEqual(dOp1.get_type_as_string(), 'COPY')

    def test_put_operation_correct(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', 
            data='{"source": "server2:/home/root", "target": "nomishe:/", "type": "COPY"}', 
            content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 201) # created
        rOp1 = json.loads(response.content)
        rOp1ID = rOp1["id"]
        self.assertEqual(rOp1["source"], "server2:/home/root")
        self.assertEqual(rOp1["target"], "nomishe:/")
        self.assertEqual(rOp1["type"], "COPY")

        dOp1 = Operation.objects.get(id=rOp1ID)
        self.assertEqual(dOp1.id, rOp1ID)
        self.assertEqual(dOp1.source, "server2:/home/root")
        self.assertEqual(dOp1.target, "nomishe:/")
        self.assertEqual(dOp1.get_type_as_string(), 'COPY')

    def test_put_operation_miss_data(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "No parameters or too many parameters. Only operation type, source and target and optional force."}')

    def test_put_operation_miss_op_type(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', data='{"source": "server2:/home/root", "target": "nomishe:/"}', 
            content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "Operation type is missing"}')

    def test_put_operation_miss_target(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', data='{"source": "server2:/home/root", "type": "COPY"}', 
            content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "Target is missing!"}')

    def test_put_operation_invalid_op_type(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', data='{"source": "server2:/home/root", "target": "nomishe:/", "type": "abcd"}', 
            content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "invalid operation type"}')

    def test_put_operation_non_plausible_op(self):
        response = self.client.put('/rest_api/v1/job/' + str(self.job1.id) + '/operation/', data='{"source": "server2:/home/", "target": "server2:/home/root/", "type": "COPY"}', 
            content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "A folder can not be moved or copied to one of its subfolders."}')

    # OperationDetail

    def test_get_operation_detail_correct(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()

        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/', 
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        
        self.assertEqual(response.status_code, 200)
        rOp1 = (json.loads(response.content))
        self.assertEqual(rOp1['source'], op1.source)
        self.assertEqual(rOp1["target"], op1.target)
        self.assertEqual(rOp1["type"], op1.get_type_as_string())

    def test_get_operation_detail_no_job_permission(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()

        response = self.client.get('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/', 
            HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.content, '{"detail": "Job does not exist or you have no permissions!"}')

    def test_delete_operation_detail_correct(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()

        response = self.client.delete('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)

        self.assertEqual(response.status_code, 204) # no content
        listOperation = Operation.objects.filter(id=op1.id)
        self.assertEqual(len(listOperation), 0)

    def test_delete_operation_detail_no_job_permission(self):
        op1 = Operation(job_id= self.job1.id, source= "server2:/home/root", target="nomishe:/", op_type= 0)
        op1.save()

        response = self.client.delete('/rest_api/v1/job/' + str(self.job1.id) + '/operation/' + str(op1.id) + '/',
            HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.content, '{"detail": "Job does not exist or you have no permissions!"}')
