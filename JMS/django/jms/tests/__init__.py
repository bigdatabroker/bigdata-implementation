from jms.tests.utils_tests import *
from jms.tests.bigdatabroker_api_view_tests import *
from postgres_index_accessor_tests import *

# REST-API
from jms.tests.rest_api_auth_tests import * 
from jms.tests.rest_api_connection_tests import * 
from jms.tests.rest_api_job_tests import *  
from jms.tests.rest_api_log_tests import * 
from jms.tests.rest_api_operation_tests import *  
from jms.tests.rest_api_server_tests import * 
from jms.tests.rest_api_user_tests import * 
from jms.tests.rest_api_tree_tests import *
from jms.tests.rest_api_auth_tests import *

# Agent-API
from jms.tests.agent_api_server_tests import *
from jms.tests.agent_api_logentry_tests import *
from jms.tests.agent_api_status_tests import *
from jms.tests.agent_api_task_tests import *
from jms.tests.agent_api_tree_tests import *

# Scheduler
from jms.tests.scheduler_operation_tests import *