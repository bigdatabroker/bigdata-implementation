from django.test import TestCase
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from django.test.client import Client
from jms.models import User, Job, Operation
from jms.scheduler.operation import OperationPlausibilityChecker
from jms.utils import import_class
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings

from rest_framework.parsers import JSONParser

import json

class SchedulerOperationPlausibilityCheckerTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.token = auth_handler.get_access_token()

    def test_check_invalid_target_path(self):
        source = "server2:/home/root"
        target = "path_does_not_exist"
        op_type = "COPY"

        AccessorClass = import_class(settings.JMS_INDEX_ACCESSOR_CLASS)
        checker = OperationPlausibilityChecker(AccessorClass(self.token), op_type, source, target)
        error = checker.check()
        self.assertEqual(error, 'Invalid target path (not found or no permissions)')

    def test_check_invalid_source_path(self):
        source = "path_does_not_exist"
        target = "server2:/home/root"
        op_type = "COPY"

        AccessorClass = import_class(settings.JMS_INDEX_ACCESSOR_CLASS)
        checker = OperationPlausibilityChecker(AccessorClass(self.token), op_type, source, target)
        error = checker.check()
        self.assertEqual(error, 'Invalid source path (not found or no permissions)')

    def test_check_coy_in_subfolder(self):
        source = "server2:/home/"
        target = "server2:/home/root/"
        op_type = "COPY"

        AccessorClass = import_class(settings.JMS_INDEX_ACCESSOR_CLASS)
        checker = OperationPlausibilityChecker(AccessorClass(self.token), op_type, source, target)
        error = checker.check()
        self.assertEqual(error, 'A folder can not be moved or copied to one of its subfolders.')



        
