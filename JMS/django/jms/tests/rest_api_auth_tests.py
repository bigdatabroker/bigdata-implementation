from django.test import TestCase
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import Server, Connection
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings
from jms.models import *

from rest_framework.parsers import JSONParser

import json

from jms.models import AccessToken
from rest_framework.test import APIClient


class RESTAPIAuthTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_without_ssl_while_ssl_required(self):
        settings.REQUIRE_SSL = True

        parsers = [JSONParser()]
        authenticators = []

        num_users = User.objects.filter(user_id=2).count()
        self.assertEquals(num_users, 0)

        num_access_tokens = AccessToken.objects.filter(user=2).count()
        self.assertEquals(num_access_tokens, 0)

        orig_req = self.client.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        self.assertEquals(orig_req.status_code, 403)

        num_new_users = User.objects.filter(user_id=2).count()
        self.assertEquals(num_new_users, 0)

        num_new_access_tokens = AccessToken.objects.filter(user=2).count()
        self.assertEquals(num_new_access_tokens, 0)

    def test_with_ssl_while_ssl_required(self):
        settings.REQUIRE_SSL = True

        parsers = [JSONParser()]
        authenticators = []

        num_users = User.objects.filter(user_id=2).count()
        self.assertEquals(num_users, 0)

        num_access_tokens = AccessToken.objects.filter(user=2).count()
        self.assertEquals(num_access_tokens, 0)

        orig_req = self.client.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), **{ 'content_type': 'application/json', 'wsgi.url_scheme': 'https'})

        self.assertEquals(orig_req.status_code, 200)

        num_new_users = User.objects.filter(user_id=2).count()
        self.assertEquals(num_new_users, 1)

        num_new_access_tokens = AccessToken.objects.filter(user=2).count()
        self.assertEquals(num_new_access_tokens, 1)

        user = User.objects.get(pk=2)
        access_token = AccessToken.objects.get(user=2)

        rAuth = json.loads(orig_req.content)
        self.assertEqual(rAuth["user"], user.user_id)
        self.assertEqual(rAuth["token"], access_token.token)

    def test_auth_while_supplying_access_token(self):
        settings.REQUIRE_SSL = False

        parsers = [JSONParser()]
        authenticators = []

        factory = APIRequestFactory()
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": 2, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)

        self.assertTrue(auth_handler.authenticate())

        token = auth_handler.get_access_token()

        response = self.client.post('/rest_api/v1/auth/', {"user_id": 2, "password": "hallo"}, HTTP_X_BDB_TOKEN=str(token.user.user_id) + ':' + token.token)

        self.assertEquals(response.status_code, 400)
