from django.test import TestCase
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.models import Server, Connection
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework.request import Request
from django.conf import settings

from rest_framework.parsers import JSONParser

import json

no_admin_id = 1
admin_id = 2

class RESTAPIConnectionTestCase(TestCase):
    def setUp(self):
        settings.REQUIRE_SSL = False
        self.client = APIClient()
        factory = APIRequestFactory()

        parsers = [JSONParser()]
        authenticators = []
        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": admin_id, "password": "hallo"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.admin_token = auth_handler.get_access_token()

        orig_req = factory.post('/rest_api/v1/auth/', json.dumps({"user_id": no_admin_id, "password": "password"}), content_type='application/json')

        request = Request(orig_req, parsers, authenticators, None, None)
        auth_handler = SimpleAuthenticationHandler(request)
        auth_handler.authenticate()

        self.no_admin_token = auth_handler.get_access_token()

        self.server1 = Server()
        self.server1.hostname = 'Server1'
        self.server1.capacity = 1000000
        self.server1.is_active = True
        self.server1.generate_token()
        self.server1.save()

        self.server2 = Server()
        self.server2.hostname = 'Server2'
        self.server2.capacity = 1000000
        self.server2.is_active = True
        self.server2.generate_token()
        self.server2.save()

    #test no token

    def test_get_connection_list_no_token(self):
        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_put_connection_list_no_token(self):
        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_get_connection_detail_no_token(self):
        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_post_connection_detail_no_token(self):
        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')

    def test_delete_connection_detail_no_token(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.delete('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/'+ con.protocol + '/')
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content, '{"detail": "No token provided."}')
        
    #test invalid token

    def test_get_connection_list_invalid_token(self):
        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/', HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')
    
    def test_put_connection_list_invalid_token(self):
        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            data='{"target":' + str(self.server2.id) +',"protocol":"HTTP"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')
    
    def test_get_connection_detail_invalid_token(self):
        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')
    
    def test_post_connection_detail_invalid_token(self):
        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            '{"speed":666}', content_type='application/json', HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    def test_delete_connection_detail_invalid_token(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.delete('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/'+ con.protocol + '/',
         HTTP_X_BDB_TOKEN= "2:abcd")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Invalid access token specified."}')

    #test the API

    #ConnectionList

    def test_get_connection_list(self):
        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        self.assertEqual(response.content, '[]') #empty list

        con = Connection(source=self.server1, target=self.server2, protocol= 'HTTP')
        con.save()

        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rCon = (json.loads(response.content))[0]
        self.assertEqual(rCon["source"], self.server1.id)
        self.assertEqual(rCon["target"], self.server2.id)
        self.assertEqual(rCon["protocol"], con.protocol)

    def test_put_connection_list_correct(self):
        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            data='{"target":' + str(self.server2.id) +',"protocol":"HTTP"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token, follow=False)
        self.assertEqual(response.status_code, 201) # CREATED
        rCon = json.loads(response.content)
        self.assertEqual(rCon["source"], self.server1.id)
        self.assertEqual(rCon["target"], self.server2.id)
        self.assertEqual(rCon["protocol"], "HTTP")

        dCon = Connection.objects.get(source=self.server1.id)
        self.assertEqual(dCon.target, self.server2)
        self.assertEqual(dCon.protocol, "HTTP")

    def test_put_connection_list_miss_data(self):
        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "No parameters or too many parameters. Only target, protocol and speed"}')

    def test_put_connection_list_miss_target(self):
        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            data='{"protocol":"HTTP"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"target": ["This field is required."]}')

    def test_put_connection_list_loop(self):
        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            data='{"target":' + str(self.server1.id) +',"protocol":"HTTP"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "no connection loops allowed!"}')

    def test_put_connection_list_duplicate(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            data='{"target":' + str(self.server2.id) +',"protocol":"HTTP"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token, follow=False)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "Connection already exists."}')

    def test_put_connection_list_non_admin(self):
        response = self.client.put('/rest_api/v1/server/' + str(self.server1.id) + '/connection/',
            data='{"target":' + str(self.server2.id) +',"protocol":"HTTP"}', content_type='application/json',
            HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token, follow=False)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Admin rights required."}')

    #ConnectionDetail

    def test_get_connection_detail_correct(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rCon = json.loads(response.content)
        self.assertEqual(rCon["source"], con.source.id)
        self.assertEqual(rCon["target"], con.target.id)
        self.assertEqual(rCon["protocol"], con.protocol)

    def test_get_connection_detail_not_exist(self):
        response = self.client.get('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.content, '{"detail": "Connection does not exist!"}')

    def test_post_connection_detail_correct(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            '{"speed":666}', content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 200) # OK
        rCon = json.loads(response.content)
        self.assertEqual(rCon["source"], con.source.id)
        self.assertEqual(rCon["target"], con.target.id)
        self.assertEqual(rCon["protocol"], con.protocol)
        self.assertEqual(rCon["speed"], 666)

        dCon = Connection.objects.get(source=con.source)
        self.assertEqual(dCon.target, con.target)
        self.assertEqual(dCon.protocol, con.protocol)
        self.assertEqual(dCon.speed, 666)

    def test_post_connection_detail_miss_data(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "No parameters or too many parameters. Only speed"}')

    def test_post_connection_detail_too_many_data(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            data='{"target":' + str(self.server2.id) +',"speed":666}', content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "No parameters or too many parameters. Only speed"}')

    def test_post_connection_detail_wrong_data(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            data='{"target":' + str(self.server2.id) + '}', content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"detail": "Only speed can be modified"}')

    def test_post_connection_detail_incorrect_data(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            data='{"speed":"abc"}', content_type='application/json', HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, '{"speed": ["Enter a whole number."]}')

    def test_post_connection_detail_no_admin(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.post('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/HTTP/',
            data='{"speed":"abc"}', content_type='application/json', HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Admin rights required."}')

    def test_delete_connection_detail_correct(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.delete('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/'+ con.protocol + '/',
         HTTP_X_BDB_TOKEN= "2:" + self.admin_token.token)
        self.assertEqual(response.status_code, 204) # no content
        listCon = Connection.objects.filter(source=self.server1)
        self.assertEqual(len(listCon), 0)

    def test_delete_connection_detail_no_admin(self):
        con = Connection()
        con.source=self.server1
        con.target=self.server2
        con.protocol= 'HTTP'
        con.save()

        response = self.client.delete('/rest_api/v1/server/' + str(self.server1.id) + '/connection/' + str(self.server2.id) + '/'+ con.protocol + '/',
         HTTP_X_BDB_TOKEN= str(no_admin_id) + ":" + self.no_admin_token.token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, '{"detail": "Admin rights required."}')