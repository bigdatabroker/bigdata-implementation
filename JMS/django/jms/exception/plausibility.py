from jms.exception.error import Error

class PlausibilityCheckerError(Error):
    """Exception for plausibility checker errors

    Author: Benno Ommerborn
    """
    pass