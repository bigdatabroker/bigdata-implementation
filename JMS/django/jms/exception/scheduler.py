from jms.exception.error import Error

class SchedulerError(Error):
    """Exception for scheduler errors

    Author: Simon Hesssner
    """
    pass