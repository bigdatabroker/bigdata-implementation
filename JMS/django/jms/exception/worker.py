from jms.exception.error import Error

class WorkerError(Error):
    """Exception for worker errors

    Author: Simon Hesssner
    """
    pass