from jms.exception.error import Error

class ModelError(Error):
	"""Exception for model errors

	Author: Simon Hesssner
	"""
	pass