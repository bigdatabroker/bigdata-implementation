from jms.exception.error import Error

class InvalidURLSyntaxException(Error):
    """ Thrown if a path/URL could not be parsed correctly. 

    Author: Julian Schuh
    """
    pass


class URLResolvementException(Error):
    """ Thrown if a syntactically correct path/URL could not be resolved. 

    Author: Julian Schuh
    """
    pass


class InvalidPathException(URLResolvementException):
    """ Thrown if the node specified by a path/URL doesen't exist 

    Author: Julian Schuh
    """
    pass


class NoPermissionsException(URLResolvementException):
    """ Thrown if a spcified path exists but the user doesen't have permissions to view it.

    Author: Julian Schuh
    """
    pass


class InvalidFlagException(Error):
    """ Thrown if an invalid flag should be applied to a node. 

    Author: Julian Schuh
    """
    pass


class InvalidNodeTypeException(Error):
    """ Thrown if the specified type of a node is not valid. 

    Author: Julian Schuh
    """
    pass
