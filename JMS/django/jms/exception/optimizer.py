from jms.exception.error import Error

class OptimizerError(Error):
    """Exception for optimizer errors

    Author: Simon Hesssner
    """
    pass