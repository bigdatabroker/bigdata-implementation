from rest_framework.exceptions import APIException
from rest_framework import status

class BadRequestHttp400(APIException):
	""" Is raised whenever a API request could not be statisfied due to
	invalid or missing arguments.

	HTTP status code is 400.

	Author: Simon Hessner
	"""

	def __init__(self, detail = None):
		""" Initializes the Exception and sets the status code and error message. """
		self.detail = detail or "Invalid arguments or missing arguments."
		self.status_code = status.HTTP_400_BAD_REQUEST


class Forbidden403(APIException):
	""" Is raised whenever an API request could not be statisfied due to
	invalid auth data.

	HTTP status code is 403.

	Author: Julian Schuh
	"""

	def __init__(self, detail = None):
		""" Initializes the Exception and sets the status code and error message. """
		self.detail = detail or "Authentication failed."
		self.status_code = status.HTTP_403_FORBIDDEN


class Http404(APIException):
	""" Is raised whenever an API request could not be statisfied due to
	lack of resource.

	HTTP status code is 404.

	Author: Simon Hessner
	"""

	def __init__(self, detail = None):
		""" Initializes the Exception and sets the status code and error message. """
		self.detail = detail or "Not found."
		self.status_code = status.HTTP_404_NOT_FOUND


class AuthenticationRequiredHttp401(APIException):
	""" Is raised whenever an API request could not be statisfied because
	the user has not authenticated.

	HTTP status code is 401.

	Author: Julian Schuh
	"""
	def __init__(self, detail = None):
		self.detail = detail or "Authentication required."
		self.status_code = status.HTTP_401_UNAUTHORIZED