class Error(Exception):
	""" Base exception for the big data broker.

	Every user defined exception should extend this class!

	Author: Simon Hesssner
	"""
	pass