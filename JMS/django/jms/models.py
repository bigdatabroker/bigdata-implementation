from django.db import models
# for polymorphic foreign key
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
# /for polymorphic foreign key

from jms.exception.model import ModelError
from jms.utils import TokenGenerator
from jms.index.abstract import IndexAccessor
from datetime import datetime
from jms.utils import get_proper_date

class User(models.Model):
    """ Represents an user

    An user has a name, a priority, belongs to various groups and can be admin.
    Please do not access _group_ids but group_ids.

    Attributes:
        user_id: The unique unix user ID (integer).
        is_admin: True if the user is an admin, else False.
        priority: Integer indicating user priority (e.g. used in scheduling).
        group_ids: Integer set of all group IDs the user belongs to.
        name: String containing user name.

    Raises:
        ModelError: User ID != 0 should be changed or user should be deleted.

    Author: Simon Hessner
    """
    #this is defined manually because we want to be able to set the ID without having to use auto_indrement
    user_id = models.PositiveIntegerField(default = 0, null = False, blank = False, primary_key = True, db_column="id")

    #Do not access directly! Please use the public attribute group_ids instead.
    _group_ids = models.CommaSeparatedIntegerField(max_length=256**2, blank = True, null = True, db_column="group_ids")

    is_admin  = models.BooleanField(default = False)
    priority  = models.PositiveIntegerField(default = 1) # lowest priority is 1
    name      = models.CharField(max_length = 32)

    def __init__(self, *args, **kwargs):
        """ Initializes important attributes.

        Saves user id to be able to check it for manipulation in save().
        Also splits the group_ids to an easily accessible list of integers.
        """
        super(User, self).__init__(*args, **kwargs)
        self._old_user_id = self.user_id

        # convert comma separated list to set of integers
        if self._group_ids:
            cleaned = filter(None, self._group_ids.split(",")) #removes empty string (if no group ids present)
        else:
            cleaned = [] # because None.split will fail... ;)
        self.group_ids = set([int(x) for x in cleaned]) #set => no duplicates

    def save(self, *args, **kwargs):
        """ Saves the object to the database.

        Checks if the user_id has been changed and if the old user_id was 0 (which means a new user object was created)
        Changing the user_id is only allowed if the old user_id was 0.
        After a "real" id was set it cannot be changed anymore.

        save() also converts the integer list to a string
        """
        if self._old_user_id != 0 and self.user_id != self._old_user_id:
            self.user_id = self._old_user_id
            raise ModelError("User ID cannot be changed!")

        #make comma separated list from set of integers
        self._group_ids = ",".join([str(x) for x in set(self.group_ids)])

        super(User, self).save(*args, **kwargs)
        self._old_user_id = self.user_id

    def delete(self):
        """ Prevents user from being deleted."""
        raise ModelError("An user cannot be deleted!")

    def __unicode__(self):
        """ Returns a string representing the User """
        return "%d: %s (admin: %s) (%d)" % (self.user_id, self.name, self.is_admin, self.priority)


class AccessToken(models.Model):
    """ Represents an access token which can be used by users to authenticate
    themselves at the JMS.

    Attributes:
        token: String containing the actual token.
        created_on: DateTime containing the time of creation.
        expires_on: DateTime that determines when the token will be expiring.
        data: String containing data accociated with the token.
        user: User object refering to the token owner.

    Author: Simon Hessner
    """

    token = models.CharField(max_length = 32, unique=True)
    created_on = models.DateTimeField(auto_now_add = True)
    expires_on = models.DateTimeField(default = 0, blank = True, null = True)
    data = models.TextField(unique = False, blank = False, null = False) #Problem with MySQLdb 1.2.1p2 @see https://docs.djangoproject.com/en/dev/ref/models/fields/#textfield
    user = models.ForeignKey('User', blank = False, null = False, on_delete = models.CASCADE)

    def generate_token(self):
        """ Generates an unique token and stores it as AccessToken.token. """
        generator = TokenGenerator(32)
        token = generator.get_token()

        while len(AccessToken.objects.filter(token__exact=token)) > 0:
            token = generator.get_token()

        self.token = token

    def is_expired(self):
        """ Method used to determine if the AccessToken has already expired.

        Returns:
            True: The token has expired
            False: The token has not expired
        """
        if not self.expires_on:
            return False

        return self.expires_on <= get_proper_date(datetime.utcnow())

    def __unicode__(self):
        """ Returns a string representing the AccessToken """
        return "User-ID: %s, token: %s" % (self.user.user_id, self.token)


class Job(models.Model):
    """ Represents a job created by an user.

    A job contains various operations and can have different events and log entries.

    Attributes:
        status: Enumeration which can hold the following values: WAITING, RUNNING, SUCCESS, ERROR
        priority: Integer indicating job priority.
        name: String holding job name.
        user: User object containing job owner.
        token: AccessToken object containing token of the job.

    Author: Simon Hessner
    """
    EDITING  = 0 #after creation
    WAITING  = 1 #after finalization
    READY    = 2 #after optimization
    RUNNING  = 3 #after start
    SUCCESS  = 4 #after end
    ERROR    = 5 #after end
    CHOICES  = ((EDITING, 'EDITING'),
               (WAITING, 'WAITING'),
               (READY, 'READY'),
			   (RUNNING, 'RUNNING'),
			   (SUCCESS, 'SUCCESS'),
			   (ERROR, 'ERROR'))

    # will create [(1,1), (2,2), ...]
    minp = 1
    maxp = 5
    ALLOWED_PRIORITIES = zip(range(minp, maxp+1), range(minp, maxp+1))

    status 	 = models.IntegerField(default=EDITING, choices=CHOICES)
    priority = models.PositiveIntegerField(default=1, choices=ALLOWED_PRIORITIES)
    name 	 = models.CharField(max_length=256)
    user 	 = models.ForeignKey('User', blank = False, null = False, on_delete = models.CASCADE) # needed because token could expire.
    token 	 = models.ForeignKey('AccessToken',  blank = True, null = True, on_delete = models.SET_NULL)

    def get_status_as_string(self):
        """ Returns the status of the job as string.

        Convenience method that makes it easier to get the status as string
        because accessing the status attribute would deliver an integer.

        Returns:
            String containing job status.
        """
        return self.get_status_display()

    def status_is_allowed(self, status):
        """ Used to determine if a given status is a valid status for the Job.

        Args:
            status: status to be checked.

        Returns:
            Boolean that indicates if status is allowed or not.
        """
        return status.upper() in [c[1] for c in self.CHOICES]

    def set_status_by_string(self, status):
        """ Changes the Jobs status.

        Args:
            status: New status as string.

        Raises:
            ModelError: Invalid new status.
        """
        if not self.status_is_allowed(status.upper()):
            raise ModelError("Invalid status provided")
        else:
            for c in self.CHOICES:
                if c[1] == status.upper():
                    self.status = c[0]

    def is_finalizable(self):
        """ Determines if the job can be finalized now.

        Returns:
            Boolean indicating if the job can be finalized.
        """
        return self.get_status_as_string() in ["EDITING", "WAITING"]

    def finalize(self):
        """ Finalizes the Job by setting the state to WAITING and
        creating a FINALIZING event.

        Raises:
            ModelError: Job is not finalizable.
        """
        if self.is_finalizable():
            self.set_status_by_string('WAITING')
        else:
            raise ModelError("Cannot change state from %s to WAITING" % self.get_status_as_string())

        # avoid integrity_error if event already exists.
        if not Event.get_event(self, "FINALIZING"):
            event = Event(job=self)
            event.set_type_by_string("FINALIZING")
            event.save()

    def is_deletable(self):
        """ Determines if the Job can be deleted now.

        Returns:
            Boolean indicating if the Job can be deleted.
        """
        return self.get_status_as_string() in ("WAITING", "EDITING", "READY")

    def is_finished(self):
        """ Determines if the Job is finished (success or error)

        Returns:
            Boolean indicating if the Job has already been finished.
        """
        return self.get_status_as_string() in ["SUCCESS", "ERROR"]

    def __unicode__(self):
        """ Returns a string representing the Job """
        return "ID: %d, %s, %s, Prio: %d, User: %d" % (self.id, self.name, self.get_status_as_string(), self.priority, self.user.user_id)


class Event(models.Model):
    """ Represents a job event.

    A job can have multiple events (such as start or end). This class associates
    job events with dates.

    Attributes:
        event_type: Enumeration which can hold one of the following values: START, END, CREATION, SUBMISSION, OPTIMIZATION, CHECK
        time: DateTime indicating when the event occured.
        job: Job object containing the job this event belongs to.

    Author: Simon Hessner
    """
    CREATION        = 0 # -> Editing
    FINALIZING      = 1 # -> Waiting
    CHECK           = 2 # -> Waiting
    OPTIMIZATION 	= 3 # -> Ready
    START           = 4 # -> Running
    END             = 5 # -> Error / Success
    CHOICES  		= ((START,			'START'),
			   		   (END, 			'END'),
			   		   (CREATION,   	'CREATION'),
			   		   (OPTIMIZATION,   'OPTIMIZATION'),
			   		   (CHECK, 		  	'CHECK'),
                       (FINALIZING,     'FINALIZING'))

    event_type = models.IntegerField(choices=CHOICES)
    time 	   = models.DateTimeField(auto_now_add = True)
    job 	   = models.ForeignKey('Job', on_delete = models.CASCADE)

    @staticmethod
    def type_is_allowed(event_type):
        """ Static method that determines if a given event_type is an allowed event type.

        Args:
            event_type: String containing the type.

        Returns:
            Boolean indicating if the type is allowed.
        """
        return event_type in [c[1] for c in Event.CHOICES]

    @staticmethod
    def _get_type_by_string(event_type):
        """ Static helper method that returns the integer representation of an event_type given as string. """
        if not Event.type_is_allowed(event_type.upper()):
            raise ModelError("Invalid event_type provided")
        else:
            for c in Event.CHOICES:
                if c[1] == event_type.upper():
                    return c[0]
        return None

    def set_type_by_string(self, event_type):
        """ Changes the type of the event.

        Args:
            event_type: String representing the event type.
        """
        self.event_type = self._get_type_by_string(event_type)

    def get_type_as_string(self):
        """ Returns the string representation of the type of the Event.

        Returns:
            String containing event type.
        """
        return self.get_event_type_display()

    @staticmethod
    def get_event(job, event):
        """ Static method that returns a special event of a job.

        Args:
            job: Job object.
            event: String containing event type.

        Returns:
            Event if (job, event) is an existing combination. None if not.
        """
        if not Event.type_is_allowed(event):
            raise ModelError("Event string not allowed: %s" % str(event))

        etype = Event._get_type_by_string(event)
        res = Event.objects.filter(job=job, event_type=etype)
        if not len(res) > 0:
            return None
        return res[0]

    class Meta:
        """ required by django to create an unique index over multiple fields. """
        unique_together = ("job", "event_type")

    def __unicode__(self):
        """ Returns a string representing the Event """
        return "%s, %s, %s (%d)" % (self.get_type_as_string(), str(self.time), self.job.name, self.job.id)



class Operation(models.Model):
    """ Represents a single operation (move, delete, copy or rename) which
    is containted in a job.

    Attributes:
        op_type: Enumeration (COPY, DELETE, MOVE, RENAME)
        status: Enumeration (WAITING, RUNNING, SUCCESS, ERROR)
        job: Job object this operation belongs to.
        source: String containing the path to the source of the operation.
        target: String containing the path to the target of the operation.
            In case of op_type = RENAME it only contains a new file / directory name.
        start: DateTime indicating when the operation startet. None if WAITING.
        end: DateTime indicating when the operation ended: None if WAITING or RUNNING

    Author: Simon Hessner
    """
    COPY 	= 0
    DELETE 	= 1
    MOVE 	= 2
    RENAME 	= 3
    CHOICES = ((COPY, 'COPY'),
			   (DELETE, 'DELETE'),
			   (MOVE, 'MOVE'),
			   (RENAME, 'RENAME'))

    WAITING = 0
    RUNNING = 1
    SUCCESS = 2
    ERROR   = 3
    CHOICES2 = ((WAITING, 'WAITING'),
                (RUNNING, 'RUNNING'),
                (SUCCESS, 'SUCCESS'),
                (ERROR, 'ERROR'))

    op_type = models.IntegerField(default=MOVE, choices=CHOICES)
    status  = models.IntegerField(default=WAITING, choices=CHOICES2)
    job 	= models.ForeignKey('Job', on_delete = models.CASCADE)
    source 	= models.CharField(max_length = 512) # source path
    target 	= models.CharField(max_length = 512, null = True, blank = True) # target path or new filename if op_type is RENAME
    start 	= models.DateTimeField(blank = True, null = True)
    end 	= models.DateTimeField(blank = True, null = True)

    def status_is_allowed(self, status):
        """ Determines if the given status string represents a valid status.

        Args:
            status: String representing a status.

        Returns:
            Boolean indicating if status is valid.
        """
        return status.upper() in [c[1] for c in self.CHOICES2]

    def set_status_by_string(self, status):
        """ Changes the status of the Operation.

        Args:
            status: String representing the new status.

        Raises:
            ModelError if status is invalid.
        """
        if not self.status_is_allowed(status.upper()):
            raise ModelError("Invalid status provided")
        else:
            for c in self.CHOICES2:
                if c[1] == status.upper():
                    self.status = c[0]

    def set_type_by_string(self, op_type):
        """ Changes the operation type.

        Args:
            op_type: New operation type as String.

        Raises:
            ModelError if type is invalid.
        """
        if not self.type_is_allowed(op_type.upper()):
            raise ModelError("Invalid type provided")
        else:
            for c in self.CHOICES:
                if c[1] == op_type.upper():
                    self.op_type = c[0]

    def type_is_allowed(self, op_type):
        """ Determines if the given operation type is valid.

        Args:
            op_type: String containing operation type.

        Returns:
            Boolean indicating if type is valid.
        """
        return op_type.upper() in [c[1] for c in self.CHOICES]

    def get_status_as_string(self):
        """ Returns the status of the operation as string.

        Convenience method that makes it easier to get the status as string
        because accessing the status attribute would deliver an integer.

        Returns:
            String containing operation status.
        """
        return self.get_status_display()

    def get_type_as_string(self):
        """ Returns the type of the operation as string.

        Convenience method that makes it easier to get the type as string
        because accessing the type attribute would deliver an integer.

        Returns:
            String containing operation type.
        """
        return self.get_op_type_display()

    def __unicode__(self):
        """ Returns a string representing the Operation """
        return "%d: %s %s <%s> %s (%d)" % (self.id, self.get_status_as_string(), self.source, self.get_type_as_string(), self.target, self.job.id)


class Server(models.Model):
    """ Represents a storage server.

    Attributes:
        capacity: Integer describing how many bytes can be stored on the server.
        last_activity: DateTime indicating when the storage server polled the last time.
        token: String containing an access token the storage server uses to authenticate at the JMS.
        hostname: String containing the hostname of the storage server.

    Author: Simon Hessner, Julian Schuh
    """
    capacity 		= models.BigIntegerField(default=1024) # in bytes
    last_activity 	= models.DateTimeField(auto_now_add = True)
    token			= models.CharField(max_length=32, unique = True)
    hostname		= models.CharField(max_length=512, db_index = True, unique = True)
    is_active       = models.BooleanField(default = False, null = None, blank = None)

    def __init__(self, *args, **kwargs):
        """ Initializes Server instance. """
        super(Server, self).__init__(*args, **kwargs)

        self._old_hostname = self.hostname

    def save(self, *args, **kwargs):
        """ Handles updating of the index when the hostname changed. """
        if self.capacity < 0:
            raise ModelError("Capacity must be positive.")

        super(Server, self).save(*args, **kwargs)

        accessor = IndexAccessor.get_accessor()
        accessor.enter_global_mode()

        # successfully saved, check for server root node
        if self.hostname != self._old_hostname or not accessor.has_node([self.hostname]):
            # theres new node matching the servers new hostname
            if not accessor.has_node([self.hostname]):
                node_values = {
                    'name': self.hostname,
                    'owner_user': 0,
                    'owner_group': 0,
                    'size': 0,
                    'permissions_oct': 511,
                    'type': 'SERVER'
                }

                if self._old_hostname and accessor.has_node([self._old_hostname]):
                    accessor.update_node([self._old_hostname], node_values)
                else:
                    accessor.add_node(accessor.ROOT_PATH, node_values)

        self._old_hostname = self.hostname

    def delete(self):
        accessor = IndexAccessor.get_accessor()
        accessor.enter_global_mode()

        if accessor.has_node([self.hostname]):
            node = accessor.get_node([self.hostname])
            accessor.delete_node(node)

        super(Server, self).delete()

    def generate_token(self):
        """ Generates an unique server token and stores it in Server.token """
        generator = TokenGenerator(32)
        token = generator.get_token()

        while len(Server.objects.filter(token__exact=token)) > 0:
            token = generator.get_token()

        self.token = token

    def __unicode__(self):
        """ Returns a string representing the Server """
        return "%d: %s (active: %s)" % (self.id, self.hostname, str(self.is_active))


class Protocol(models.Model):
    """ Represents a transfer protocol that is supported by a certain storage server

    Author: Simon Hessner

    This model is not meant to represent a single protocol but a protocol
    in combination with exactly one server. This allows defining a priority
    for this protocol on the server.

    Attributes:
        name: String containing the protocol name.
        priority: Integer indicating the priority of the protocol.
        server: Server object this Protocol object belongs to.
    """
    name 		= models.CharField(max_length=32)
    priority	= models.IntegerField()
    server 		= models.ForeignKey('Server', on_delete = models.CASCADE, db_index = True)

    class Meta:
        """ required by django to create an unique index over multiple fields. """
        unique_together = ("server", "name")

    def __unicode__(self):
        """ Returns a string representing the Protocol """
        return "%d: %s (%d) Server: %d" % (self.id, self.name, self.priority, self.server.id)


class Connection(models.Model):
    """ Represents the connection between two storage servers.

    Associcates transfer speed over a specific protocol with the connection.

    Author: Simon Hessner

    Attributes:
        source: Server object containing source server.
        target: Server object containing target server.
        protocol: String containing protocol name.
        speed: Integer containing connection speed (via protocol)
    """
    source 			= models.ForeignKey('Server', related_name='source_id')
    target			= models.ForeignKey('Server', related_name='target_id')
    protocol 		= models.CharField(max_length = 32, null=False, blank=False)
    speed 			= models.IntegerField(default = 10000) # in kbit/s

    class Meta:
        """ required by django to create indexes over multiple fields. """
        unique_together = ("source", "target", "protocol")
        index_together  = [["source", "target"]]

    def save(self, *args, **kwargs):
        """ The save method ensures that source is not target

        Raises:
            ModelError: if target = source
        """
        if self.source == self.target:
            raise ModelError("No loop connection allowed!")

        super(Connection, self).save(*args, **kwargs)

    def __unicode__(self):
        """ Returns a string representing the Connection """
        return "%d: %s -> %s (%s) %d" % (self.id, self.source.hostname, self.target.hostname, self.protocol, self.speed)


class LogEntry(models.Model):
    """Represents a log entry.

    A log entry can be associated with various other objects such as
    User, Job, Operation or Server.

    Author: Simon Hessner

    Attributes:
        user: User object containing the user that owns the operation / job. None if global entry.
        is_error: Boolean indicating if the entry describes an error.
        created_on: DateTime containing the time of the log entry.
        message: String containing log entry message.
        entry_type: Because the LogEntry can belong to various different objects
            the type of the referenced object must be stored. Django realizes this
            via integers. entry_type contains an integer.
            Possible types are: Job, Operation, Server, User.
        foreign_id: Integer containing the ID of the referenced object.
        foreign_obj: The actual referenced object.
    """
    is_error 	= models.BooleanField(default = False)
    created_on 	= models.DateTimeField(auto_now_add = True)
    message 	= models.TextField()

    #Begin of polymorphic foreign key
    #limit describes which models can be referenced
    _limit		= models.Q(app_label = 'jms', model='job') \
                | models.Q(app_label = 'jms', model='operation') \
                | models.Q(app_label = 'jms', model='server') \
                | models.Q(app_label = 'jms', model='user')
    #entry_type stores the model id (handled by django!)
    entry_type  = models.ForeignKey(ContentType, limit_choices_to = _limit, related_name="log_entries", null = True, blank = True, db_index = True)
    #This is the id of the referenced object. handled by django!
    foreign_id  = models.PositiveIntegerField(null = True, blank = True, db_index = True)
    #needed by django for the relationship. do not use!
    foreign_obj = generic.GenericForeignKey('entry_type', 'foreign_id')

    def get_subject(self):
        """ Returns the object referenced by the log entry.

        Return:
            Object foreign_obj(entry_type, forgeign_id)
        """
        return self.entry_type.get_object_for_this_type()

    @staticmethod
    def get_log_entries(model, id):
        """ Returns all log entries of the specified object.

        Args:
            model: Model class of the object.
            id: Primary Key of the object.

        Returns:
            Set of log entries that belong to (model, id)
        """
        entry_type = ContentType.objects.get(model=model._meta.module_name)
        return LogEntry.objects.filter(entry_type=entry_type, foreign_id=id)

    def get_type_as_string(self):
        """ Returns the type of this entry as a human readable string. """
        if not self.foreign_obj:
            return "global_message"
        return ContentType.objects.get_for_model(self.foreign_obj)

    #End of polymorphic foreign key

    class Meta:
        """ required by django to create indexes over multiple fields. """
        index_together = [["entry_type", "foreign_id"]]

    @staticmethod
    def get_allowed_types():
        """ Returns a list of all allowed log entry types. """
        return ["job", "operation", "server", "user"]

    def __unicode__(self):
        """ Returns a string representing the LogEntry """
        return "%d: %s" % (self.id, str(self.get_type_as_string()))
