# -*- coding: utf-8 -*-

from rest_framework.response import Response
from rest_framework import status
from jms.rest_api.views.bigdatabrokerapiview import BigDataBrokerAPIView
from jms.models import Operation, Job
from jms.rest_api.serializers.operation import OperationSerializer, OperationPUTSerializer
from jms.exception.api import *
from jms.scheduler.operation import OperationPlausibilityChecker
from django.conf import settings
from jms.utils import import_class

class OperationView(BigDataBrokerAPIView):
    """
        abstract
        Provides various utility methods
    """
    def check_permissions_of_job(self, job_id):
        job = Job.objects.filter(user=self.user_id, id=job_id)
        if len(job) != 1:
            raise Http404("Job does not exist or you have no permissions!")

    def check_job_status(self, job):
        if job.get_status_as_string() != "EDITING":
            raise Http404("Job must have the status EDITING")

    def get_job(self, job_id):
        try:
            return Job.objects.get(id=job_id)
        except Job.DoesNotExist:
            raise Http404 ("Job does not exist.")

class OperationList(OperationView):
    """
        Handles REST-API requests to the list view of the operation resource. Can be
        used to get a list of operations of a job or to add a new operation to a specified job.
    """
    def get(self, request, job_id, format=None):
        """
            Returns a list of operations associated with a specified job. This method is called when the client uses HTTP-GET for the REST-API request.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request.
            Returns
                Response object: Contains HTTP status (error or success) and a list of operations formatted in JSON.
        """
        self.check_connection(request)
        if not self.check_admin(request):
            self.check_permissions_of_job(job_id)
        job = self.get_job(job_id)
        operations = Operation.objects.filter(job=job)
        serializer = OperationSerializer(operations, many=True)
        return Response(serializer.data)   
        
    def put(self, request, job_id, format=None):
        """
            Creates a new operation (HTTP-PUT) and appends it to a job. Returns the new oper- ation object to allow the user to delete it by using the operation ID.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request.
            Returns
                Response object: Contains HTTP status (error or success) and the new operation ob- ject formatted in JSON.
        """
        self.check_connection(request)
        if not request.DATA or len(request.DATA) > 4:
            raise BadRequestHttp400("No parameters or too many parameters. Only operation type, source and target and optional force.")

        if not self.check_admin(request):
            self.check_permissions_of_job(job_id)
        job = self.get_job(job_id)
        self.check_job_status(job)

        if not request.DATA.has_key("type"):
            raise BadRequestHttp400("Operation type is missing")
        op_type = str(request.DATA["type"]).upper()

        # handle target parameter by hand because it is optional and
        # the serializer does not know when it is needed and when not
        if op_type == "DELETE":
            target = None
            if request.DATA.has_key("target"):
                raise BadRequestHttp400("A delete operation must not have a target!")
        else:
            if not request.DATA.has_key("target"):
                raise BadRequestHttp400("Target is missing!")
            else:
                target = request.DATA["target"].encode("utf8")

        serializer = OperationPUTSerializer(data=request.DATA) 
        if serializer.is_valid(): 
            source = request.DATA["source"].encode("utf8")         

            operation = serializer.object
            if not operation.type_is_allowed(op_type):
                raise BadRequestHttp400("invalid operation type")

            operation.set_type_by_string(op_type)
            operation.job = job   

            # if there is no force parameter in the request the default
            # behaviour is used.
            # force means that the OperationPlausibilityChecker is not executed!
            force = settings.DEFAULT_OPERATION_CHECK_BEHAVIOUR             
            if request.DATA.has_key("force"):
                force = bool(request.DATA["force"])

            if not force: 
                # the operation will be checked for plausibility.
                AccessorClass = import_class(settings.JMS_INDEX_ACCESSOR_CLASS)
                checker = OperationPlausibilityChecker(AccessorClass(self.token), op_type, source, target)
                error = checker.check()
                if error:
                    # the operation is not plausible
                    raise BadRequestHttp400(error)
            
            serializer.save() 
            return Response(serializer.data, status=status.HTTP_201_CREATED) 
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OperationDetail(OperationView):
    """
        Handles REST-API requests to detail view of the operation resource. Can be
        used to get details of a specific operation in a specific job or to delete operations.
    """
    def get_operation(self, op_id, job_id):
        """
            helper methode
        """
        op = Operation.objects.filter(id=op_id, job=job_id)
        if len(op) != 1:
            raise Http404("Operation does not exist or no access.")
        return op[0]

    def get(self, request, job_id, op_id, format=None):
        """
            Returns information of a specific job operation via HTTP-GET.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and information about the operation (e.g. status, type, ...) formatted in JSON.
        """
        self.check_connection(request)
        if not self.check_admin(request):
            self.check_permissions_of_job(job_id)
        operation = self.get_operation(op_id, job_id)
        serializer = OperationSerializer(operation)
        return Response(serializer.data)

    def delete(self, request, job_id, op_id, format=None):
        """
            Deletes a specific operation of a specific job. Called when using HTTP-DELETE.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request.
            Returns
                Response object: Contains HTTP status (error or success)
        """
        self.check_connection(request)
        if not self.check_admin(request):
            self.check_permissions_of_job(job_id)
        job = self.get_job(job_id)
        self.check_job_status (job)
        operation = self.get_operation(op_id, job_id)
        operation.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

