from rest_framework.response import Response
from rest_framework import status
from jms.rest_api.views.bigdatabrokerapiview import BigDataBrokerAPIView
from jms.models import Server
from jms.rest_api.serializers.server import ServerSerializer
from jms.exception.api import *
from jms.exception.model import ModelError

class ServerList(BigDataBrokerAPIView):
    """
        Handles REST-API requests to the list view of the server resource. Can be used
        to get a list of storage servers that are registered at the Job Management Server.
    """
    def get(self, request, format=None):
        """
            Gets called when the client is using HTTP-GET for the REST-API request. Returns a list of storage servers registered at the Job management Server.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and a list of storage servers formatted in JSON.
        """
        self.check_connection(request)
        server = Server.objects.all() 
        if not self.check_admin(request):
            server = server.filter(is_active=True)
        serializer = ServerSerializer(server, many=True) 
        return Response(serializer.data)
        
class ServerDetail(BigDataBrokerAPIView):
    """
        Handles REST-API requests to the detail view of the server resource. Can be used to get details of a specific storage server and to delete or modify specific storage servers.
    """
    def get_server(self, server_id):
        """
            helper methode
        """
        try:
            return Server.objects.get(id=server_id)
        except Server.DoesNotExist:
            raise Http404 ("Server does not exist.")
            
    def get(self, request, server_id, format=None):
        """
            When a client calls the server detail view using HTTP-GET this method returns infor- mation about a specific storage server.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request.
            Returns
                Response object: Contains HTTP status (error or success) and information about the storage server formatted in JSON.
        """
        self.check_connection(request)
        if not self.check_admin(request):
            raise Forbidden403("Admin rights are required.")
        server = self.get_server(server_id)
        serializer = ServerSerializer(server)
        return Response(serializer.data)

    def post(self, request, server_id, format=None):
        """
            Modifies information of a specific storage server when HTTP-POST is used for the REST-API request.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and the modified storage server object formatted in JSON.
        """
        self.check_connection(request)
        if not request.DATA or len(request.DATA) > 2:
            raise BadRequestHttp400("No parameter or too many parameters. Only capacity and hostname is changable.")

        if not self.check_admin(request):
            raise Forbidden403("Admin rights are required.")

        server = self.get_server(server_id)
        serializer = ServerSerializer(server, data=request.DATA)

        if serializer.is_valid():
            try:
                serializer.save()
            except ModelError:
                raise BadRequestHttp400("Capacity must be positive.")
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, server_id, format=None):
        """
            Deletes a specific storage server when the client uses HTTP-DELETE.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success).
        """
        self.check_connection(request)
        if not self.check_admin(request):
            raise Forbidden403("Admin rights are required.")

        server = self.get_server(server_id)
        server.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
