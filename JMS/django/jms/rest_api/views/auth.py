from rest_framework.views import APIView
from rest_framework.response import Response
from jms.rest_api.views.bigdatabrokerapiview import BigDataBrokerAPIView
from jms.rest_api.serializers.accesstoken import AccessTokenSerializer
from jms.auth.simple_authentication_handler import SimpleAuthenticationHandler
from jms.exception.api import *
from rest_framework.throttling import AnonRateThrottle

class Auth(BigDataBrokerAPIView):
    """
        This class handles authentication requests that are sent by clients (e.g. com- mand line interface or Webclient) via
        the REST-API. It uses an AuthenticationHandler to determine if the authentication data is valid and to get
        an AccessToken which is then returned to the client. All data that is sent or received is processed by the AuthSerializer.
    """
    throttle_classes = (AnonRateThrottle,)

    def post(self, request):
        """
            Handles authentication requests sent to the REST-API via HTTP-POST and returns an access token on success or an error message if the user could not be authenticated.

            Parameters
                request: Object that contains all necessary information about the HTTP request. 
                
            Returns
                Response object: Contains HTTP status (error or success) and (if the authentication was successful)
                an access token that can be used by the client to authenticate on further requests or an error message.
        """
        self.check_ssl(request)
        if request.META.has_key('HTTP_X_BDB_TOKEN'):
            raise BadRequestHttp400("Token must not be provided by client.")    # its illegal to get a token while using another token

        auth_handler = SimpleAuthenticationHandler(request) # initialize authentication handler and pass the whole request

        if auth_handler.authenticate():
            token = auth_handler.get_access_token()

            serializer = AccessTokenSerializer(token)
            return Response(serializer.data)

        else:
            raise Forbidden403(auth_handler.get_error_message())
