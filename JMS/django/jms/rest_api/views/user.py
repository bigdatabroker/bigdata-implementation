from rest_framework.response import Response
from rest_framework import status
from jms.rest_api.views.bigdatabrokerapiview import BigDataBrokerAPIView
from jms.rest_api.serializers.user import UserSerializer
from jms.exception.api import *
from jms.models import User

class UserDetail(BigDataBrokerAPIView):
    """
        Handles REST-API requests to the detail view of the user resource. Can be used
        to get details of a specific user or to modify user details like the user priority.
    """
    def get_user(self, user_id):
        """
            helper methode
        """
        try:
            return User.objects.get(user_id=user_id)
        except User.DoesNotExist:
            raise Http404 ("User does not exist.")

    def get(self, request, user_id, format=None):
        """
            Returns information of a specific user if the client uses HTTP-GET for the request.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and user data formatted in JSON.
        """
        user_id = int(user_id) #for comparison!
        
        self.check_connection(request)
        # the admin can see all user details and each user can see his own!
        if not user_id == self.user_id and not self.check_admin(request):
            raise Forbidden403("Admin rights required.")

        user = self.get_user(user_id)
        serializer = UserSerializer(user)
        return Response(serializer.data)
      
    def post(self, request, user_id, format=None):
        """
            Modifies information of a user. (HTTP-POST)
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and the modified user ob- ject formatted in JSON.
        """
        self.check_connection(request)
        if not request.DATA or len(request.DATA) > 2:
            raise BadRequestHttp400("No parameter or too many parameters. Only name and priority.")
        if not self.check_admin(request):
            raise Forbidden403("Admin rights required.")
        if request.DATA.has_key("priority") and request.DATA["priority"] < 1:
            raise BadRequestHttp400("priority must be > 1.")
            
        user = self.get_user(user_id)
        serializer = UserSerializer(user, data=request.DATA, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        