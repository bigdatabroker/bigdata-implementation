from rest_framework.response import Response
from rest_framework import status
from jms.rest_api.views.bigdatabrokerapiview import BigDataBrokerAPIView
from jms.models import Connection, Server
from jms.rest_api.serializers.connection import ConnectionSerializer, ConnectionPUTSerializer
from jms.exception.api import *
from django.db import IntegrityError

class ConnectionList(BigDataBrokerAPIView):
    """
        Handles REST-API requests to the list view of the server connection resource.
        Can be used to get a list of speeds from a specific storage server to other storage servers.
    """
    def get_server(self, source_server_id):
        """
            helper methode
        """
        try:
            return Server.objects.get(id=source_server_id)
        except Server.DoesNotExist:
            raise Http404("Server does not exist!")

    def get(self, request, source_server_id, format=None):
        """
            Returns a list of storage servers a specific storage server is connected to and the speed of this connection with different protocols. This method is called on HTTP-GET requests.
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and a list of connection speeds formatted in JSON.
        """
        self.check_connection(request)
        connections = Connection.objects.filter(source=source_server_id)
        serializer = ConnectionSerializer(connections, many=True) 
        return Response(serializer.data)

    def put(self, request, source_server_id):     
        """
            Adds the connection speed from a specific server to another server via different com- munication protocols. This method is called on HTTP-PUT requests.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and the new connection object formatted in JSON.
        """   
        self.check_connection(request)
        if not request.DATA or len(request.DATA) > 3:            
            raise BadRequestHttp400("No parameters or too many parameters. Only target, protocol and speed")
        if not self.check_admin(request):
            raise Forbidden403("Admin rights required.")
        serializer = ConnectionPUTSerializer(data=request.DATA) 
        if serializer.is_valid(): 
            source_server = self.get_server(source_server_id)
            target_server = serializer.object.target
            if source_server == target_server:
                raise BadRequestHttp400("no connection loops allowed!")
            serializer.object.source=source_server 
            try:
                serializer.save() 
            except IntegrityError:                
                raise BadRequestHttp400("Connection already exists.") 
            return Response(serializer.data, status=status.HTTP_201_CREATED)                          
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  

class ConnectionDetail(BigDataBrokerAPIView):
    """
        Handles REST-API requests to detail view of the connection resource. Can be
        used to get the speed between two storage servers via a specific protocol.
    """
    def get_connection(self, source_server_id, target_server_id, protocol):
        """
            helper methode
        """
        try:
            return Connection.objects.get(source=source_server_id, target=target_server_id, protocol=protocol)
        except Connection.DoesNotExist:
            raise Http404("Connection does not exist!")
            
    def get(self, request, source_server_id, target_server_id, protocol, format=None):
        """
            Returns information of a specific connection when the client uses HTTP-GET.

            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and the new connection object formatted in JSON.
        """
        self.check_connection(request)
        connection = self.get_connection(source_server_id, target_server_id, protocol)
        serializer = ConnectionSerializer(connection)
        return Response(serializer.data)

    def post(self, request, source_server_id, target_server_id, protocol, format=None):
        """
            Modifies information of a storage server connection, e.g. speed of a protocol. This method is called on HTTP-POST requests.           
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and the modified connec- tion object formatted in JSON.
        """
        self.check_connection(request)
        if not request.DATA or len(request.DATA) > 1:
            raise BadRequestHttp400("No parameters or too many parameters. Only speed")
        if not self.check_admin(request):
            raise Forbidden403("Admin rights required.")

        if request.DATA.has_key("target") or request.DATA.has_key("source") or request.DATA.has_key("protocol"):
            raise BadRequestHttp400("Only speed can be modified")        

        connection = self.get_connection(source_server_id, target_server_id, protocol)
        serializer = ConnectionSerializer(connection, data=request.DATA, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, source_server_id, target_server_id, protocol, format=None):
        """
            Deletes a specific connection. Method is called when the client uses HTTP-DELETE for the REST-API request.

            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success).
        """
        self.check_connection(request)
        if not self.check_admin(request):
            raise Forbidden403("Admin rights required.")
        connection = self.get_connection(source_server_id, target_server_id, protocol)
        connection.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)