from rest_framework.views import APIView
from jms.exception.api import *
from rest_framework.exceptions import AuthenticationFailed
from jms.models import AccessToken, User, Job
from django.utils import timezone

from django.conf import settings

class BigDataBrokerAPIView(APIView):
    """
        abstract class
        Represents the superclass of all REST-API views
    """
    def __init__(self):
        self.token = None
        self.token_str = None
        self.user_id = 0
        super(BigDataBrokerAPIView, self).__init__()

    def get_userdata_of_requester(self):
        """
            Get the user object of the requester
        """
        try:
            return User.objects.get(user_id=self.user_id)
        except User.DoesNotExist:
            raise Http404("User does not exist!")

    def check_admin(self, request):
        """
            Checks whether the requester is admin
        """
        user = self.get_userdata_of_requester()
        return user.is_admin

    def check_ssl(self, request):
        """
            It is checked whether the SSL network protocol is used for secure transmission of data for the connection
        """
        if settings.REQUIRE_SSL and not request.is_secure():
            raise Forbidden403('No SSL connection')

    def check_connection(self, request):
        """
            Checks whether the connection is secure and the user is authenticated
        """
        self.check_ssl(request)
        if not request.META.has_key('HTTP_X_BDB_TOKEN'):
            raise AuthenticationRequiredHttp401('No token provided.')

        auth_data = self._parse_token(request.META['HTTP_X_BDB_TOKEN'])

        self.user_id = auth_data['user_id']
        self.token_str = auth_data['token']

        try:
            result = AccessToken.objects.get(token=self.token_str)
        except AccessToken.DoesNotExist:
            raise AuthenticationFailed('Invalid access token specified.')

        if result.user.user_id != self.user_id:
            raise AuthenticationFailed('Specified credentials invalid.')

        if result.expires_on is not None and result.expires_on < timezone.now():
            raise AuthenticationFailed('Specified token expired.')

        self.token = result
        return self.token

    def _parse_token(self, token):
        """
            Parse the access token of the request
        """
        auth_data = token.split(':')


        if len(auth_data) != 2:
            raise AuthenticationFailed('<ID:Token> expected')

        try:
            user_id = int(auth_data[0])
        except ValueError:
            raise AuthenticationFailed('User ID must be integer')

        token_str = auth_data[1]
        if token_str == "":
            raise AuthenticationFailed("Token must not be empty")

        return {'user_id' : user_id, 'token' : token_str}
