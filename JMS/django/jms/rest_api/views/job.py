from rest_framework.response import Response
from rest_framework import status
from jms.rest_api.views.bigdatabrokerapiview import BigDataBrokerAPIView
from jms.models import Job, AccessToken, Event
from jms.rest_api.serializers.job import JobSerializer
from jms.exception.api import *
from jms.exception.model import *
from jms.scheduler.plausibility import *
from jms.worker.optimizer import JobOptimizer

class JobView (BigDataBrokerAPIView):
    """
        abstract
        Provides various utility methods
    """
    def get_job(self, request, job_id):
        if self.check_admin(request):
            return self.get_job_with_admin_rights(job_id)
        else:
            return self.get_job_with_requester_rights(job_id)

    def get_job_with_admin_rights(self, job_id):
        try:
            return Job.objects.get(id=job_id)
        except Job.DoesNotExist:
            raise Http404("Job does not exist!")

    def get_job_with_requester_rights(self, job_id):
        job = Job.objects.filter(user=self.user_id, id=job_id)
        if len(job) != 1:
            raise Http404("Job does not exist or you have no permissions!")
        return job[0]

    def check_job_status(self, job, status):
        if job.get_status_as_string() != status:
            raise Http404("Job must have the status " + str(status))

class JobList(JobView):
    """
        Handles REST-API requests to the list view of the job resource. Can be used to
        get a list of jobs that belong to the currently logged in user or to create a new job.
    """
    def get(self, request, format=None):
        """
            Returns a list of jobs that belong to the user who is currently logged in. This method is called when the client uses HTTP-GET.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and a list of jobs formatted in JSON.
        """
        self.check_connection(request)
        if self.check_admin(request):
            jobs = Job.objects.all().order_by('-id') #-id ==> DESC
        else:
            jobs = Job.objects.filter(user=self.user_id).order_by('-id') #-id ==> DESC
        serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)

    def put(self, request):
        """
            Creates a new job via HTTP-PUT and returns it to the client. The client can use the returned object to identify the job and to add operations to it.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and the new job object formatted in JSON.
        """
        self.check_connection(request)
        if not request.DATA or len(request.DATA) > 2:
            raise BadRequestHttp400("No parametes or too many parameters. Only priority and name")
        user = self.get_userdata_of_requester()
        serializer = JobSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.object.user=user
            serializer.object.token = self.token
            serializer.save()

            if not Event.get_event(serializer.object, "CREATION"):
                event = Event(job=serializer.object)
                event.set_type_by_string("CREATION")
                event.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class JobDetail(JobView):
    """
        Handles REST-API requests to the detail view of the job resource. This class is
        used to get detail information about a specific job or to delete/modify a job.
    """
    def get(self, request, job_id, format=None):
        """
            Returns all information (e.g. status, date of creation, amount of operations, ...) about a specific job. This method is called when the client uses HTTP-GET.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success) and the job object format- ted in JSON.
        """
        self.check_connection(request)
        #check if the requester is an admin, then get_job_with_admin_rights, else get_job_with_requester_rights
        job = self.get_job(request, job_id)
        serializer = JobSerializer(job)
        return Response(serializer.data)

    def post(self, request, job_id, format=None):
        """
            Changes information (e.g. name or priority) of a specified job. To get this method called the client must use HTTP-POST.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request.
            Returns
                Response object: Contains HTTP status (error or success) and the modified job for- matted in JSON.
        """
        self.check_connection(request)
        if not request.DATA or len(request.DATA) > 2:
            raise BadRequestHttp400("No parameters or too many parameters. Only priority and name")
        #check if the requester is an admin, then get_job_with_admin_rights, else get_job_with_requester_rights
        job = self.get_job(request, job_id)
        self.check_job_status(job, "EDITING")

        serializer = JobSerializer(job, data=request.DATA, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, job_id, format=None):
        """
            Deletes a specified job if the client uses HTTP-DELETE for the REST-API request.
            
            Parameters
                request: Object that contains all necessary information about the HTTP request. 
            Returns
                Response object: Contains HTTP status (error or success).
        """
        self.check_connection(request)
        #check if the requester is an admin, then get_job_with_admin_rights, else get_job_with_requester_rights
        job = self.get_job(request, job_id)
        
        if not job.is_deletable():
            raise BadRequestHttp400("Job cannot be deleted anymore!")
        #delete all operation of the job to be deleted
        for operation in Operation.objects.filter(job=job):
            operation.delete()

        job.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

class JobFinalize(JobView):
    """
        Finalizes the specified job
    """
    def post(self, request, job_id, format=None):
        """
            Finalizes the specified job.
            
            Parameters
                none
            Returns
                Response object: Contains HTTP status (error or success).
        """
        self.check_connection(request)
        #check if the requester is an admin, then get_job_with_admin_rights, else get_job_with_requester_rights
        job = self.get_job(request, job_id)
        if not job.is_finalizable():
            raise BadRequestHttp400("Job is not finalizable anymore!")

        manager = PlausibilityCheckManager(self.token, job)
        manager.check()

        error_operations = manager.get_errors()
        if len(error_operations) != 0:            
            return Response(error_operations, status=status.HTTP_400_BAD_REQUEST)
        if not manager.is_critical_errors():
            # After success of plausibility checking create an event for this.
            if not Event.get_event(job, "CHECK"):
                event = Event(job=job)
                event.set_type_by_string("CHECK")
                event.save()

            job.finalize()           
            job.save()       

            # The optimization is executed in background
            optimizer = JobOptimizer(job=job)
            optimizer.execute()        

            return Response({"detail" : "Job is finalized now."}, status=status.HTTP_200_OK)
        
        raise BadRequestHttp400(str(manager.get_errors()))


