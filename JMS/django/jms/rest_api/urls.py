from django.conf.urls import patterns, url
from jms.rest_api.views.job import JobList, JobDetail, JobFinalize
from jms.rest_api.views.operation import OperationList, OperationDetail
from jms.rest_api.views.server import ServerList, ServerDetail
from jms.rest_api.views.log import LogList, LogDetail
from jms.rest_api.views.connection import ConnectionList, ConnectionDetail
from jms.rest_api.views.node import NodeList, NodeDetail
from jms.rest_api.views.user import UserDetail
from jms.rest_api.views.auth import Auth

urlpatterns = patterns('jms.rest_api.views',
    url(r'^auth/$', Auth.as_view()),

    url(r'^job/$', JobList.as_view()),
    url(r'^job/(?P<job_id>[0-9]+)/$', JobDetail.as_view()),
    url(r'^job/(?P<job_id>[0-9]+)/finalize/$', JobFinalize.as_view()),

    url(r'^job/(?P<job_id>[0-9]+)/operation/$', OperationList.as_view()),
    url(r'^job/(?P<job_id>[0-9]+)/operation/(?P<op_id>[0-9]+)/', OperationDetail.as_view()),

    url(r'^server/$', ServerList.as_view()),
    url(r'^server/(?P<server_id>[0-9]+)/$', ServerDetail.as_view()),

    url(r'^server/(?P<source_server_id>[0-9]+)/connection/$', ConnectionList.as_view()),
    url(r'^server/(?P<source_server_id>[0-9]+)/connection/(?P<target_server_id>[0-9]+)/(?P<protocol>[a-zA-Z0-9%-_.~]+)/$', ConnectionDetail.as_view()),

    url(r'^logentry/$', LogList.as_view()),
    url(r'^job/(?P<job_id>[0-9]+)/logentry/$', LogDetail.as_view()),

    url(r'^node/(?P<path>.+)/childs/$', NodeList.as_view()),
    url(r'^node/(?P<path>.+)/$', NodeDetail.as_view()),

    url(r'^user/(?P<user_id>[0-9]+)/$', UserDetail.as_view()),
)
