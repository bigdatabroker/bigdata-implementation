from rest_framework import serializers
from jms.models import LogEntry

class LogSerializer(serializers.ModelSerializer):
    """
        This class handles the serialization and deserialization of log entries.
        This allows sending and receiving log entries in the JSON format via the REST-API.
    """
    type = serializers.SerializerMethodField('get_type_as_string')

    class Meta:
        model = LogEntry
        fields = ('id', 'created_on', 'foreign_id', 'message', 'type')
        read_only_fields = ('id', 'created_on', 'foreign_id', 'message')

    def get_type_as_string(self, entry):
        """ Returns a human readable status string """
        return entry.get_type_as_string()    