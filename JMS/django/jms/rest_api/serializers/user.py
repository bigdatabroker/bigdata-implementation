from rest_framework import serializers
from jms.models import User

class UserSerializer(serializers.ModelSerializer):
    """
        The UserSerializer is a class that handles serialization and deserialization
        of user data. It is involved in every client communication that is done via the REST-API. The data format is JSON.
    """
    class Meta:
        model = User
        fields = ('user_id', 'name', 'priority', 'is_admin')
        read_only_fields = ('user_id',)