from rest_framework import serializers
from jms.index.models import *

class NodeSerializer(object):
	"""
		Class that determines which fields of a node in the index tree should be deliv- ered
	 	to the client via the REST-API. The NodeSerializer also handles serialization
	  	and deserialization of node data using the JSON format.
	"""

	def __init__(self, node, many = False):
		self._node = node
		self._many = many

	def __format_node(self, node):

		# TODO: to_json --> model?
		structured_data = {
			'path': node.path,
			'path_string': '/%s' % ('/'.join(node.path)),
			'hostname': node.hostname,
			'url': '%s:/%s' % (node.hostname, '/'.join(node.path)),
			'flags': node.flags,
			'name': node.name,
			'owner': {
				'user': node.owner_user,
				'group': node.owner_group
			},
			'permissions': oct(node.permissions),
			'type': node.type,
			'dates': {
				'created': node.created_on,
				'changed': node.changed_on,
				'meta_changed': node.meta_changed_on
			}
		}

		if isinstance(node, FileNode)	:	# You may think now: WTF instanceof? WHY THE HELL? But there's a quite simple reason for this. We MUST HAVE a strong separartion of the model and the view (this class). If a model changes, the API MAY NOT CHANGE its output. If we move the generation of the view into the model (which would be a better solution in termis of object prientation), we wouldn't have this seperation and it could happen more easily that changed to the model also change the api output.
			file_data = {
				'size': node.size,
				'hash': node.hash
			}

			structured_data.update(file_data)

		return structured_data

	@property
	def data(self):

		if self._many:
			all_nodes = []

			for node in self._node:
				all_nodes.append(self.__format_node(node))

			return all_nodes

		else:
			return self.__format_node(self._node)
