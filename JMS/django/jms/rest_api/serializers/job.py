from rest_framework import serializers
from jms.models import Job

class JobSerializer(serializers.ModelSerializer):
    """ Serializes job objects. 

    The client also gets a list of events the job already had.
    Handles serialization and deserialization of job information. 
    This Serializer is used whenever job data has to be sent or received via
    the REST-API. The JSON format is used for any kind of communication.

    Author: Simon Hessner, Benno Ommerborn
    """
    status = serializers.SerializerMethodField('get_status_as_string')
    dates = serializers.SerializerMethodField('get_events')

    class Meta:
        model = Job
        fields = ('id', 'priority', 'name', 'user', 'status', 'dates')
        read_only_fields = ('id', 'user')

    def get_status_as_string(self, job):
        """ Returns a human readable status string """
        return job.get_status_as_string()

    def get_events(self, job):
        """ returns a list of events. the types are strings, no ints. """
        events = job.event_set.all()
        return [{'event': x.get_type_as_string(), 'time' : x.time} for x in events]