from rest_framework import serializers
from jms.models import Server

class ServerSerializer(serializers.ModelSerializer):
    """
        Handles serialization and deserialization of server information.
        This is required for the client communication via REST-API which uses the JSON format.

        Author: Simon Hessner, Benno Ommerborn
    """
    class Meta:
        model = Server
        fields = ('id','capacity', 'last_activity', 'hostname', 'is_active')
        read_only_fields = ('id', 'last_activity', 'hostname', 'is_active')