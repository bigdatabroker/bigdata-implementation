from rest_framework import serializers
from jms.models import AccessToken

class AccessTokenSerializer(serializers.ModelSerializer):
    """
        Handles serialization and deserialization of auth data
        that is sent via the REST- API. AuthSerializer uses the JSON
        format for the communication with the clients.
    """
    class Meta:
        model = AccessToken
        fields = ('user', 'token', 'expires_on')
        read_only_fields = ('user', 'token')