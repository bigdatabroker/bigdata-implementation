from rest_framework import serializers
from jms.models import Operation

class OperationPUTSerializer(serializers.ModelSerializer):
    """ Serializes and deserializes operation objects.

    This serializer is only used for PUT requests because there are some fields in the normal serializer
    that are read only (target, source, type, ...) but they have to be 
    set on PUT.

    Author: Simon Hessner, Benno Ommerborn
    """
    status = serializers.SerializerMethodField('get_status_as_string')
    type = serializers.SerializerMethodField('get_type_as_string')

    class Meta:
        model = Operation

        fields = ('id', 'type', 'status', 'job', 'source', 'target', 'start', 'end')
        read_only_fields = ('id', 'job', 'start', 'end')

    def get_status_as_string(self, operation):
        """ Returns the operation status as human readable string. """
        return operation.get_status_as_string()

    def get_type_as_string(self, operation):
        """ Returns the operation type as human readable string. """
        return operation.get_type_as_string()


class OperationSerializer(serializers.ModelSerializer):
    """ Serializes and deserializes operation objects.

    See description of OperationPUTSerializer for the reason why there
    are two serializers for operations.

    Author: Simon Hessner, Benno Ommerborn
    """
    status = serializers.SerializerMethodField('get_status_as_string')
    type = serializers.SerializerMethodField('get_type_as_string')

    class Meta:
        model = Operation
        fields = ('id', 'type', 'status', 'job', 'source', 'target', 'start', 'end')
        read_only_fields = ('id', 'job', 'source', 'target', 'start', 'end')

    def get_status_as_string(self, operation):
        """ Returns the operation status as human readable string. """
        return operation.get_status_as_string()    

    def get_type_as_string(self, operation):
        """ Returns the operation type as human readable string. """
        return operation.get_type_as_string()