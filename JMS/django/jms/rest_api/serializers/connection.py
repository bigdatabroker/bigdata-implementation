from rest_framework import serializers
from jms.models import Connection

class ConnectionPUTSerializer(serializers.ModelSerializer):
    """Serializes and deserializes connection objects.

        This serializer is only used for PUT requests because there are some fields in the normal serializer
        that are read only but they have to be set on PUT.

        Author: Simon Hessner, Benno Ommerborn
    """
    class Meta:
        model = Connection
        fields = ('source', 'target', 'protocol', 'speed')
        read_only_fields = ('source',)

class ConnectionSerializer(serializers.ModelSerializer):
    """
        Class that determines which fields of a node in the index tree should
        be delivered to the client via the REST-API. The NodeSerializer also
        handles serialization and deserialization of node data using the JSON format.
    """
    class Meta:
        model = Connection
        fields = ('source', 'target', 'protocol', 'speed')
        read_only_fields = ('source', 'target', 'protocol')