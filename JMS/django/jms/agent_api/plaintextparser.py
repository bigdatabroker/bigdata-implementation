from rest_framework.parsers import BaseParser

class PlainTextParser(BaseParser):
    """ This Parser simply returns the HTTP body of a request.

    Author: Simon Hessner pasted it from http://django-rest-framework.org/api-guide/parsers.html
    """

    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Simply return a string representing the body of the request.
        """                
        return stream