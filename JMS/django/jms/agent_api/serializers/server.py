from rest_framework import serializers
from jms.models import Server

class ServerSerializer(serializers.ModelSerializer):
    """ Handles serialization of storage server information
    and decides which fields to read from the client or which fields
    to return to the client.
    
    Author: Simon Hessner
    """
    class Meta:
        model = Server
        fields = ('id', 'hostname', 'capacity', 'token', 'is_active')
        read_only_fields = ('id', 'token', 'is_active')