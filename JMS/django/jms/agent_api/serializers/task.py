from rest_framework import serializers
from jms.models import Job, Operation

class TaskSerializer(serializers.ModelSerializer):
    """ Handles serialization of tasks (operations).
    
    Author: Simon Hessner
    """

    type_string = serializers.SerializerMethodField('get_type_as_string')    

    class Meta:
        model = Operation
        fields = ('id', 'source', 'target', 'op_type', 'type_string', 'job')        

    def get_type_as_string(self, operation):
        """ Returns the operation time as human readable string """
        return operation.get_type_as_string()