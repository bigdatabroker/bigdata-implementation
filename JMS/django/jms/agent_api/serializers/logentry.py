from rest_framework import serializers
from jms.models import LogEntry

class LogEntrySerializer(serializers.ModelSerializer):
    """ Handles serialization of log entries and decides
    which fields to read from the client or which fields
    to return to the client.
    
    Author: Simon Hessner
    """
    class Meta:
        model = LogEntry
        fields = ('message', 'is_error', 'foreign_id', 'entry_type') #fields to be extracted from the http body
        read_only_fields = ('foreign_id', 'entry_type')
