from rest_framework import status
from rest_framework.response import Response
from jms.agent_api.serializers.logentry import LogEntrySerializer
from jms.models import LogEntry as LogEntryModel
from jms.models import Job
from jms.models import Operation
from jms.exception.api import BadRequestHttp400
from jms.agent_api.views.base import BaseView
from rest_framework.exceptions import PermissionDenied

class LogEntry(BaseView):
    """ Handles HTTP-PUT requests of an agent running on a storage server to log a message.

    A log entry can be assigned to a job, an operation or a server.

    Author: Simon Hessner
    """
    def put(self, request, format=None):
        """ Creates new Job object and stores it. """
        self.check_ssl(request)
        token = self._get_token(request.META)
        server = self._get_server(token)

        if not server.is_active:
            raise PermissionDenied("Server has to be activated before it can log events.")

        if not request.DATA.has_key("type"):
            raise BadRequestHttp400("No entry type specified.")
        entry_type = request.DATA["type"]

        if not entry_type in LogEntryModel.get_allowed_types():
            raise BadRequestHttp400("Invalid entry type.")

        foreign_object = server #default 
        if entry_type == "job":
            try:
                foreign_object = Job.objects.get(pk = self._get_foreign_id(request))
            except Job.DoesNotExist:
                raise BadRequestHttp400("Job does not exist.")

        if entry_type == "operation":
            try:
                foreign_object = Operation.objects.get(pk = self._get_foreign_id(request))
            except Operation.DoesNotExist:
                raise BadRequestHttp400("Operation does not exist.")

        serializer = LogEntrySerializer(data=request.DATA)

        if serializer.is_valid(): 
            #inject subject on a rude way. django rest framework does not
            #provide a clean method for this.
            serializer.object.foreign_obj = foreign_object
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED) 
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

    def _get_foreign_id(self, request):
        """ extracts the foreign id from the request and returns it

        Args:
            request: Request object from django rest framework

        Returns:
            Foreign id
        """
        if not request.DATA.has_key("foreign_id"):
            raise BadRequestHttp400("foreign_id required.")
        return request.DATA["foreign_id"]