from rest_framework import status as rf_status
from rest_framework.response import Response
from jms.models import Operation, Event
from datetime import datetime
from jms.exception.api import BadRequestHttp400, Forbidden403
from jms.agent_api.views.base import BaseView
from jms.utils import get_proper_date

class Status(BaseView):
    """ Handles status updates of the storage server via POST.

    Author: Simon Hesssner
    """
    def post(self, request, format=None):
        """ Updates status of Operation """

        self.check_ssl(request)

        token = self._get_token(request.META)
        server = self._get_server(token)
        server.last_activity = get_proper_date(datetime.now())
        server.save()

        if not request.DATA:
            raise BadRequestHttp400("no body found")

        if not request.DATA.has_key("status") and not request.DATA.has_key("operation"):
            raise BadRequestHttp400("Operation and status needed!")

        if  request.DATA.has_key("status"):
            if not server.is_active:
                raise Forbidden403("Server has to be activated before it updates its state.")

            status = str(request.DATA["status"])

            if request.DATA.has_key("operation"):
                try:
                    operation = int(request.DATA["operation"])
                except ValueError:
                    raise BadRequestHttp400("Operation must be int")
                self._update_operation(operation, status, server)
            else:
                raise BadRequestHttp400("Bad Request: Status given, but no operation.")

        elif request.DATA.has_key("operation"):
            # status should not be updated, but job_id or operation_id given
            raise BadRequestHttp400("Bad request: Operation given, but no status")

        return Response(None, status=rf_status.HTTP_200_OK)

    def _update_operation(self, operation_id, status, server):
        """ Updates the status of an operation and sets the start and end time. """
        try:
            operation = Operation.objects.get(pk=operation_id)
        except Operation.DoesNotExist:
            raise BadRequestHttp400("Operation does not exist")

        if not operation.status_is_allowed(status):
            raise BadRequestHttp400("Invalid status provided.")

        # agent is only allowed to change status of operations it is
        # responsible for.
        if not operation.source.startswith("%s:" %server.hostname):
            raise BadRequestHttp400("Operation cannot be executed by this agent.")

        if status == "RUNNING":
            #If a storage server starts the operation later than it got the task
            operation.start = get_proper_date(datetime.now())
            operation.end = None
        if status in ["ERROR", "SUCCESS"]:
            self._update_job(operation.job, operation, status)
            operation.end = get_proper_date(datetime.now())
            if operation.start is None:
                raise BadRequestHttp400("Operation cannot be ended before it started.")
        if status == "WAITING": #will probably never occur.
            operation.start = None
            operation.end = None

        operation.set_status_by_string(status)
        operation.save()

    def _update_job(self, job, operation, status):
        """ If the last operation of a job finishes (error or success)
        then the job is marked es arror or success.
        furthermore there is a event object for the job created.
        if it was the last job that used a certain token, this token gets deleted.
        """
        create_event = False
        if status == "ERROR":
            job.set_status_by_string("ERROR")
            create_event = True

        unfinished = Operation.objects.filter(job=job, status__in =[Operation.WAITING, Operation.RUNNING])
        if len(unfinished) == 1 and unfinished[0] == operation:
            if job.get_status_as_string() != "ERROR":
                job.set_status_by_string(status)
                create_event = True
                job.save()
        
            # Delete token if this was the last job using it
            if job.token:
                delete = True                
                for _job in job.token.job_set.all():                    
                    if not _job.is_finished():                                          
                        delete = False

                if delete:                                       
                    job.token.delete()     
                    job.token = None #set_Null does not work in django....       

        if create_event and not Event.get_event(job, "END"):
            e = Event(job=job)
            e.set_type_by_string("END")                    
            e.save()

        job.save()