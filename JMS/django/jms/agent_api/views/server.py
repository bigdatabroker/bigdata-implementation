from rest_framework import status
from rest_framework.response import Response
from jms.agent_api.views.base import BaseView
from jms.agent_api.serializers.server import ServerSerializer
from jms.exception.api import BadRequestHttp400
from jms.logger.logger import Logger
from jms.exception.model import ModelError

class Server(BaseView):
    """ Handles the storage server registration via HTTP-PUT.

    Author: Simon Hessner
    """
    def put(self, request, format=None):
        """ Creates a new Server object, assigns an unique access token to
        it and returns the token via JSON.
        """
        self.check_ssl(request)

        if request.META.has_key("HTTP_X_BDB_AGENT_TOKEN"):
            raise BadRequestHttp400("Do not provide a token when requesting a token.")

        serializer = ServerSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.object.generate_token()
            try:
                serializer.save()
            except ModelError:
                raise BadRequestHttp400("Capacity must be positive.")


            Logger.get_logger().log("registered successfully", False, serializer.object)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
