from rest_framework.views import APIView
from jms.models import Server
from jms.exception.api import Forbidden403
from django.conf import settings

class BaseView(APIView):
    """Helper class that defines methods used by many views

    Author: Simon Hessner
    """

    def check_ssl(self, request):
        if settings.REQUIRE_SSL and not request.is_secure():
            raise Forbidden403('No SSL connection')

    def _get_token(self, data):
        """ Extracts token from the request and raises an exception if no token is present 

        Args:
            data: meta data from request

        Returns:
            Token
        """
        if not data.get("HTTP_X_BDB_AGENT_TOKEN"):
            raise Forbidden403("please authenticate with an access token!")
        return data.get("HTTP_X_BDB_AGENT_TOKEN")

    def _get_server(self, token):
        """ Returns the server associated with the token. 
        Denies Access if the token was not found.

        Args:
            token: Token String

        Returns:
            Server object
        """
        try:
            return Server.objects.get(token=token)
        except Server.DoesNotExist:
            raise Forbidden403("Unknown access token!")