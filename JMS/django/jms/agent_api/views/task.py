from rest_framework import status
from rest_framework.response import Response
from jms.agent_api.views.base import BaseView
from jms.models import Operation, Event
from jms.agent_api.serializers.task import TaskSerializer
from datetime import datetime
from jms.scheduler.scheduler import *
from rest_framework.exceptions import PermissionDenied
from jms.utils import get_proper_date

class Task(BaseView):
    """ Returns the next task for an agent (storage server).

    Uses scheduling internally.

    Author: Simon Hessner
    """
    def get(self, request, format=None):
        """ Asks the scheduler what to do next and returns the task to the
        agent. """

        self.check_ssl(request)

        token = self._get_token(request.META)
        server = self._get_server(token)
        if not server.is_active:
            raise PermissionDenied("Server has to be activated before get tasks.")

        scheduler = Scheduler.get_scheduler()       
        
        if not scheduler.has_next_operation(server):
            return Response({"detail" : "nothing to do"}, status=status.HTTP_204_NO_CONTENT)

        operation = scheduler.get_next_operation(server)
        protocol = operation["protocol"]
        operation = operation["operation"]

        # updates the job if neccessary
        self._update_job(operation.job, operation)

        # will be set to SUCCESS or ERROR by status update
        operation.set_status_by_string("RUNNING")
        operation.start = get_proper_date(datetime.now())
        operation.end = None
        operation.save()
        
        serializer = TaskSerializer(operation) 
        ret = serializer.data
        ret["protocol"] = protocol

        job = operation.job
        if not job.token or job.token.is_expired():
            # There is the possibility that the job can be executed
            # even without a token. The agent has to decide!
            # If the agent needs a token it has to log an error
            ret["access_data"] = None
        else:
            ret["access_data"] = job.token.data
        return Response(ret, status=status.HTTP_200_OK)

    def _update_job(self, job, status):
        """ If the operation that is returned is the first of the job
        then the status of the job is changed to RUNNING
        and a job event (START) is created.
        """
        finished = Operation.objects.filter(job=job, status__in =[Operation.SUCCESS, Operation.ERROR])
        if len(finished) == 0 and job.get_status_as_string() != "RUNNING":
            # First operation of job
            job.set_status_by_string("RUNNING")
            job.save()

            if not Event.get_event(job, "START"):
                e = Event(job=job)
                e.set_type_by_string("START")                
                e.save()