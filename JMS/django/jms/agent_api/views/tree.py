from rest_framework.response import Response
from jms.agent_api.views.base import BaseView
from jms.exception.api import BadRequestHttp400
from jms.utils import TokenGenerator
from datetime import datetime
from jms.agent_api.plaintextparser import PlainTextParser
from django.conf import settings
from jms.utils import StatParser
from jms.logger.logger import Logger
from rest_framework import status
from jms.worker.index import IndexUpdater

from jms.index.abstract import IndexAccessor

import os

class Tree(BaseView):
    """ Takes a new subtree from the agent in the stat format and
    stores it for later execution in the cache dir.

    The user gets directly informed if the format of the stat file is
    invalid.

    Author: Simon Hessner
    """
    parser_classes = (PlainTextParser,) #tree comes in plain text!

    def __init__(self):
        """ Creates a new Tree object and initializes the stat parser. """
        super(Tree, self).__init__()
        self.parser = StatParser()
        self.server = None

    def post(self, request, format=None):
        """ Extracts the stat file from the HTTP body, parses it and
        checks if it is valid. Stores it on the hard disk to be
        available to the background workers.
        Creates a background job that will update the index later."""

        self.check_ssl(request)

        token = self._get_token(request.META)
        self.server = self._get_server(token)

        tree = request.DATA #whole body is tree

        if not tree:
            raise BadRequestHttp400("No tree given")

        try:
            filename = self._get_filename(self.server.hostname)

            with open(filename, 'w') as f:
                if not self._check_tree_format(tree, f):
                    os.remove(filename)
                    raise BadRequestHttp400("Malformed tree.")

        except BadRequestHttp400 as e:
            try:
                os.remove(filename)
            except:
                pass

            raise e
        except IOError:
            Logger.get_logger().log("Error writing stat file to %s" % filename, True, self.server)
            return Response({"detail": "problems writing stat file to the cache."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        worker = IndexUpdater(path = filename, server_id = self.server.id)
        worker.execute()

        return Response(None, status=status.HTTP_202_ACCEPTED)

    def _get_filename(self, agent):
        """ Creates a unique filename for the stat file and returns it. """
        destination =  settings.TREE_BASE_PATH
        token = TokenGenerator(8).get_token()
        time = str(datetime.now())
        return "%s/%s__%s__%s.stat" % (destination, str(agent), token, time)

    def _check_tree_format(self, tree, f):
        """ Checks if the sent tree is valid. """

        ia = IndexAccessor.get_accessor()
        ia.enter_global_mode()

        linenumber = 0
        line = tree.readline().decode("utf8").rstrip()
        while line:
            if not line or line == "":
                continue #skip empty line

            if not self.parser.parse_line(line):
                #the first line must not be a valid stat element
                #it may contain the root path
                if linenumber != 0:
                    return False
                else:
                    root = "%s:%s" % (self.server.hostname, line)

                    if not ia.has_node(root):
                        raise BadRequestHttp400("invalid root path")

            f.write(line.encode('utf8'))
            f.write("\n")

            linenumber += 1
            line = tree.readline().decode("utf8")
        return True
