from django.conf.urls import patterns, url
from jms.agent_api.views.status import Status
from jms.agent_api.views.logentry import LogEntry
from jms.agent_api.views.server import Server
from jms.agent_api.views.task import Task
from jms.agent_api.views.tree import Tree

urlpatterns = patterns('jms.agent_api.views',
   url(r'^status/$', Status.as_view()),
   url(r'^logentry/$', LogEntry.as_view()),
   url(r'^server/$', Server.as_view()),
   url(r'^task/$', Task.as_view()),
   url(r'^tree/$', Tree.as_view()),
)