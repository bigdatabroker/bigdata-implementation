from jms.models import *

from django.conf import settings
from jms.utils import import_class

class Logger(object):
    """ Singleton base class for different loggers (e.g. DatabaseLogger)

    Provides some methods to log messages related to different objects:
    - None (global messages)
    - Job
    - Operation
    - User
    - Server

    Author: Simon Hessner
    """
    _instance = None
    # for singleton
    def __new__(class_, *args, **kwargs):
        """ realizes singleton """
        if not isinstance(class_._instance, class_):
            class_._instance = object.__new__(class_, *args, **kwargs)
        return class_._instance

    def __init__(self):
        """ Logger is abstract! """
        raise NotImplementedError("Logger is abstract")

    @staticmethod
    def get_logger():
        """ Returns an instance of the JMS_LOGGER_CLASS class. """
        LoggerClass = import_class(settings.JMS_LOGGER_CLASS)
        return LoggerClass()

    def log(self, message, err, obj = None):
        """ Creates a LogEntry with the specified data.
        Dispatches the log call to the appropriate handler functions. 

        Args:
            message: Message to be logged.
            err: Boolean that indicates if the message is a error message.
            obj: Object that will be associated with the message.

        Raises:
            TypeError: obj has a unsupported type
        """
        if obj is None:
            return self._log_global(message, err)

        if isinstance(obj, Job):
            return self._log_job(message, err, obj)

        if isinstance(obj, Operation):
            return self._log_operation(message, err, obj)

        if isinstance(obj, User):
            return self._log_user(message, err, obj)

        if isinstance(obj, Server):
            return self._log_server(message, err, obj)

        raise TypeError("given object cannot be logged")

    def _log_global(self, message, error):
        """ abstract. """
        raise NotImplementedError("Logger is abstract")

    def _log_job(self, message, error, job):
        """ abstract. """
        raise NotImplementedError("Logger is abstract")

    def _log_operation(self, message, error, operation):
        """ abstract. """
        raise NotImplementedError("Logger is abstract")

    def _log_user(self, message, error, user):
        """ abstract. """
        raise NotImplementedError("Logger is abstract")

    def _log_server(self, message, error, server):
        """ abstract. """
        raise NotImplementedError("Logger is abstract")