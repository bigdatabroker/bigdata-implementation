from jms.logger.logger import Logger
from jms.models import LogEntry

class DatabaseLogger(Logger):
    """ A logger that uses the djangp-based log models to log events. 

    There are different type of "foreign objects" that can be related
    to the created log entry: Job, Operation, User, Server or simply 
    None for global messages.

    Author: Simon Hessner
    """
    def __init__(self):
        """ Initializes the DatabaseLogger """
        pass #overwritten because parent is abstract

    def __create_entry(self, message, error, obj = None):
        """ Helper method that creates the log entry and returns it
        without saving. 

        Args:
            message: Message to be logged.
            err: Boolean that indicates if the message is a error message.
            obj: Object that will be associated with the message.
        """
        entry = LogEntry()
        entry.is_error = bool(error)
        entry.message = str(message)
        entry.foreign_obj = obj
        return entry

    def _log_global(self, message, error):
        """ Create a log entry with no reference (global message). """
        entry = self.__create_entry(message, error)
        entry.save()
        return entry

    def _log_job(self, message, error, job):
        """ Create a log entry with a reference to a job. """
        entry = self.__create_entry(message, error, job)
        entry.save()
        return entry

    def _log_operation(self, message, error, operation):
        """ Create a log entry with a reference to an operation. """
        entry = self.__create_entry(message, error, operation)
        entry.save()
        return entry

    def _log_user(self, message, error, user):
        """ Create a log entry with a reference to an user. """
        entry = self.__create_entry(message, error, user)
        entry.save()
        return entry

    def _log_server(self, message, error, server):
        """ Create a log entry with a reference to a server. """
        entry = self.__create_entry(message, error, server)
        entry.save()
        return entry