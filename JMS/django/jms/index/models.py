from jms.index.utils import *

class Node(object):
    def __init__(self):
        self.name = ''
        self.path = []
        self.hostname = ''
        self.owner_user = -1
        self.owner_group = -1
        self.permissions = 0
        self.created_on = None
        self.changed_on = None
        self.meta_changed_on = None
        self.flags = []

    # TODO by Nomis: __unicode__ instead of __str__?
    def __str__(self):
        return "Unspecified Node %s (%s)" % self.name, self.path

    @property
    def can_have_children(self):
        """ Indicates if the represented node type can hace child elements.

        Returns:
            True if the node can have children, else False
        """
        return False

    def is_accessible_by(self, token, type = 'element'):
        """ Checks if the represented node can be accessed by the specified token,
        assumed that the user can access all parent nodes.

        Args:
            token: AccessToken object with that the permissions are checked
            type: type of the access to check. Supported values:
                element: checks access for the element itself
                contents: checks access for the contents of the element

        Returns:
            True if the user with the specified token can access the element with the specified access type,
            else False
        """
        return False

    @property
    def url(self):
        """ Returns the full URL of the node with which the node can be adressed uniquely
        across the whole big data broker software.

        Returns:
            String representing full path to the node
        """
        return "%s:/%s" % (self.hostname, '/'.join(self.path))

    @property
    def is_mutable(self):
        """ Checks if the represented element if "mutable".
        If an element is mutable, the element can be renamed, deleted, copied or moved.
        Mutable elements can be moved to elements that are not mutable, but unmutable cannot be moved
        to mutable elements.

        Returns:
            True if the represented element is mutable by above definition, else False.
        """
        return False

    @property
    def type(self):
        """ Returns a string which classified the type of the represented element.

        Returns:
            String representing the node type.
        """
        return 'FUCKING_UNKNOWN'

class ServerNode(Node):
    def __init__(self):
        self.hash = ""
        self.size = 0

        super(ServerNode, self).__init__()

    def is_accessible_by(self, token, type = 'element'):
        return True

    @property
    def can_have_children(self):
        return True

    def __str__(self):
        return "Server %s (%s)" % (self.name, self.path)

    @property
    def type(self):
        return 'SERVER'

    @property
    def is_mutable(self):
        return False

class FileNode(Node):
    def __init__(self):
        self.hash = ""
        self.size = 0

        super(FileNode, self).__init__()

    def is_accessible_by(self, token, type = 'element'):
        is_owner = (self.owner_user == token.user.user_id)
        is_in_group = (self.owner_group in token.user.group_ids)

        perms = parse_perms(self.permissions)

        if is_owner:
            return 'r' in perms['user']
        elif is_in_group:
            return 'r' in perms['group']
        else:
            return 'r' in perms['other']

    @property
    def can_have_children(self):
        return False

    def __str__(self):
        return "File %s (%s)" % (self.name, self.path)

    @property
    def type(self):
        return 'FILE'

    @property
    def is_mutable(self):
        return True

class DirectoryNode(Node):

    def __init__(self):
        super(DirectoryNode, self).__init__()


    def __str__(self):
        return "Directory %s (%s)" % (self.name, self.path)

    @property
    def can_have_children(self):
        return True

    def is_accessible_by(self, token, type = 'element'):
        is_owner = (self.owner_user == token.user.user_id)
        is_in_group = (self.owner_group in token.user.group_ids)

        perms = parse_perms(self.permissions)

        needed_flags = ['x']
        if type == 'contents':
            needed_flags.append('r')

        if is_owner:
            for flag in needed_flags:
                if not flag in perms['user']:
                    return False
            return True

        elif is_in_group:
            for flag in needed_flags:
                if not flag in perms['group']:
                    return False
            return True

        else:
            for flag in needed_flags:
                if not flag in perms['other']:
                    return False
            return True

    @property
    def type(self):
        return 'DIRECTORY'

    @property
    def url(self):
        return "%s:/%s/" % (self.hostname, '/'.join(self.path))

    @property
    def is_mutable(self):
        return True
