import psycopg2

from interface import IndexAccessor
from models import *
from psycopg2.extensions import adapt
from psycopg2.extras import RealDictCursor
from collections import deque

from django.conf import settings

from jms.exception.index import *
from jms.index.utils import *
from jms.utils import *

from jms.worker.abstract import WorkerJob

class PostgresIndexAccessor(IndexAccessor):
    """ PostgresIndexAccessor inherits from abstract IndexAccessor and implements an
    index accessor which can handle postgreSQL databases.
    """

    __ROOT_NODE_PARENT_ID = 0

    def __init__(self, token = None):
        self.db = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (settings.INDEX_DATABASE['NAME'], settings.INDEX_DATABASE['USER'], settings.INDEX_DATABASE['HOST'], settings.INDEX_DATABASE['PASSWORD']))
        self.token = token
        self._global_mode = False

    def __full_path_array_from_node_object(self, node):
        arr = [node.hostname]
        arr.extend(node.path)

        return arr

    def _process_flags(self, raw_flags, inheriting_flags):
        """ Processes the flag for a specified node with specified inheriting flags of the parent node.

        Args:
            raw_flags: Flags of the node as read from the database (including prefixes)
            inheriting_flags: Flags of the parent node that should be inherited to the current node.

        Returns:
            Dictionary containing three arrays which contain all flags that apply to the specified node.
                the inheriting key contains all flags that should be applied to child nodes of the specified node,
                local contains the flags that were applied to the specified node but should not be inherited and
                combined contains all flags that are applied to the node.
        """
        local_flags = []

        for raw_flag in raw_flags:
            flag_type = raw_flag[0]
            flag = raw_flag[1:]

            if flag_type == '+':
                inheriting_flags.append(flag)
            elif flag_type == '-':
                inheriting_flags.remove(flag)
            elif  flag_type == '~':
                local_flags.append(flag)

        return {
            'inheriting': inheriting_flags,
            'local': local_flags,
            'combined': (local_flags + inheriting_flags)
        }

    def _resolve_path(self, path, raw = False):
        """ Resolves the specified path from the database and checks the permissions of the elements with the
        specified access token.

        Args:
            path: Path that should be resolved; either as string or as array
            raw: if True, an array containing dictionaries as queried from the database will be returned, else
                an array of node objects will be retuned

        Throws:
            InvalidURLSyntaxException: if the syntac of the specified url is invalid
            InvalidPathException: if the path doesen't exist
            NoPermissionsException: if the user doesen't have permissions to view the element at the specified path

        Returns:
            None if the specified path could not be parsed or was invalid,
                None if the path doesen't exist,
                False if the user cannot access the path
                or the nodes of the path in the format specified by the raw argument
        """

        if isinstance(path, basestring):
            parts = parse_url(path)
        else:
            parts = path

        # get all nodes of the path
        cursor = self.db.cursor(cursor_factory = RealDictCursor)
        cursor.execute("SELECT *, flags AS flags_raw, get_path(tree_node_id) AS path FROM resolve_path(%s)", (parts, ))
        rows = cursor.fetchall()

        if len(rows) <= 0:
            raise InvalidPathException("not found: %s" %path)

        # process flags
        current_flags = []
        for row in rows:
            processed_flags = self._process_flags(row['flags_raw'], current_flags)
            row['flags'] = processed_flags['combined']


        nodes = self._convert_raw_to_objects(rows)

        # Check permissions
        if not self._global_mode:
            for node in nodes:
                if not node.is_accessible_by(self.token):
                    raise NoPermissionsException()
        if raw:
            return rows
        else:
            return nodes

    def _convert_raw_to_objects(self, rows):
        """ Converts an array with dictionary that were returned from a database query
                into an array of appropriate node objects.

        Args:
            rows: rows returned from the database

        Returns:
            Array containing all node objects.
        """
        nodes = []

        for row in rows:
            nodes.append(self._convert_raw_to_object(row))

        return nodes

    def _convert_raw_to_object(self, row):
        if row['type'] == 'DIRECTORY':
            node = DirectoryNode()
        elif row['type'] == 'SERVER':
            node = ServerNode()
        else:
            node = FileNode()

        node.name = row['name']
        node.path = row['path'][1:]
        node.hostname = row['path'][0]
        node.owner_user = row['owner_user']
        node.owner_group = row['owner_group']
        node.permissions = row['permissions']
        node.flags = row['flags']

        node.created_on = row['created_on']
        node.changed_on = row['changed_on']
        node.meta_changed_on = row['meta_changed_on']

        # monkey patching for performance increasement; postgres index accessor internally and fallback compatible
        setattr(node, '_PostgresIndexAccessor_tree_node_id', row['tree_node_id'])
        setattr(node, '_PostgresIndexAccessor_parent_id', row['parent_id'])

        return node

    # TODO comment!
    def enter_global_mode(self):
        self._global_mode = True

    # TODO comment!
    def get_node(self, path):
        nodes = self._resolve_path(path)
        return nodes[-1]

    # TODO comment!
    def get_children(self, node, levels = 1):

        # node can't have children, e.g. because its a file
        if not node.can_have_children:
            return []

        # user is not allowed to list the contents of the specified directory
        if not self._global_mode and not node.is_accessible_by(self.token, 'contents'):
            raise NoPermissionsException()

        full_path = self.__full_path_array_from_node_object(node)

        raw_nodes = self._resolve_path(full_path, True)

        # Get the root node of the list
        list_root_node = raw_nodes[-1]
        list_root_node_id = list_root_node['parent_id']     #TODO: use tree_node_id instead of parent_id?!

        cursor = self.db.cursor(cursor_factory = RealDictCursor)
        cursor.execute("""
            WITH RECURSIVE nodes_cte(id, label, parent_id, depth, path) AS (
                SELECT tn.tree_node_id, tn.name, tn.parent_id, 1::INT AS depth, tn.tree_node_id::TEXT AS path
                FROM tree_node AS tn
                WHERE tn.parent_id = %s
            UNION ALL
                SELECT c.tree_node_id, c.name, c.parent_id, p.depth + 1 AS depth, (p.path || '->' || c.tree_node_id::TEXT)
                FROM nodes_cte AS p, tree_node AS c
                WHERE c.parent_id = p.id AND depth <= %s
            )
            SELECT tn.*, flags AS flags_raw, get_path(tn.tree_node_id) AS path
            FROM nodes_cte AS n
            INNER JOIN tree_node AS tn ON(tn.tree_node_id = n.id)
            WHERE depth > 1;
        """, (list_root_node_id, levels))

        children = cursor.fetchall()

        # ##############################################################################################################
        # ####################################   Extensive permission check and flag calculation   ###################################
        # ##############################################################################################################

        # build up a dictionary in the format parent ids -> elements in order to find all nodes with a specific parent id fast
        grouped_rows = {}
        for row in children:
            parent_id = row['parent_id']
            if not grouped_rows.has_key(parent_id):
                grouped_rows[parent_id] = []

            grouped_rows[parent_id].append(row)

        # calculate inherited flags of the list root node and build up a dictionary in the format id => inheriting flags of the node
        tmp_flags = []
        for row in raw_nodes:
            processed_flags = self._process_flags(row['flags_raw'], tmp_flags)
            tmp_flags = processed_flags['inheriting']

        inheriting_flags = { list_root_node['tree_node_id']: tmp_flags }

        # do a breadth first search to check which nodes are visible by the user start with the main node of which the children should be searched
        queue = deque([list_root_node['tree_node_id']])

        returned_rows = []
        while len(queue) > 0:
            current_node_id = queue.popleft()

            # node doesent't have children, so stop here (node itself will already be in the list unless its the root node)
            if not grouped_rows.has_key(current_node_id):
                continue

            for current_node in grouped_rows[current_node_id]:

                # calculate flags
                parent_inheriting_flags = list(inheriting_flags[current_node['parent_id']])
                processed_flags = self._process_flags(current_node['flags_raw'], parent_inheriting_flags)
                inheriting_flags[current_node['tree_node_id']] = processed_flags['inheriting']

                current_node['flags'] = processed_flags['combined']

                # check perms
                node_object = self._convert_raw_to_object(current_node)

                # if the user is allowed to view the element or if we are in global mode, add it to the return array
                if self._global_mode or node_object.is_accessible_by(self.token, 'element'):
                    returned_rows.append(node_object)

                    # if the node can have children and the user is allowed to view the children or global mode is enabled, put node onto the queue
                    if node_object.can_have_children and (self._global_mode or node_object.is_accessible_by(self.token, 'contents')):
                        queue.append(current_node['tree_node_id'])

        return returned_rows

    def has_node(self, path):
        try:
            self.get_node(path)
        except (InvalidURLSyntaxException, URLResolvementException) as e:
            return False

        return True

    def get_parent(self, node):
        path = self.__full_path_array_from_node_object(node)

        nodes = self._resolve_path(path)

        if len(nodes) <= 1:
            return None

        return nodes[-2]

    def add_flag(self, path_or_node, flag):

        if not self._global_mode:
            raise NoPermissionsException()

        if hasattr(path_or_node, 'path'):
            path = self.__full_path_array_from_node_object(path_or_node)
        else:
            path = path_or_node

        if not flag[0] in self.FLAG_PREFIXES:
            raise InvalidFlagException()

        raw_nodes = self._resolve_path(path, True)

        node = raw_nodes[-1]

        if flag in node['flags_raw']:
            return

        cursor = self.db.cursor()
        cursor.execute("UPDATE tree_node SET flags = flags || CAST(%s AS character varying) WHERE tree_node_id = %s", (flag, node['tree_node_id']))
        self.db.commit()

    def remove_flag(self, path_or_node, flag):

        if not self._global_mode:
            raise NoPermissionsException()

        if hasattr(path_or_node, 'path'):
            path = self.__full_path_array_from_node_object(path_or_node)
        else:
            path = path_or_node

        if not flag[0] in self.FLAG_PREFIXES:
            raise InvalidFlagException()

        raw_nodes = self._resolve_path(path, True)

        node = raw_nodes[-1]

        db_flags = []

        for f in node['flags_raw']:
            if f == flag:
                continue
            db_flags.append(f)

        cursor = self.db.cursor()
        cursor.execute("UPDATE tree_node SET flags = %s WHERE tree_node_id = %s", (db_flags, node['tree_node_id']))
        self.db.commit()

    def _prepare_node_data(self, node_data):
        if not node_data.has_key('times'):
            node_data['times'] = {}
            node_data['times']['birth'] = None
            node_data['times']['last_modification'] = None
            node_data['times']['last_change'] = None

        return node_data

    def add_node(self, path, node_data):

        if not self._global_mode:
            raise NoPermissionsException()

        if path == self.ROOT_PATH:
            parent_id = self.__ROOT_NODE_PARENT_ID
        else:
            raw_nodes = self._resolve_path(path, True)
            parent_id = raw_nodes[-1]['tree_node_id']

        node_data = self._prepare_node_data(node_data)

        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO tree_node (parent_id, name, owner_user, owner_group, size, permissions, type, created_on, changed_on, meta_changed_on)
            VALUES
            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """, (
            parent_id,
            node_data['name'],
            int(node_data['owner_user']),
            int(node_data['owner_group']),
            int(node_data['size']),
            int(node_data['permissions_oct']),
            node_data['type'],
            node_data['times']['birth'],
            node_data['times']['last_modification'],
            node_data['times']['last_change']
        ))
        self.db.commit()

    def update_node(self, path, node_data):

        if not self._global_mode:
            raise NoPermissionsException()

        raw_nodes = self._resolve_path(path, True)
        node_id = raw_nodes[-1]['tree_node_id']

        node_data = self._prepare_node_data(node_data)

        cursor = self.db.cursor()
        cursor.execute("""
            UPDATE tree_node SET
                name = %s,
                owner_user = %s,
                owner_group = %s,
                size = %s,
                permissions = %s,
                type = %s,
                created_on = %s,
                changed_on = %s,
                meta_changed_on = %s
            WHERE tree_node_id = %s
        """, (
            node_data['name'],
            int(node_data['owner_user']),
            int(node_data['owner_group']),
            int(node_data['size']),
            int(node_data['permissions_oct']),
            node_data['type'],
            node_data['times']['birth'],
            node_data['times']['last_modification'],
            node_data['times']['last_change'],
            node_id
        ))
        self.db.commit()

    def delete_node(self, node):
        if not self._global_mode:
            raise NoPermissionsException()

        path = self.__full_path_array_from_node_object(node)

        raw_nodes = self._resolve_path(path, True)

        node = raw_nodes[-1]

        deleted_parent_tree_node_id = (-1 * node['tree_node_id'])

        cursor = self.db.cursor()
        cursor.execute("UPDATE tree_node SET parent_id = %s WHERE tree_node_id = %s", (deleted_parent_tree_node_id, node['tree_node_id']))
        self.db.commit()

        job = _TreeDeletionJob(tree_node_id=node['tree_node_id'])
        job.execute()

    def _delete_node(self, node_id):

        if not self._global_mode:
            raise NoPermissionsException()

        print('deleting tree node ' + str(node_id) + ' recursively')

        cursor = self.db.cursor()
        cursor.execute("""
            WITH RECURSIVE nodes_cte(id, parent_id) AS (
                SELECT tn.tree_node_id AS id, tn.parent_id
                FROM tree_node AS tn
                WHERE tn.tree_node_id = %s
            UNION ALL
                SELECT c.tree_node_id AS id, c.parent_id
                FROM nodes_cte AS p, tree_node AS c
                WHERE c.parent_id = p.id
            )
            DELETE FROM tree_node WHERE tree_node_id IN(SELECT id FROM nodes_cte)
        """, (int(node_id), ))
        self.db.commit()

class _TreeDeletionJob(WorkerJob):
    def __init__(self, *args,**kwargs):
        self.tree_node_id = kwargs['tree_node_id']

    def _perform(self):
        accessor = PostgresIndexAccessor() # Yes, we don't use Index.get_accessor, because the worker is an internal part of the postgres index accessor and not visible from the outside. Maybe other implementations dont need it.
        accessor.enter_global_mode()
        accessor._delete_node(self.tree_node_id) #yep, its okay to call protected method from within packages (see java e.g.)

    def _get_queue_name(self):
        return "index"

    def _get_timeout(self):
        return 3600
