from jms.index.utils import *
from jms.exception.index import *

# TODO comment!
def parse_perms(perms):
    user = []
    if (perms & 1 << 8):
        user.append('r')
    if (perms & 1 << 7):
        user.append('w')
    if (perms & 1 << 6):
        user.append('x')

    group = []
    if (perms & 1 << 5):
        group.append('r')
    if (perms & 1 << 4):
        group.append('w')
    if (perms & 1 << 3):
        group.append('x')

    other = []
    if (perms & 1 << 2):
        other.append('r')
    if (perms & 1 << 1):
        other.append('w')
    if (perms & 1 << 0):
        other.append('x')

    return {
        'user': user,
        'group': group,
        'other': other
    }


def parse_url(url):
    """ Parses a path/url into an array that can be used with the database

    Args:
        path: Path to parse as string

    Returns:
        Array containing all parts of the path. a:/b/c would e.g. parsed into [a, b, c] etc.
    """

    # TODO: Trailing / = DIR?!?!?!
    while '//' in url:
        url = url.replace('//', '/')

    big_parts = url.split(':', 1)

    if len(big_parts) != 2:
        raise InvalidURLSyntaxException("invalid syntax.")

    hostname = big_parts[0]

    if len(hostname) <= 0:
        raise InvalidURLSyntaxException("No hostname specified")

    if '/' in hostname:
        raise InvalidURLSyntaxException("hostname may not contain special character /")

    real_path_parts = filter(None, big_parts[1].strip('/').split('/'))

    # build path to look for in the database
    parts = [hostname]
    parts.extend(real_path_parts)

    return parts
