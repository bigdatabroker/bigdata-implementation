from django.conf import settings
from jms.utils import import_class

class IndexAccessor:

	""" List of allowed prefixes for raw flags.
	Prefixes:
		+: new flag, will be inherited
		~: new local flag, will not be inherited
		-: remove inheriting flag for the current node and childs
	"""
	FLAG_PREFIXES = ['-', '+', '~']

	""" Constant that can be used to identify the absolute root of tre tree.
	Children of the absolute root are server nodes.
	"""
	ROOT_PATH = (False, False, True, False, False)

	# TODO comment!
	@staticmethod
	def get_accessor(*args):
            AccessorClass = import_class(settings.JMS_INDEX_ACCESSOR_CLASS)
            accessor = AccessorClass(*args)

            return accessor

	def __init__(self, token = None):
		"""
		Constructor. Initializes the IndexAccessor.

		Args:
			token: AccessToken that is used to determine wether the user can see a node or not.

		Author: Julian Schuh
		"""
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def get_node(self, path):
	        """ Returns the node at the spcified path or None, if the node doesen't exist or if the user doesen't have the permissions
	        to access the node.

	        Args:
	            path: path of the node as string or array

	        Returns:
	            Node object containinhg node at the specified path.
	        """
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def get_children(self, node, depth = 1):
	        """ Returns an array containing all children of the node at the specified path with the ability to define
	        a depth for which the children should be retrieved.
	        Will return None, if the specified node cannot have children and will return False, if the user isn't allowed
	        to view the children.

	        Args:
	        	node: node object for which the children should be returned
	        	depth: number of levels of children that should be returned

	        Returns:
	            Array containing all node objects that are children of the specified node upto the specified depth.
	        """
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def has_node(self, path):
	        """ Verifies if the node with the specified path exists and is accessable by the user.

	        Returns:
	            True if the node exists and is accessable, else false
	        """
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def get_parent(self, node):
	        """ Retrieves the parent node of the specified node.

	        Returns:
	            Parent node object
	        """
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def add_flag(self, path_or_node, flag):
		""" Adds a flag to the specified node.

		Args:
			path_or_node: Node the flag should be added to. Can be either a node object or a path to a node.
			flag: Flag that should be added to the node. The flag must match the following format:
				[+-~]flagname
				+x adds a flag x that will be applied recursively to all child nodes of the node.
				-x neutralizes the flag x which was inherited by a parent node. children of the specified
					node will not have the flag x, too.
				~x the flag is only applied to the node locally and will not be inherited and not neneutralized by
					inheriting nodes.

				The flag will be written to the database only for the one node that was specified as argument, the
					flags of a node will as a consequency be calculated recursively when retrieving nodes from the database.
					That means that nodes will not keep their flags when moved or copied; flags will have to be applied
					manually if needed.

		"""
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def remove_flag(self, path_or_node, flag):
		""" Removed a flag from the specified node.

		Args:
			path_or_node: Node the flag should be added to. Can be either a node object or a path to a node.
			flag: Flag that should be removed from the node. Refer to add_flag for further information on the
				format of the flags.
		"""
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def enter_global_mode(self):
		""" Brings the Index Accessor into global mode. In global mode all permission checks are bypassed.
		For security global mode must be explicitely enabled by calling this method, even if no AccessToken
		was provided."""
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def add_node(self, path, node_data):
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def update_node(self, path, node_data):
		raise NotImplementedError("Class IndexAccessor is abstract.")

	def delete_node(self, node):
		raise NotImplementedError("Class IndexAccessor is abstract.")
