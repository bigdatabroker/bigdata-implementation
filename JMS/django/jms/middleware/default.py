# Eastereggs!

class ImperationMiddleware(object):

    def process_response(self, request, response):

        response['X-Imper']  = 'Fun'

        return response

class CenterOfTheWorkdMiddleware(object):

    def process_response(self, request, response):

        response['X-Laupheim']  = '__Kinder__fest'

        return response

class MoodMiddleware(object):

    def process_response(self, request, response):

        response['X-Laune']  = 'Schlecht'

        return response
