from jms.models import Operation
from jms.models import Job
from jms.models import User
from jms.exception.scheduler import SchedulerError
from django.conf import settings
from jms.utils import import_class

class MacroScheduler(object):
    """ Abstract base class for MacroSchedulers. 

    A macro scheduler is responsible for ordering the list of jobs
    that are ready to be executed (e.g. by priority)

    Author: Simon Hessner
    """
    def __init__(self):
        """ Abstract class! """
        raise NotImplementedError("MacroScheduler is abstract")

    def get_job_queue(self):
        """ Returns the ordered list of ready jobs. """
        raise NotImplementedError("MacroScheduler is abstract")


class MicroScheduler(object):
    """ Abstract base class for MicroSchedulers.

    A micro scheduler is responsible for deciding which operation
    should be executed next by a specific storage server. It also
    decides which protocols to use.

    Author: Simon Hessner
    """
    def __init__(self):
        """ Abstract class! """
        raise NotImplementedError("MicroScheduler is abstract")
    
    def set_jobs(self, ordered_jobs):
        """ Method to set the ordered list of jobs calculated by
        the Macroscheduler. """
        raise NotImplementedError("MicroScheduler is abstract")    

    def get_next_operation(self, server):
        """ Returns the next operation for a specific storage server. """
        raise NotImplementedError("MicroScheduler is abstract")


class Scheduler(object):
    """ Scheduling manager class.

    Holds a MicroScheduler and a MacroScheduler and executes them.
    
    Raises: 
        SchedulerError: Wrong scheduler object given.

    Author: Simon Hessner
    """
    def set_micro_scheduler(self, scheduler):
        """ Sets the MicroScheduler instance. 

        Args:
            scheduler: MicroScheduler instance

        Raises:
            SchedulerError: If scheduler has wrong type
        """
        if not isinstance(scheduler, MicroScheduler):
            raise SchedulerError("Please provide a MicroScheduler instance.")
        self._micro_scheduler = scheduler

    def set_macro_scheduler(self, scheduler):
        """ Sets the MacroScheduler instance. 

        Args:
            scheduler: MacroScheduler instance

        Raises:
            SchedulerError: If scheduler has wrong type
        """
        if not isinstance(scheduler, MacroScheduler):
            raise SchedulerError("Please provide a Macrocheduler instance.")
        self._macro_scheduler = scheduler

    def get_next_operation(self, server):
        """ Returns the next operation for a specific storage server.

        Args:
            server: Server that wants to have the next operation.

        Returns: 
            Dict containing operation and protocol.
        """
        if self._macro_scheduler is None or self._micro_scheduler is None:
            raise SchedulerError("Please provide both MicroScheduler and MacroScheduler before requesting the next operation.")
            
        ordered_jobs = self._macro_scheduler.get_job_queue()
        self._micro_scheduler.set_jobs(ordered_jobs)

        return self._micro_scheduler.get_next_operation(server)

    def has_next_operation(self, server):
        """ Determines if the given server has something to execute. 

        Args:
            server: Server that wants to have the next operation.

        Returns:
            True if there is work, else False.
        """
        return self.get_next_operation(server) is not None

    @staticmethod
    def get_scheduler():
        """ Returns a pre-configured scheduler instance that has
        already been injected with a MicroScheduler and a Macrocheduler.

        Which MicroScheduler and Macrocheduler are used can be specified
        in the config.py

        Returns:
            Scheduler instance.
        """
        scheduler = Scheduler()
        micro = import_class(settings.SCHEDULER_CLASSES['micro'])
        macro = import_class(settings.SCHEDULER_CLASSES['macro'])

        scheduler.set_micro_scheduler(micro())
        scheduler.set_macro_scheduler(macro())

        return scheduler


class SimpleMacroScheduler(MacroScheduler):
    """ A very simple implementation of a MacroScheduler.
    It orders all ready jobs by job_priority * user_priority.

    Author: Simon Hessner
    """
    def __init__(self):
        """ Initializes the SimpleMacroScheduler """
        pass #just make it non abstract

    def get_job_queue(self):   
        """ Returns a ordered list of jobs. They are ordered by
        job_priority * user_priority (descending) 

        Returns:
            Ordered list of jobs that want to be executed.
        """
        #the table name is needed for the calulation of job.user.priority * job.priority
        user_table = User._meta.db_table 
        job_table = Job._meta.db_table

        filtered = Job.objects.filter(status__in =[Job.READY, Job.RUNNING])
        joined = filtered.extra(tables=[user_table,]).extra(where=["%s.id = %s.user_id" % (user_table, job_table)]) # we do not want a cross join!
        calculated = joined.extra(select={"prio_product" : "(%s.priority+1) * (%s.priority+1)" % (user_table, job_table)})

        ret = calculated.order_by('-prio_product', 'id') #high product = high prio
        
        return ret

class SimpleMicroScheduler(MicroScheduler):
    """ A very simple implementation of a MicroScheduler. It returns the
    first operation that can be executed by the given server.

    Author: Simon Hessner
    """
    def __init__(self):
        """ Initializes the SimpleMicroScheduler. """
        self.jobs = []

    def set_jobs(self, jobs):
        """ This method sets the ordered job list.

        Args:
            jobs: Ordered job list by MacroScheduler
        """
        self.jobs = jobs

    def get_next_operation(self, server):
        """ Returns the first matching operation that can be executed
        by the given server.

        Args:
            server: Server that wants to work.

        Returns:
            Operation to be executed by server.
        """
        if len(self.jobs) == 0:
            return None

        # find first matching operation for the asking server!
        for job in self.jobs:                   
            prefix = server.hostname + ":"            
            operations = job.operation_set.filter(status = Operation.WAITING, source__startswith=prefix).order_by('id')
            if len(operations) > 0:
                # there is always the default protocol returned because
                # one month is too less time to implement the full project
                # AND intelligent scheduling algorithms.
                return {"operation" : operations[0], "protocol" : "__DEFAULT__"}

        return None          