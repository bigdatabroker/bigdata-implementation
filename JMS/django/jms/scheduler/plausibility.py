from django.conf import settings
from jms.utils import import_class
from jms.models import Operation
from jms.exception.plausibility import PlausibilityCheckerError
from jms.scheduler.operation import OperationPlausibilityChecker
from django.conf import settings
from jms.index.abstract import IndexAccessor

class PlausibilityCheckManager(object):
    """
        The PlausibilityCheckManager manages different PlausibilityChecker instances.

        Attributes:
            token: Access token
            job: Job to check
            checkers: Array of checkers.
            errors: Array of error string messages.

        Author: Benno Ommerborn
    """

    def __init__(self, token, job, checkers=settings.DEFAULT_PLAUSIBILITY_CHECKER_CLASSES):
        """ Constructor

            Args:
                token: Access token
                job: Job to check

        """
        self._token = token
        self._job = job
        self._plausibility_checkers = []
        self._errors = {}
        self._is_critical_errors = False

        for checker in checkers:
            checker_class = import_class(checker)
            self.register_plausibility_checkers([checker_class])

    def register_plausibility_checkers(self, checkers):
        """
            Adds an PlausibilityChecker instance to the checkers array

            Args:
             checkers: Array of Checkers
        """
        self._plausibility_checkers += checkers

    def remove_plausibility_checkers(self, checkers):
        """
            Removes a PlausibilityChecker instance from the checkers array if it was added
            before. If not, nothing happens.
        """
        for checker in checkers:
            self._plausibility_checkers.remove(checker)

    def check(self):
        """
            Convenience method to add many PlausibilityChecker instances to the checkers
            array. Uses register_plausibility_checker() internally.
        """
        for plausibility_checker_class in self._plausibility_checkers:
            plausibility_checker = plausibility_checker_class(self._token, self._job)
            plausibility_checker.check()
            self._errors.update(plausibility_checker.get_errors())
            if plausibility_checker.is_critical_errors():
                self._is_critical_errors = True

    def get_errors(self):
        """
            Returns all error messages.
        """
        return self._errors

    def is_critical_errors(self):
        """
            Returns true if there is a fatal error that causes the job to fail.
            In this case the PlausibilityCheckManager does not need to execute further PlausibiliyCheckers
        """
        return self._is_critical_errors

class PlausibilityChecker():
    """
        abstract class
        The PlausibilityChecker is a class that checks weather the operations of a job
        can be executed and weather they are useful or nonsense.

        Author: Benno Ommerborn
    """
    def __init__(self, token, job):
        """ Constructor

            Args:
                token: Access token
                job: Job to check
                is_critical_errors: fatal error that causes the job to fail.
        """
        self._token = token
        self._job = job
        self._errors = {}
        self._is_critical_errors = False

    def get_errors(self):
        """
            Returns all error messages.
        """
        return self._errors

    def is_critical_errors(self):
        """
            Returns true if there is a fatal error that causes the job to fail.
            In this case the PlausibilityCheckManager does not need to execute further PlausibiliyCheckers
        """
        return self._is_critical_errors

class SimplePlausibilityChecker(PlausibilityChecker):
    """
        The SimplePlausibilityChecker checks if whether a folder has been moved or copied to one of the subfolders

        Author: Benno Ommerborn
    """
    def check(self):
        """
            The methode checks every operation if whether a folder has been
            moved or copied to one of the subfolders
            If there are operations that are not plausible they are added to
            the array that will be returned.
            Furthermore the is_critical_error flag is set to true
        """
        accessor = IndexAccessor.get_accessor(self._token)

        operations = Operation.objects.filter(job=self._job.id)
        error_operations = []
        error_message = None
        for operation in operations:
            if not accessor.has_node(operation.source):
                error_message = "Invalid source path (not found or no permissions)"
            if not accessor.get_node(operation.source).is_mutable:
               error_message = "Source is not mutable"
            source_node = accessor.get_node(operation.source)

            if operation.get_type_as_string() in ("MOVE", "COPY"):
                if not accessor.has_node(operation.target):
                    error_message = "Invalid target path (not found or no permissions)"

                target_node = accessor.get_node(operation.target)

                if not target_node.can_have_children:
                    error_message = "target cannot have children"

                # check if there is a element in the same directory like the
                # source tha has the same name
                if source_node.name in [x.name for x in accessor.get_children(target_node)]:
                    error_message = "Target does already contain a element with name of source."

                # check if the source is a prefix of the target
                # eg. MOVE /a/b -> /a/b/c/d/
                target_len = len(target_node.path)
                source_len = len(source_node.path)

                t = target_node.path[0:source_len]
                s = source_node.path
                if s == t:
                    error_message = "A folder can not be moved or copied to one of its subfolders."

            if error_message:
                # the operation is not plausible
                error_operations.append(operation)
                self._is_critical_errors = True
                self._errors.update({int(operation.id) : "criticial error: " + error_message})
        return error_operations

class InverseChecker(PlausibilityChecker):
    """
        The InverseChecker checks if whether a folder has been moved back and forth so that overall there is no change

        Author: Benno Ommerborn
    """
    def check(self):
        """
            The methode checks every operation if whether a folder has been moved back and forth so that overall there is no change
            If there are operations that are not plausible they are added to the array that will be returned.
        """
        operations = Operation.objects.filter(job=self._job.id)
        error_operations = []
        for operation in operations:
            inverse = Operation.objects.filter(source=operation.target, target=operation.source, op_type=operation.op_type, job=operation.job)
            if len(inverse) != 0:
                # the operation is not plausible
                error_operations.append(operation)
                self._errors.update({int(operation.id) : "error: Operation is inverse of " + str(inverse[0].id)})
        return error_operations

class PermissionChecker(PlausibilityChecker):
    """
        The Permission checks if any operation tries to move/copy/delete/rename a file
        that is not accessible by the job owner.

        Author: Benno Ommerborn
    """
    def check(self):
        """
            This method walks through the operations of the job and checks
            if every operation can be executed with the rights of the job owner.
            If there are operations that violate access rights they are added to
            the array that will be returned.
            Furthermore the is_critical_error flag is set to true
        """
        accessor = IndexAccessor.get_accessor(self._token)

        operations = Operation.objects.filter(job=self._job.id)
        error_operations = []
        error_message = None
        for operation in operations:
            if not accessor.has_node(operation.source):
                error_message = "Invalid source path (not found or no permissions)"
            if operation.target and not accessor.has_node(operation.target):
                error_message = "Invalid source path (not found or no permissions)"
            if not accessor.get_node(operation.source).is_mutable:
                error_message = "Source is not mutable"
            if error_message:
                error_operations.append(operation)
                self._is_critical_errors = True
                self._errors.update({int(operation.id) : "criticial error: " + error_message})
        return error_operations
