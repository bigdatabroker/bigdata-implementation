from django.conf import settings
from jms.utils import import_class

class OperationPlausibilityChecker(object):
    """ A checker class that is used to check wheter
    specific operations are non-sense or not.

    Attributes:
        accessor: IndexAccessor object
        op_type: Type of operaction (move, delete, ...)
        source: Source path
        target: Target path

    Author: Simon Hessner
    """
    def __init__(self, accessor, op_type, source, target):
        """ Constructor

        Args:
            accessor: IndexAccessor object
            op_type: Type of operaction (move, delete, ...)
            source: Source path
            target: Target path        
        """
        self.accessor = accessor
        self.op_type = op_type
        self.source = source
        self.target = target

    def check(self):
        """ Checks the given operation for plausibility.

        These checks are done:
            All types: Does the source path exist?
            Delete:    Is the target == None?
            Rename:    Valid filename? Target does not exist before?
            Copy/Move: No name conflicts?
            Copy/Move:      source is no prefix of target?

        Returns:
            None if all checks were passed.
            Error message if at least one check failed.
        """
        if not self.accessor.has_node(self.source):
            return "Invalid source path (not found or no permissions)"

        if not self.accessor.get_node(self.source).is_mutable:
            return "Source is not mutable"

        source_node = self.accessor.get_node(self.source)

        if self.op_type == "DELETE" and self.target is not None:
            return "Delete has no target"

        if self.op_type in ("MOVE", "COPY", "RENAME") and not self.target:
            return "this operation type needs a target"

        if self.op_type == "RENAME":
            # Only / and \0 are disallowed as filename in unix
            if "/" in self.target or "\0" in self.target:
                return "target must not contain nullbyte or slash"

            parent = self.accessor.get_parent(source_node)
            if parent is None:
                return "you cannot work on server level"

            # check if there is a element in the same directory like the
            # source tha has the same name
            if self.target in [x.name for x in self.accessor.get_children(parent)]:
                return "target already exists"

        if self.op_type in ("MOVE", "COPY"):
            if not self.accessor.has_node(self.target):
                return "Invalid target path (not found or no permissions)"

            target_node = self.accessor.get_node(self.target)

            if not target_node.can_have_children:
                return "target cannot have children"

            # check if there is a element in the same directory like the
            # source tha has the same name
            if source_node.name in [x.name for x in self.accessor.get_children(target_node)]:
                return "Target does already contain a element with name of source."

            # check if the source is a prefix of the target
            # eg. MOVE /a/b -> /a/b/c/d/
        
            target_len = len(target_node.path)
            source_len = len(source_node.path)

            t = target_node.path[0:source_len]
            s = source_node.path

            if s == t:
                return "A folder can not be moved or copied to one of its subfolders."

        return None