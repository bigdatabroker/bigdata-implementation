from jms.exception.optimizer import OptimizerError
from jms.models import Job, Operation
from django.conf import settings
from jms.utils import import_class
from jms.index.abstract import IndexAccessor

class OptimizationManager(object):
    """ The OptimizationManager is a class that is able to have a
    few registered optimizers. It provides a method that calls
    all registered optimizers.

    There is the possibility to register optimizers directly in the
    constructor. By default there is the settings.DEFAULT_OPTIMIZER_CLASSES
    used.

    Author: Simon Hessner
    """
    def __init__(self, optimizers = settings.DEFAULT_OPTIMIZER_CLASSES):
        """ Initializes the OptimizationManager

        Args:
            optimizers: Array of class names. These will be registered.
                The default value is settings.DEFAULT_OPTIMIZER_CLASSES
        """
        self._optimizers = []
        self.register_optimizers(optimizers)

    def register_optimizer(self, optimizer_class):
        """ Registers a single optimizer class.

        Args:
            optimizer_class: Class name as String.
        """
        self.register_optimizers([optimizer_class])

    def register_optimizers(self, optimizer_classes):
        """ Registers multiple optimizer classes.

        Args:
            optimier_classes: List of class names as string.
        """
        _optimizers = []
        for opt in optimizer_classes:
            _optimizers.append(import_class(opt))

        self._optimizers += _optimizers

    def remove_optimizer(self, optimizer_class):
        """ Removes a previously registered optimizer class.

        Args:
            optimizer_class: Name of the class as String.
        """
        optimizer = import_class(optimizer_class)
        if self._optimizers.contains(optimizer):
            self._optimizers.remove(optimizer)

    def optimize(self, job):
        """ Calls optimize() on every registered optimier class after
        creating an instance of them.

        Args:
            job: Job that contains the operations that should be optimized.
        """
        for optimizer in self._optimizers:
            obj = optimizer(job)
            obj.optimize()

class Optimizer(object):
    """ Abstract base class for Optimizers.

    Author: Simon Hessner
    """

    def __init__(self, job):
        """ Initializes the optimizer with a job.

        Args:
            job: Job that should be optimized.

        Raises:
            OptimizerError: If invalid job.
        """
        if not job:
            raise OptimizerError("Optimizer.__init__: job needed")

        if not isinstance(job, Job):
            raise OptimizerError("Optimizer.__init__: job not instance of Job")

        self.job = job

    def optimize():
        """ Executes the actual optimizing. """
        raise NotImplementedError("Optimizer is abstract!")

class GroupOptimizer(Optimizer):
    """ The GroupOptimizer analyzes the operations of a job.
    If there are redundant operations, they will be removed.
    If there are multiple operations that could be summarized,
    they will be summarized.

    Example:
    Given the following directory structure:
    - /a/
    -- /a/1/
    -- /b/2/
    - /c/

    and these operations:
    move a/1 -> c
    move a/2 -> c

    Then the optimizer summarizes them to:
    move a -> c

    Author: Simon Hessner
    """

    def optimize(self):
        """ Summarizes operations and removes duplicates. """
        if not self.job.token:
            #only jobs that have a valid token are optimized
            return

        ia = IndexAccessor.get_accessor(self.job.token)

        self._remove_duplicates(ia)
        self._summarize(ia)
        self._remove_duplicates(ia)

    def _remove_duplicates(self, ia):
        """ Walks over all operations of a job and removes operations
        that are redundant.
        """
        operations = list(self.job.operation_set.all())

        to_delete = []
        for operation in operations:
            for op in operations:
                # an object is no duplicate of itself
                if op.id == operation.id:
                    continue

                # duplicates have same type and status
                if op.op_type != operation.op_type or op.status != operation.status:
                    continue

                # the source must be the same (possibly not the same string, but the same path)
                if ia.has_node(op.source) and ia.has_node(operation.source):
                    src1 = ia.get_node(op.source)
                    src2 = ia.get_node(operation.source)
                    if src1.url != src2.url:
                        continue
                else:
                    continue

                # if not DELETE, the target must be the same.
                if op.op_type != "DELETE":
                    if ia.has_node(op.target) and ia.has_node(operation.target):
                        tar1 = ia.get_node(op.target)
                        tar2 = ia.get_node(operation.target)
                        if tar1.url != tar2.url:
                                continue
                    else:
                        continue

                if not op in to_delete and operation not in to_delete:
                    to_delete.append(op)

        for x in to_delete:
            if x in operations:
                operations.remove(x)
            x.delete()

    def _summarize(self, ia):
        operations = list(self.job.operation_set.all())

        for operation in operations:
            ##########################################
            ###### SUMMARIZATION OF OPERATIONS #######
            ##########################################

            op_type = operation.get_type_as_string()

            # RENAME cannot be summarized!
            if not op_type in ("COPY", "MOVE", "DELETE"):
                continue

            # This is a little flag to make the following algorithm
            # easier.
            # Delete operations have no target, so get_node(None)
            # has to be avoided. isdel helps!
            isdel = op_type == "DELETE"

            if ia.has_node(operation.source) and (isdel or ia.has_node(operation.target)):
                source = ia.get_node(operation.source)
                target = None if isdel else ia.get_node(operation.target)

                # will be needed later to get the path of the summarized operations
                parent = ia.get_parent(source)
                if not parent.is_mutable:
                    # operations on the root node cannot be summarized!
                    continue

                # other elements on the same level as source
                sisters = ia.get_children(parent)
                sisters = [x for x in sisters if x.path != source.path]

                # The following code iterates over all elements in the
                # directory of the source of the current operation
                # If it contains all files in this directory and
                # the target is also the same, remove is set to True
                # to_remove is filled with all elements that should be removed
                # if remove is True after the loop
                remove = True
                to_remove = [operation]

                for x in sisters:
                    # True if there is a operation that applies to x in
                    # the same way the current operation applies to source
                    # The target of the operations must be the same
                    contains = False
                    for op in operations:
                        if (op.get_type_as_string() == op_type
                        and op.status == operation.status
                        and ia.has_node(op.source) and (isdel or ia.has_node(op.target))):
                            s_node = ia.get_node(op.source)
                            t_node = None if isdel else ia.get_node(op.target)

                            if s_node.path == x.path and (isdel or t_node.path == target.path):
                                contains = True
                                break #otherwise always the last operation would be appended!

                    # if at least one element of the directory of source
                    # is not affected by a correct operation type
                    # there is nothing that can be summarized!
                    if not contains:
                        remove = False
                        break #improves performance
                    else:
                        to_remove.append(op)

                # summarizing one operation is nonsense!
                if remove and len(to_remove) > 1:
                    for x in to_remove:
                        if x in operations:
                             operations.remove(x)
                             x.delete()

                    #we have to produce a summarized operation
                    new_operation = Operation()
                    new_operation.set_type_by_string(op_type)
                    new_operation.job = self.job
                    new_operation.target = operation.target
                    new_operation.source = parent.url
                    new_operation.save()

                    # adding the new operation to the operations list
                    # makes it possible to summarize this newly created operation
                    operations.append(new_operation)
                    self._remove_duplicates(ia)
