from jms.models import AccessToken
from jms.models import User

class AuthenticationHandler(object):
    """ Base class for every authentication handler.

    It takes the data the user submits to authenticate and validates it.
    On success an access token is generated. If it is the first
    authentication of the user an user object is created.

    Author: Julian Schuh, Simon Hessner
    """
    def __init__(self, data):
        """ Initializes a new AuthenticationHandler.

        Args:
            data: Request data.
        """
        self._data = data
        self._token = None
        self._error_message = ''

    def authenticate(self):
        """ Executes the actual authentication. 

        Returns:
            True if success, else False.
        """
        return False

    def get_access_token(self):
        """ Returns the generated AccessToken.

        Returns:
            AccessToken if the user authenticated successfully. If not,
            None is returned.
        """
        return self._token

    def get_error_message(self):
        """ Returns the error message if the authentication failed. 

        Returns:
            String containing error message.
        """
        return self._error_message

    def __create_token__(self, expires, user_id, name, group_ids, is_admin, priority, data):
        """ Helper method that handles the creation of AccessTokens and update of user data.

        Args:
            expires: Datetime when the token should expire. None if never.
            user_id: Unique ID of the authenticated user.
            name: Username
            group_ids: Groups the user belongs to (as CSV)
            is_admin: Boolean that indicates if the user is an admin.
            priority: User priority (used for scheduling)
            data: Data that should be associated with the token.

        """
        user = None

        try:
            user = User.objects.get(user_id = user_id)
        except User.DoesNotExist:            
            user = User(user_id = user_id)

        #update user data
        user.priority = int(priority)
        user.is_admin = bool(is_admin)
        user.name = str(name)
        user.group_ids = group_ids.split(',')
        user.save()

        #create token
        access_token = AccessToken()
        access_token.generate_token()        
        access_token.user = user
        access_token.expires_on = expires
        access_token.data = data
        access_token.save()

        self._token = access_token