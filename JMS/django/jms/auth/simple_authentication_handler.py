from authentication_handler import AuthenticationHandler
import datetime as dt
from jms.utils import get_proper_date

class SimpleAuthenticationHandler(AuthenticationHandler):
    """ A very simple implementation of an AuthenticationHandler.

    It takes an user_id and a password and checks if the combination is
    contained in the USERS array.

    If yes, the token is created and the user has authenticated successfully.

    Author: Simon Hessner, Julian Schuh
    """
    # only for testing
    USERS = {1: {"groups" : "1,2,3",
                 "name" : "bart",
                 "password" : "password",
                 "admin" : False,
                 "priority" : 1
                },
             2: {"groups" : "1,2,3",
                 "name" : "julez",
                 "password" : "hallo",
                 "admin" : True,
                 "priority" : 1
                },
             3: {"groups" : "1,2,3",
                 "password" : "test",
                 "name" : "benno",
                 "admin" : False,
                 "priority" : 1
                },
             4: {"groups" : "1,2,3",
                 "password" : "bigdatabroker",
                 "name" : "nomis",
                 "admin" : True,
                 "priority" : 1
                },
             99: {"groups" : "1,2,3",
                 "password" : "telekom",
                 "name" : "imper",
                 "admin" : False,
                 "priority" : 1
                 },
             17: {"groups" : "1,2,3",
                 "password" : "a",
                 "name" : "matze",
                 "admin" : False,
                 "priority" : 1
                }
             }

    def __init__(self, data):
        """ Initializes a new SimpleAuthenticationHandler

        Args:
            data: Request data.
        """
        super(SimpleAuthenticationHandler, self).__init__(data)

    def authenticate(self):
        """ Checks if the user can be authenticated.

        Returns:
            True if authentication was successfull. 
        """
        now = dt.datetime.now()
        delta = dt.timedelta(days = 14)
        expiration = get_proper_date(now + delta)
        data = str(now)

        if not self._data.DATA.has_key("user_id") or not  self._data.DATA.has_key("password"):
            self._error_message = "no auth data specified"
            return False

        try:
            user_id = int(self._data.DATA["user_id"])
        except:
            user_id = -1

        password = self._data.DATA["password"]

        if not self.is_user_allowed(user_id, password):
            return False

        group_ids = self.USERS[user_id]["groups"]
        admin = self.USERS[user_id]["admin"]
        name =  self.USERS[user_id]["name"]
        priority = self.USERS[user_id]["priority"]

        super(SimpleAuthenticationHandler, self).__create_token__(expiration, user_id, name, group_ids, admin, priority, data)

        return True

    def is_user_allowed(self, user_id, pw):
        """ Checks if (user_id, pw) can be authenticated.

        Returns:
            True if user_id has the specified password.
        """
        return self.USERS.has_key(user_id) and self.USERS[user_id]["password"] == pw
