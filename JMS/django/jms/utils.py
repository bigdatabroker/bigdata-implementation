# -*- coding: utf-8 -*-

import string
import random
import re
from datetime import datetime
from django.utils.timezone import utc

from django.utils.importlib import import_module


# TODO comment!
def import_class(class_path):
    module_path, class_name = class_path.rsplit('.', 1)
    module = import_module(module_path)
    class_instance = getattr(module, class_name)

    return class_instance

class TokenGenerator(object):
    """ A simple utility class that generates tokens.

    The token length and the used chars are configurable.

    Author: Simon Hessner, Julian Schuh
    """

    def __init__(self, length, chars = string.ascii_lowercase + string.digits):
        """ Initializes the generator.

        Parameters:
            length: The length of the token
            chars: Set of chars to use
        """
        self._length = length
        self._chars = chars

    def get_token(self):
        """ Creates and returns a token with the specified properties. """
        return ''.join(random.choice(self._chars) for x in range(self._length))

class StatParser(object):
    """ Utility class that parses lines of stat files and checks if their
    format is correct.

    It extracts the file type and octal file permissions from the rwx string.

    Constants:
        FILE: 0
        DIRECTORY: 1
        OTHER: 2

    Author: Simon Hessner, Julian Schuh
    """

    def __init__(self):
        """ Initializes a new StatParser. Precompiles the regex. """
        self.regex = re.compile(u'^(([-dlpscbD]{1})([rwxst-]{9}))\s+(\d+)\s(\d+)\s(\d+)\s"(.*?)"\s"(.*?)"\s"(.*?)"\s"(.*?)"\s"„?(.*?)“?"\s*$', re.IGNORECASE)
        self.parsed = None

    def parse_line(self, line):
        """ Tries to execute the regex compiler on the given line and
        returns True if there were no syntax errors, False if the format
        of the line is not correct.

        Args:
            line: String containing stat line.

        Returns:
            True if format is correct, else False.
        """

        result = self.regex.search(line)

        if not result:
            return False

        try:
            name = result.group(11)
            splitted = name.rstrip("/").rsplit("/", 1)

            file_name = None
            if len(splitted) >1:
                file_name = splitted[1]

            self.parsed = {
                'permissions': result.group(1),
                'permissions_oct': self.rwx_to_oct(result.group(3)),
                'type': self.get_type(result.group(2)),
                'size': result.group(4),
                'owner_user': result.group(5),
                'owner_group': result.group(6),
                'times': {
                    'birth': self.get_datetime(result.group(7)),
                    'last_access': self.get_datetime(result.group(8)),
                    'last_modification': self.get_datetime(result.group(9)),
                    'last_change': self.get_datetime(result.group(10))
                },
                'path': splitted[0],
                'name': file_name
            }
        except ValueError:            
            return False

        return True

    def get_result(self):
        """ Should only be called if parse_line was called before.
        Returns:
            The line as dict with additionally parsed values.
        """
        return self.parsed

    def get_datetime(self, date_string):
        """ Helper method that converts a string containing a datetime
        to a datetime object.

        Args:
            String containing a date.

        Returns:
            Datetime object
        """
        try:
            date_string = date_string.split('.', 1)[0]     # Server sends nanosecond, python is not capable of working with that. Lets just remove the mcro seconds.
            return datetime.strptime(date_string, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            return None

    def rwx_to_oct(self, rwx):
        """ Helper method that converts a string in the "rwx format"
        into an octal number.

        Args:
            rwx: String containing permissions

        Returns:
            Octal integer containing permissions
        """
        #split the three parts: user, group, other (each containing
        #three chars)
        user = rwx[0:3]
        group = rwx[3:6]
        all_users = rwx[6:9]

        stri = ""
        for part in [user, group, all_users]:
            exp = 0 #exponent for the sum calculation
            part_sum = 0

            #walk through the part reverse because the last char has value 1,
            #the middle char has value 2, the first char has value 3
            for char in part[::-1]:
                if char in (u'r', u'w', u'x', u's', u't'):  # Sticky bits and setuid/setguid bits. NO capital S or T.
                    part_sum += 2**exp
                exp += 1
            stri += str(part_sum)
        return int(stri, 8) #handle the string as octal integer

    def get_type(self, eltype):
        """ Extracts the element type of the rwx format.

        Args:
            rwx: permission string

        Returns:
            File type (FILE, DIRECTORY, OTHER)
        """
        if eltype == '-':
            return 'FILE'
        if eltype == 'd':
            return 'DIRECTORY'
        return 'OTHER'

def get_proper_date(date):
    """ Little helper method that converts the datetime into the
    format django wants it. """
    return date.replace(tzinfo = utc)
