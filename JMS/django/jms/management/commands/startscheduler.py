from django.core.management.base import BaseCommand

from rq import use_connection
from rq_scheduler import Scheduler
from datetime import datetime
from jms.worker.token import TokenDeleter

from jms.logger.database import DatabaseLogger
from django.conf import settings

def sched():
    """ Helper function that is called by the rqscheduler in a defined interval. """
    deleter = TokenDeleter()
    deleter.execute_now()

class Command(BaseCommand):
    """ Adds a new command to manage.py.

    Call python manage.py startscheduler to execute this command.

    It initializes rqscheduler with the correct values.

    Author: Simon Hessner
    """
    args = 'None'
    help = 'schedules token deleter'

    def handle(self, *args, **options):
        """ python manage.py startscheduler """
        use_connection()
        scheduler = Scheduler('default')

        for job in scheduler.get_jobs():
            if job.func == sched:
                print ("already scheduled: %s" % job.id)
                return

        job = scheduler.schedule(scheduled_time=datetime.now(),
            func = sched,
            interval = settings.TOKEN_DELETER_INTERVAL,
            repeat = None #repeat forever
            )

        print("Added %s" % job.id)