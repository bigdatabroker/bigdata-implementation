from django.core.management.base import BaseCommand

from rq import use_connection
from rq_scheduler import Scheduler
from datetime import datetime
from jms.worker.index import IndexUpdater

from jms.logger.database import DatabaseLogger
from django.conf import settings

class Command(BaseCommand):
    """ Adds a new command to manage.py.

    Call python manage.py startscheduler to execute this command.

    It initializes rqscheduler with the correct values.

    Author: Simon Hessner
    """
    args = 'path to statfile'
    help = 'imports tree'

    def handle(self, *args, **options):
        """ python manage.py importtree """
        use_connection()
        
        if len(args) != 2:
            return "please provide both server id and path to stat file"

        index = IndexUpdater(path=args[0], server_id=args[1])
        index.execute()
        return "ok"