from jms.worker.abstract import WorkerJob
from jms.logger.logger import Logger
from jms.exception.worker import WorkerError
from jms.scheduler.optimizer import OptimizationManager
from jms.models import Event

class JobOptimizer(WorkerJob):
    """ This is a background worker class that calls the 
    OptimizationManager and optimizes jobs to be executed more
    efficient.

    Author: Simon Hessner
    """
    def __init__(self, **arguments):
        """ Initializes the JobOptimizer.

        Args:
            job: Job to be optimized.

        Raises:
            WorkerError if no job is given.
        """
        if not arguments.has_key("job"):
            d = Logger.get_logger()
            msg = "JobOptimizer: no job given via constructor"
            d.log(msg, True, None)
            raise WorkerError(msg)

        self.job = arguments["job"]

    def _perform(self):
        """ Gets called by super.execute() (if scheduled) or super.execute_now()

        Creates a OptimizationManager and lets the given job be optimized.

        """
        optimizer = OptimizationManager()
        optimizer.optimize(self.job)

        self.job.set_status_by_string("READY")
        self.job.save()

        if not Event.get_event(self.job, "OPTIMIZATION"):
            event = Event(job=self.job)
            event.set_type_by_string("OPTIMIZATION")
            event.save()