import django_rq
from django.conf import settings

class WorkerJob(object):
    """ Base class for jobs that should be able to run in the
    background.

    Running Jobs in the background enables the application to
    return a result to the user who does not have to wait until
    the job finished (which could take weeks or even days)

    Author: Simon Hessner
    """

    def __init__(self, **arguments):
        """ Initializes a new WorkerJob

        Args:
            arguments: Array of strings the worker needs to work.
        """
        raise NotImplementedError("WorkerJob is abstract.")

    def execute(self):
        """ Adds itself to the queue to be executed later. """
        queue = django_rq.get_queue(self._get_queue_name())
        timeout = self._get_timeout()
        queue.enqueue_call(func=self.execute_now, timeout=timeout)


    def execute_now(self):
        """ Executes the job directly without adding it to the queue. """
        return self._perform()

    def _get_queue_name(self):
        return "default"

    def _get_timeout(self):
        return settings.DEFAULT_RQ_TIMEOUT

    def _perform(self):
        """ This method performs the actual work of the worker job.
        It has to be overwritten by the subclasses of WorkerJob.
        """
        raise NotImplementedError("WorkerJob is abstract.")
