# -*- coding: utf-8 -*-

from jms.worker.abstract import WorkerJob
from jms.exception.worker import WorkerError
from os import path
from jms.logger.logger import Logger
from jms.utils import StatParser
from os import remove
from django.conf import settings
from jms.index.abstract import IndexAccessor
from jms.models import Server, AccessToken
from jms.index.utils import parse_url
import codecs

class IndexUpdater(WorkerJob):
    """ Reads stat files from the cache, parses them and updates the
    file index.

    Attributes:
        path: Path to stat file that should be processed.

    Author: Simon Hessner
    """
    def __init__(self, *args, **kwargs):
        """ Initializes the IndexUpdater

        Args:
            path: Path to stat file that should be processed.
        """

        if not kwargs.has_key('path') or kwargs['path'] is None:
            raise WorkerError("Please provide a path to the stat file.")
        self.path = path.abspath(kwargs['path'])

        if not kwargs.has_key('server_id') or kwargs['server_id'] is None:
            raise WorkerError("Please provide a server id")
        self.server_id = int(kwargs['server_id'])

        self.root_deleted = False


    def _get_queue_name(self):
        """ Returns index as name for the queue this worker will be put in. """
        return "index"

    def _get_timeout(self):
        return 60 * 60 * 24 # 1 day
    
    def _perform(self):
        """ Gets called when rq has finished the other tasks.

        This method reads the stat file from the place the API has stored it
        and updates the tree structure.
        """
        logger = Logger.get_logger()

        try:
            server = Server.objects.get(pk=self.server_id)
        except Server.DoesNotExist:
            msg = "IndexUpdater: ID %d is no registered server" % self.server_id
            logger.log(msg, True, None)
            raise WorkerError(msg)

        accessor = IndexAccessor.get_accessor()
        accessor.enter_global_mode()

        if not path.isfile(self.path):
            msg = "IndexUpdater: %s is no file" % self.path
            logger.log(msg, True, None)
            raise WorkerError(msg)

        created_nodes = 0

        parser = StatParser()
        root_node = accessor.get_node(server.hostname + ":/") # default when first line does not contain a path.
        root_string = "/"
        try:
            with codecs.open(self.path, 'r', 'utf8') as f:
                linenumber = 0               
                for line in f:    
                    if not line or line == "" or len(line) == 0 or line.isspace():
                        continue #skip empty line

                    # please do not add the u'' to the string. python will crash completely!                                           
                    print('processing line ' + line.encode('utf8'))
                    if parser.parse_line(line):
                        info = parser.get_result()
                        print('info: ' + str(info))
                        ipath = info["path"]
                        if not self._check_prefix(root_string, ipath):
                            msg = "Path %s is not located under root %s" % (ipath.encode("utf8"), root_string.encode("utf8"))
                            logger.log(msg, True, None)
                            raise WorkerError(msg+" " + str(linenumber))

                        self._execute_update(info, accessor, root_node, server)
                        created_nodes += 1
                    else:
                        if linenumber == 0:
                            #remove last newline because
                            #otherwise the indexaccessor would crash
                            if line.endswith("\n"):
                                line = line[0:-2]

                            # the first line is allowed to contain a path
                            # instead of a stat entry. other lines are not!
                            root_node = accessor.get_node(u"%s:%s" % (server.hostname, line))
                            root_string = line
                        else:
                            # This case should definitely never occur.
                            # (Because the file was already checked on upload)
                            # But if it altough occurs, we want to log it.
                            msg = "IndexUpdater: The following line could not be parsed. Stopped here: \n %s" % line
                            logger.log(msg, True, None)
                            raise WorkerError(msg)
                    linenumber += 1
        except IOError:
            logger.log("IndexUpdater: %s cannot be opened" % self.path, True, None)
            raise WorkerError("IndexUpdater: %s cannot be opened" % self.path)

        try:
            remove(str(self.path))
        except OSError:
            logger.log(u"Cannot delete file %s" % self.path, True, None)

        self.path = None

        return "Created %i nodes" % created_nodes
    
    def _delete_root(self, accessor, root_node):        
        """ Deletes the given root_node and all its children.

        Args:
            accessor: IndexAccessor instance 
            root_node: First node to delete
        """
        for node in accessor.get_children(root_node):
            accessor.delete_node(node)

        self.root_deleted = True

    def _execute_update(self, element, accessor, root_node, server):
        """ First deletes the root and then adds the given element to the index.

        Args:
            element: Element that should be added.
            accessor: IndexAccessor instance
            root_node: Root element of the whole update processed
            server: The update requester server
        """
        if not self.root_deleted:
            self._delete_root(accessor, root_node)

        if element['name'] is None:
            # root element; doesen't have name
            return

        parent_path = u"%s:%s" % (server.hostname, element['path'])

        print('adding ' + element['name'].encode("utf8") + ' node to path '+ parent_path.encode("utf8"))
        accessor.add_node(parent_path, element)
    
    def _check_prefix(self, root, path):
        """ Checks if root is a prefix of path.

        Args:
            root: root path
            path: path that is "under" root

        Returns:
            True if path is a (indirect) child of root.
        """
        root = parse_url("dummy:%s" % root)[1:]
        path = parse_url("dummy:%s" % path)[1:]

        if len(root) > len(path):
            return False

        minlen = min(len(root), len(root))

        shortened_root = root[0:minlen]
        shortened_path = path[0:minlen]

        return shortened_path == shortened_root