from jms.worker.abstract import WorkerJob
from jms.models import AccessToken
from datetime import datetime
from jms.utils import get_proper_date


class TokenDeleter(WorkerJob):
    """ This background worker deletes expired tokens.

    Deleting expired tokens increases the security of the
    Big Data Broker and keeps the database clean.

    Author: Simon Hessner
    """
    def __init__(self, **arguments):
        pass #no arguments required

    def _perform(self):
        #required by django due to time zone issues
        now = get_proper_date(datetime.utcnow())
        res = AccessToken.objects.filter(expires_on__lt = now)

        # We have to call delete() separately for every object because
        # only this way django calls delete() on the object and handles
        # cascade deletes correctly
        for token in res:
            token.delete()
