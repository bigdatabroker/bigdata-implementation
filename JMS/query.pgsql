DROP FUNCTION RESOLVE_PATH(VARCHAR[]);

CREATE OR REPLACE FUNCTION RESOLVE_PATH(VARCHAR[]) RETURNS SETOF node AS
$BODY$
DECLARE
	current_name VARCHAR;
	current_parent_id INT;
	current_node node;

	path node[];
BEGIN
	current_parent_id = -1;
	current_node = NULL;

	FOREACH current_name IN ARRAY $1 LOOP
		SELECT *
		FROM node
		WHERE parent_id = current_parent_id AND name = current_name
		INTO current_node;

		IF current_node IS NULL THEN
			RETURN;
		END IF;

		path = path || current_node;

		current_parent_id = current_node.node_id;

		RAISE NOTICE 'row = %', current_node;
	END LOOP;

	FOREACH current_node IN ARRAY path LOOP
		RETURN NEXT current_node;
	END LOOP;

	RETURN;
END
$BODY$
LANGUAGE 'plpgsql';

SELECT * FROM RESOLVE_PATH(array['x', 'y', 'f']);





=================

WITH RECURSIVE tree_node_r(id, name, parent_id, depth, path) AS (
	SELECT tn.tree_node_id, tn.name, tn.parent_id, 1::INT AS depth, tn.tree_node_id::TEXT AS path
	FROM tree_node AS tn
	WHERE tn.parent_id = -1
	UNION ALL
	SELECT c.tree_node_id, c.name, c.parent_id, p.depth + 1 AS depth, (p.path || '->' || c.tree_node_id::TEXT)
	FROM tree_node_r AS p, tree_node AS c
	WHERE c.parent_id = p.id
)
SELECT * FROM tree_node_r AS n ORDER BY n.id ASC;

WITH RECURSIVE parents(tree_node_id, parent_id, depth, path) AS (
		SELECT tree_node_id, parent_id, 1, tree_node_id::TEXT
		FROM tree_node AS tree_node
		WHERE tree_node_id = 8
	UNION ALL
		SELECT tree_node.tree_node_id, tree_node.parent_id, parent.depth + 1, tree_node.tree_node_id::TEXT || '/' || parent.path
		FROM parents AS parent, tree_node AS tree_node
		WHERE parent.parent_id = tree_node.tree_node_id
)
SELECT * FROM parents;


======================


-- Function: resolve_path(character varying[])

-- DROP FUNCTION resolve_path(character varying[]);

DROP FUNCTION get_path(bigint);

CREATE OR REPLACE FUNCTION get_path(bigint) RETURNS TEXT[] AS
$BODY$
DECLARE
	path character varying[];
	current_node tree_node;
BEGIN
	SELECT *
	FROM tree_node
	WHERE tree_node_id = $1
	INTO current_node;

	path = current_node.name || path;

	WHILE current_node.parent_id <> -1 LOOP

		SELECT *
		FROM tree_node
		WHERE tree_node_id = current_node.parent_id
		INTO current_node;

		path = current_node.name || path;

	END LOOP;

	RETURN path;
END
$BODY$
LANGUAGE plpgsql;

SELECT get_path(8) AS path;

