#!/bin/bash
IFS='
'
ROOT_FOLDER=$1

TEMP=/tmp/indexing-$$

pushd ./ > /dev/null
mkdir $TEMP
cd $TEMP

# Create index
find $ROOT_FOLDER > $TEMP/large_index.txt

# Split index
split -l 10000 $TEMP/large_index.txt $TEMP/snipped_index.txt

# Get stats for each index snipped
for i in $TEMP/snipped_index.txt*; do 
    #stat -c "%a %s %u %g %W %X %Y %Z %f %N" `cat $i` > $i.stat
    stat -c "%A %s %u %g \"%w\" \"%x\" \"%y\" \"%z\" \"%n\"" `cat $i` \
        | sed s/\ +0200//g \
        | sed s/[\'\`]//g \
        | sed s/' -> '.*/\"/g \
        > $i.stat
    #stat -c "\"%w\" %N" `cat $i` > $i.stat
done

# Deliver output
cat $TEMP/snipped_index*.stat

# Cleanup
popd > /dev/null
rm -rf $TEMP
